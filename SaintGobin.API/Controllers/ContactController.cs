﻿using SaintGobin.Common;
using SaintGobin.Common.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Mvc;
using SaintGobin.API.Models;
using System.Web;
using RestSharp;
using System.Net.Http.Formatting;
using System.Net.Http;
using System.Web.Script.Serialization;
using Newtonsoft.Json;
using System.Data;
using SaintGobin.Common.DAO;
namespace SaintGobin.API.Controllers
{
    public class ContactController : ApiController
    {
        ContactsBL contactsBL = new ContactsBL();
        UserBL userBL = new UserBL();
        #region Log In
        [System.Web.Mvc.HttpPost]
        public ResponseModel Login(LoginRequestModel model)
        {
            ResponseModel response = null;
            try
            {
                if (model == null)
                {
                    response = new OkResponseModel(EnumResult.Failure, "Invalid credentials");
                    return response;
                }
                //todo Authenticate user through active b
                //Currently i am using static username and password.
                LoginBL lbl = new LoginBL();

                if (userBL.ValidateUser(model.UserName, model.Password))
                {
                   
                    UserBL ubl = new UserBL();
                    var data = ubl.getProfile(model.UserName.ToString());
                    #region OTP Generation
                    Random generator = new Random();
                    string number = generator.Next(1111, 9999).ToString("D4");
                    data.OTP = Convert.ToInt32(number);
                    #endregion

                    this.SendSMSInCsharp(data.OTP.ToString(), model.UserName, model.Password);

                    if (data.Employee_Name == null)
                    {
                        response = new OkResponseModel(EnumResult.Failure, "Error While Fetching Profile");
                        return response;
                    }
                    response = new OkResponseModel(EnumResult.Succes, data);
                    DeviceDetails deviceDetails = new DeviceDetails();
                    deviceDetails.user_id = data.User_id.ToString() ;
                    deviceDetails.gsm_ids = model.gsm_id.ToString();
                    userBL.Submit_DeviceDetails(deviceDetails);//Insert Login User Device Details
                    return response;
                }
                else
                {
                    response = new OkResponseModel(EnumResult.Failure, "Invalid credentials");
                    return response;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                response = new OkResponseModel(EnumResult.Exception, error);
                return response;
            }

        }
        [System.Web.Mvc.HttpPost]
        public bool SendSMSInCsharp(string SecurityCode, string sgid, string mobileno)
        {

       bool ISVALID= userBL.SubmitSMSDetails(sgid, mobileno, SecurityCode.ToString(), false, DateTime.Now, DateTime.Now, "API RESPONSE");
            return ISVALID;

            //using (var client = new HttpClient())
            //{
            //    client.BaseAddress = new Uri("http://10.87.20.172");
            //    var result = client.PostAsync("/WS_MobileServices/MobileServices.svc/SendSMS", new
            //    {
            //        sgid = sgid,
            //        appid = "app001",
            //        mobNo = mobileno,
            //        msg = SecurityCode
            //    }, new JsonMediaTypeFormatter()).Result;
            //    if (result.IsSuccessStatusCode)
            //    {
            //        var products = result.Content.ReadAsStringAsync().Result;
            //        var dict = new JavaScriptSerializer().Deserialize<Dictionary<string, object>>(products);
            //        var postalCode = dict["SendSMSResult"];
            //        UserBL userBL = new UserBL();
            //        if (postalCode == "")
            //        {
            //            userBL.SubmitSMSDetails(sgid, mobileno, SecurityCode.ToString(), false, DateTime.Now, DateTime.Now, postalCode.ToString());
            //        }
            //        else
            //        {
            //            userBL.SubmitSMSDetails(sgid, mobileno, SecurityCode.ToString(), true, DateTime.Now, DateTime.Now, postalCode.ToString() );
            //        }

            //    }
            //    else
            //    {
            //        Console.WriteLine("{0} ({1})", (int)result.StatusCode, result.ReasonPhrase);
            //    }  
         //   }







    //          // Get the posted form values and add to list using model binding
    //IList<string> postData  = new List<string> {"Value1", "m"};

    //using (var client = new HttpClient())
    //{
    //    // Assuming the API is in the same web application. 
    //    string baseUrl = HttpContext.Current .Request .Url.GetComponents(UriComponents.SchemeAndServer, UriFormat.SafeUnescaped);
    //    client.BaseAddress = new Uri(baseUrl);
    //    int result = client.PostAsync("/api/values", 
    //                                  postData, 
    //                                  new JsonMediaTypeFormatter())
    //                        .Result
    //                        .Content
    //                        .ReadAsAsync<int>()
    //                        .Result;

    //    // add to viewmodel
    //    //var model = new ViewModel
    //    //{
    //        var intValue = result;
    //    //};





         //   //Create a restclient
         //   RestClient restClient = new RestClient("url");

         //   //Create request with GET
         ////   var request = new RestRequest("api/serverdata", Method.GET);

         //   var request = new RestRequest("api/serverdata", Method.GET) { RequestFormat = DataFormat.Json };



            //if (SecurityCode != 0 && sgid != null && mobileno != null)
            //{






            //   // we creating the necessary URL string:

            //    string _URL = "http://cloud.smsindiahub.in/vendorsms/pushsms.aspx"; //where the SMS Gateway is running
            //    string _senderid = "WEBSMS";   // here assigning sender id
            //    string _user = HttpUtility.UrlEncode("namrathaup"); // API user name to send SMS
            //    string _pass = HttpUtility.UrlEncode("919067167133");     // API password to send SMS
            //    string _recipient = HttpUtility.UrlEncode("906767133");  // who will receive message
            //    string _messageText = HttpUtility.UrlEncode("Hi"); // text message

            //     //Creating URL to send sms
            //    //string _createURL = _URL +
            //    //"username =" + _user +
            //    //   "&pass=" + _pass +
            //    //   "&senderid=" + _senderid +
            //    //   "&dest_mobileno=" + _recipient +
            //    //   "&message=" + _messageText;

            //    string _createURL = "http://cloud.smsindiahub.in/vendorsms/pushsms.aspx?user=nam&password=9902509884&msisdn=" + mobileno + "&sid=WEBSMS&msg=" + SecurityCode.ToString() + "&fl=0";
            //    try
            //    {
            //        // creating web request to send sms
            //        HttpWebRequest _createRequest = (HttpWebRequest)WebRequest.Create(_createURL);
            //        // getting response of sms
            //        HttpWebResponse myResp = (HttpWebResponse)_createRequest.GetResponse();
            //        System.IO.StreamReader _responseStreamReader = new System.IO.StreamReader(myResp.GetResponseStream());
            //        string responseString = _responseStreamReader.ReadToEnd();
            //        string[] values = responseString.Split(',');
            //        string[] result = values[1].Split(':');
            //        bool IsSent = true;
            //        if (result[0] == "ErrorMessage")
            //        {
            //            IsSent = false;
            //            userBL.SubmitSMSDetails(sgid, mobileno, SecurityCode.ToString(), IsSent, DateTime.Now, DateTime.Now, result[1].ToString());

            //        }
            //        else
            //        {
            //            IsSent = true;
            //            userBL.SubmitSMSDetails(sgid, mobileno, SecurityCode.ToString(), IsSent, DateTime.Now, DateTime.Now, result[1].ToString());

            //        }
            //        _responseStreamReader.Close();
            //        myResp.Close();

            //    }
            //    catch
            //    {
            //        //

            //    }
            //}

            //return true;

        }
        [System.Web.Mvc.HttpPost]
        public SGIDList Tests()
        {

            SGIDList sg = new SGIDList();
            sg.sgid = "hi";
            return sg;
        }
        #endregion

        #region Upload
        [System.Web.Mvc.HttpPost]
        public List<ResponseModel> UploadContact(List<Contact> data)
        {
            List<ResponseModel> responseModel = new List<ResponseModel>(); 
            ResponseModel response = null;
            bool Result = true;
            try
            {
                if (data == null)
                {
                    response = new ResponseModel(EnumResult.Failure) { Response = "Invalid data posted" };
                    responseModel.Add(response);
                    return responseModel;
                }

                List<GUIDLIST> guid = new List<GUIDLIST>();
                #region Validation 
                var chanellist = from chanel in data from adds in chanel.Contactnoslist
                where adds.number != null
                select chanel;
                #endregion
                chanellist = chanellist.GroupBy(x => x.GUID).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                foreach (var item in chanellist)
                {
                    Result = contactsBL.AddContacts(item);
                }
                if (Result)
                {
                    response = new OkResponseModel(EnumResult.Succes, Result) { };
                }
                else
                {
                    response = new OkResponseModel(EnumResult.Failure, Result) { };
                }

                responseModel.Add(response);
                return responseModel;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                response = new OkResponseModel(EnumResult.Exception, error);
                responseModel.Add(response);
                return responseModel;
            }
        }
        #endregion

        #region DownLoad
       [System.Web.Mvc.HttpPost]
        public ResponseModel DownloadContact(UserZoneInfo data )
        {
            List<ResponseModel> responseModel = new List<ResponseModel>(); 
            ResponseModel response = null;
            DownloadContact model = new DownloadContact();
           
            List<ContactList> result = new List<ContactList>();
            try
            {
                if (data == null)
                {
                    response = new ResponseModel(EnumResult.Failure) { Response = "Invalid data posted" };
                    responseModel.Add(response);
                    return response;
                }

                string LastSyncDate = contactsBL.GetLastSyncdate(data.user_id);
                result = contactsBL.GetPendingSyncRecords(LastSyncDate, data.user_id, data.zone);

                int TotalRows = result.Count();
                model.TotalRows = TotalRows;


                string pagenumber = "";
                try
                {
                    pagenumber = data.CurPage.ToString();
                }
                catch (Exception)
                {
                    pagenumber = null;
                }
                int page = (String.IsNullOrEmpty(pagenumber)) ? 1 : Int32.Parse(pagenumber);

                int CurPage = page;
                int PerPage = data.PerPage;
                int start = (page - 1) * PerPage;
                int offset = PerPage;
                List<ContactList> users = result;
                users = result.Skip(start).Take(offset).ToList();
                model.contact = users;
                response = new OkResponseModel(EnumResult.Succes, model) { };
                responseModel.Add(response);
                return response;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                response = new OkResponseModel(EnumResult.Exception, error);
                responseModel.Add(response);
                return response;
            }
        }
        #endregion

        #region DownLoad All contact
       [System.Web.Mvc.HttpPost]
       public ResponseModel DownloadAllContact(UserZoneInfo data)
       {
           List<ResponseModel> responseModel = new List<ResponseModel>();
           ResponseModel response = null;
           DownloadContact model = new DownloadContact();

           List<ContactList> result = new List<ContactList>();
           try
           {
               if (data == null)
               {
                   response = new ResponseModel(EnumResult.Failure) { Response = "Invalid data posted" };
                   responseModel.Add(response);
                   return response;
               }
               result = contactsBL.getallContact( data.user_id, data.zone);
               int TotalRows = result.Count();
               model.TotalRows = TotalRows;

               string pagenumber = "";
               try
               {
                   pagenumber = data.CurPage.ToString();
               }
               catch (Exception)
               {
                   pagenumber = null;
               }
               int page = (String.IsNullOrEmpty(pagenumber)) ? 1 : Int32.Parse(pagenumber);

               int CurPage = page;
               int PerPage = data.PerPage;
               int start = (page - 1) * PerPage;
               int offset = PerPage;
               List<ContactList> users = result;
               users = result.Skip(start).Take(offset).ToList();
               model.contact = users;
               response = new OkResponseModel(EnumResult.Succes, model) { };
               responseModel.Add(response);
               return response;
           }
           catch (Exception ex)
           {
               string error = ex.Message;
               response = new OkResponseModel(EnumResult.Exception, error);
               responseModel.Add(response);
               return response;
           }
       }
       #endregion

        #region Delete 
       [System.Web.Http.HttpPost]
       public ResponseModel GetDeletedRecords(UserZoneInfo data)
        {
            ResponseModel response = null;
            List<GUIDLIST> result = new List<GUIDLIST>();
            try
            {
                if (data == null)
                {
                    response = new ResponseModel(EnumResult.Failure) { Response = "Invalid data posted" };
                    return response;
                }
                List<GUIDLIST> guid = new List<GUIDLIST>();
                string LastSyncDate = contactsBL.GetLastSyncdate(data.user_id);
                result = contactsBL.GetDeletedRecords(LastSyncDate, data.user_id, data.zone);
               // contactsBL.SaveSyncDetails(data.user_id, data.zone);// Update Last Sync date Of the User 
                response = new OkResponseModel(EnumResult.Succes, result) { };
                return response;
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                response = new OkResponseModel(EnumResult.Exception, error);
                return response;
            }
        }
        #endregion

        #region test
       [System.Web.Http.HttpPost]
       public ResponseModel Test(List<Contact> data )
       {
           ResponseModel response = null;
          
           try
           {
               if (data == null)
               {
                   response = new ResponseModel(EnumResult.Failure) { Response = "Invalid data posted" };
                   return response;
               }
             
              
               response = new OkResponseModel(EnumResult.Succes, data) { };
               return response;
           }
           catch (Exception ex)
           {
               string error = ex.Message;
               response = new OkResponseModel(EnumResult.Exception, error);
               return response;
           }
       }
       #endregion

        #region Sync Details
       [System.Web.Http.HttpPost]
       public ResponseModel UpdateSyncDetail(UserZoneInfo data)
       {
           CommonDAO commonDAO = new CommonDAO();
           commonDAO.WriteToFile("SyncDateAPI HIT---- " + DateTime.Now); 
           ResponseModel response = null;
           bool valid = true;
           try
           {
               if (data == null)
               {
                   response = new ResponseModel(EnumResult.Failure) { Response = "Invalid data posted" };
                   return response;
               }
               List<GUIDLIST> guid = new List<GUIDLIST>();
              valid= contactsBL.SaveSyncDetails(data.user_id, data.zone);// Update Last Sync date Of the User 
              response = new OkResponseModel(EnumResult.Succes, valid) { };
               return response;
           }
           catch (Exception ex)
           {
               string error = ex.Message;
               response = new OkResponseModel(EnumResult.Exception, error);
               return response;
           }
       }
       #endregion

        #region Delete REQUEST
       [System.Web.Http.HttpPost]
       public List<ResponseModel> DeleteRequest(List<GUIDLIST>  data)
       {
           ResponseModel response = null;
           List<ResponseModel> responseModel = new List<ResponseModel>(); 
           bool valid = true;
           try
           {
               if (data == null)
               {
                   response = new ResponseModel(EnumResult.Failure) { Response = "Invalid data posted" };
                   responseModel.Add(response);
                   return responseModel;
               }
               foreach (var item in data)
               {
                   valid = contactsBL.DeleteRequest(item.GUID,item.user_id);// Update Last Sync date Of the User 
               }
             
               response = new OkResponseModel(EnumResult.Succes, valid) { };
               responseModel.Add(response);
               return responseModel;
           }
           catch (Exception ex)
           {
               string error = ex.Message;
               response = new OkResponseModel(EnumResult.Exception, error);
               responseModel.Add(response);
               return responseModel;
           }
       }
       #endregion

        #region SyncNotification
       [System.Web.Http.HttpGet]
       public bool SyncNotification()
       {
           try
           {
             bool valid=  contactsBL.Syncnotification();
             return valid;
           }
           catch (Exception)
           {
               throw;
           }
       }

       #endregion
       
        #region CallLog
       [System.Web.Mvc.HttpPost]
       public ResponseModel CallLog(LogDetails  data)
       {
       
           ResponseModel response = null;
           try
           {
               if (data == null)
               {
                   response = new ResponseModel(EnumResult.Failure) { Response = "Invalid data posted" };
                  
                   return response;
               }
            bool valid = contactsBL.CallLog(data);
            response = new OkResponseModel(EnumResult.Succes, valid) { };
            return response;
           }
           catch (Exception ex)
           {
               string error = ex.Message;
               response = new OkResponseModel(EnumResult.Exception, error);
               return response;
           }
       }
       #endregion
    }
}
