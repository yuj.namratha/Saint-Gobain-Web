﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaintGobin.API.Models
{
    public class ContactAPI
    {
    

  public string  firstname{get;set;}
  public string  middlename{get;set;}
  public string  lastname{get;set;}
  public string  email1{get;set;}
  public string  email2{get;set;}
  public string  email3{get;set;}
  public string  email1_type{get;set;}
  public string  email2_type{get;set;}
  public string  email3_type{get;set;}
  public string  address1{get;set;}
  public string  address2{get;set;}
  public string  address3{get;set;}
  public string  address1_type{get;set;}
  public string  address2_type{get;set;}
  public string  address3_type{get;set;}
  
  public string  contactno1{get;set;}
  public string  contactno2{get;set;}
  public string  contactno3{get;set;}
  public string  contactno4{get;set;}
  public string  contactno5{get;set;}
  public string  website1{get;set;}
  public string  website2{get;set;}
  public string  website3{get;set;}
  public string  street{get;set;}
  public string  Pobox{get;set;}
  public string  jobtitle{get;set;}
  public string  Neiborhood{get;set;}
  public string  city{get;set;}
  public string  state{get;set;}
  public string  zipcode{get;set;}
  public string  country{get;set;}
  public string  home_street{get;set;}
  public string  home_Pobox{get;set;}
  public string  home_jobtitle{get;set;}
  public string  home_Neiborhood{get;set;}
  public string  home_city{get;set;}
  public string  home_state{get;set;}
  public string  home_zipcode{get;set;}
  public string  home_country{get;set;}
  public string  work_street{get;set;}
  public string  work_Pobox{get;set;}
  public string  work_jobtitle{get;set;}
  public string  work_Neiborhood{get;set;}
  public string  work_city{get;set;}
  public string  work_state{get;set;}
  public string  work_zipcode{get;set;}
  public string  work_country{get;set;}
  public string  company {get;set;}
  public string  Notes {get;set;}
  public string  addedby{get;set;}
  public string updatedby { get; set; }


    }
}