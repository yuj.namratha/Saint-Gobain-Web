﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SaintGobin.Common
{
    public class APPSession
    {
        public static APPSession Current
        {
            get
            {
                APPSession session = (APPSession)HttpContext.Current.Session["LOGIN"];
                if (session == null)
                {
                    session = new APPSession();
                    HttpContext.Current.Session["LOGIN"] = session;
                }
                return session;
            }
        }
        public bool IsAdmin { get; set; }
        public int User_id { get; set; }
        public string SGID { get; set; }
        public string zone { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string reporting_person { get; set; }
        public string reporting_person_id { get; set; }
        public string date_of_joining { get; set; }
        public string date_of_relieving { get; set; }
        public string mobile_no { get; set; }
        public string Employee_Name { get; set; }

        public string TemporaryVariable { get; set; }
    }
}
