﻿using SaintGobin.Common.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace SaintGobin.Common.BL
{
    public class ContactsBL
    {
        ContactsDAO contactsDAO = new ContactsDAO();
        #region Edit Contact
        public bool EditContact(Contact contact)
        {
            try
            {

                return contactsDAO.EditContact(contact);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Delete Contact
        public bool DeleteContact(int  id)
        {
            try
            {

                return contactsDAO.DeleteContact(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Add Contacts Portal
        public bool  AddContacts(Contact  contact)
        {
            try
            {

                return contactsDAO.AddContacts(contact);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region GetContactInformation
        public List<Contact> GetDeleteformation()
        {
            try
            {
                return contactsDAO.GetDeleteformation();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetContactInformation
        public List<Contact> GetContactInformation()
        {
            try
            {
                return contactsDAO.GetContactInformation();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Contact> GetContactInformationOnGUIDs(string guid)
        {
            try
            {
                return contactsDAO.GetContactInformationOnGUID(guid);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Contact> GetContactInformationOnID(string guid)
        {
            try
            {
                return contactsDAO.GetContactInformationOnID(guid);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<Contact> GetContactInformations(int user_id)
        {
            try
            {
                return contactsDAO.GetContactInformationOnUsers(user_id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Sync Details Update
        public bool SaveSyncDetails(string user_id, string zone)
        {
            try
            {
                return contactsDAO.SaveSyncDetails( user_id,  zone);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Pending Sync Record
        public List<ContactList> GetPendingSyncRecords(string syncdate, string user_id,string zone)
        {
            try
            {
                return contactsDAO.GetPendingSyncRecords(syncdate, user_id, zone);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region Get ALl Record
        public List<ContactList> getallContact(string user_id, string zone)
        {
            try
            {
                return contactsDAO.GetAllRecords(user_id, zone);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Pending Sync Record
        public List<GUIDLIST> GetDeletedRecords(string syncdate, string user_id, string zone)
        {
            try
            {
                return contactsDAO.GetDeletedRecords(syncdate, user_id, zone);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetLastSyncdate
        public string GetLastSyncdate(string user_id)
        {
            try
            {
                return contactsDAO.GetLastSyncdate(user_id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

    
        #region Delete Request
        public bool DeleteRequest(string GUID,string user_id)
        {
            try
            {
                return contactsDAO.DeleteRequest(GUID, user_id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Deny Request 
        public bool DeleteRequestDeny(string id)
        {
            try
            {

                return contactsDAO.DeleteRequestDeny(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region merger
        public int Merger(List<VCF > id)
        {
            try
            {

                return contactsDAO.Merger(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Delete
        public bool Delete(List<VCF> id)
        {
            try
            {

                return contactsDAO.Delete(id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Sync notification
        public bool Syncnotification()
        {
            try
            {
                CommonDAO commonDAO = new CommonDAO();
                return commonDAO.SyncNotification();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region CallLog
        public bool CallLog(LogDetails logDetails)
        {
            try
            {

                return contactsDAO.CallLog(logDetails);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion


        #region GetCallLogHistory
        public List<CallLogHistory> GetCallLog(string user_id)
        {
            try
            {
                return contactsDAO.GetCallLog(user_id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion



        #region GetCallLogHistory
        public List<OTP> GETOTP()
        {
            try
            {
                return contactsDAO.GETOTP();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool truncate()
        {
            try
            {
                return contactsDAO.truncate();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion



    }
}