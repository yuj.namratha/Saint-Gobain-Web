﻿using SaintGobin.Common.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common.BL
{
   public  class RoleBL
    {
       RoleDAO roleDAO = new RoleDAO();
       public UserRoleMapping GetUserRoles(int user_id)
       {
           try
           {
               return roleDAO.GetUserRoles(user_id);
           }
           catch (Exception)
           {
               throw;
           }
       }
   }
}
