﻿
using SaintGobin.Common.DAO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common.BL
{
    public class UserBL
    {
        UserDAO ob = new UserDAO();
        #region Get Profile
        public User getProfile(string userName)
        {
            try
            {
                if (userName == null)
                {
                    userName = "";
                }
                return ob.getProfile(userName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Users
        public List<User> GetUsers()
        {
            try
            {
             
                return ob.getUsers();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Validate User
        public bool ValidateUser(string LoginId, string Password)
        {
            try
            {

                return ob.ValidateUser( LoginId,  Password);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Device Details
        public bool Submit_DeviceDetails(DeviceDetails data)
        {
            try
            {

                return ob.Submit_DeviceDetails(data);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
        #region Device Details
        public bool SubmitSMSDetails(string sgid, string mobileno, string message, bool IsSent, DateTime reqdatetime, DateTime processdatetime, string returnMsg)
        {
            try
            {
                return ob.SubmitSMSDetails( sgid,  mobileno,  message,  IsSent,  reqdatetime,  processdatetime,  returnMsg);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion
    }
}
