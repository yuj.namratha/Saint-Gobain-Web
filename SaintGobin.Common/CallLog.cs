﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common
{
    public class CallLog
    {
        public string duration { get; set; }
        public string calldate { get; set; }
        public string mobno { get; set; }
    }

    public class LogDetails
    {
        public LogDetails()
        {
            callLog = new List<CallLog>(); 
        }
         public string empname { get; set; }
         public string zone { get; set; }
         public int user_id { get; set; }
        public List<CallLog> callLog{get;set;}
    }

    public class CallLogHistory {
        public int id { get; set; }
        public string duration { get; set; }
        public DateTime  calldate { get; set; }
        public string mobno { get; set; }
        public string empname { get; set; }
        public string zone { get; set; }
        public int user_id { get; set; }
        public string added_on { get; set; }
        public string SGID { get; set; }
    }
}
