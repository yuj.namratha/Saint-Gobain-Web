﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common
{
    public class Contact
    {
		public Contact()
		{
				contactlist = new List<Contactno>();
				Adresslist = new List<Adress>();
				URLlist = new List<URL>();
                websiteslist = new List<Websites>();
                Emaillist = new List<Email>();
               Emailslist = new List<Emails>();
               Contactnoslist = new List<Contactnos>();

            
		} 

    public int id {get;set;}	
    public string firstname	{get;set;}	
    public string middlename	{get;set;}
    public string vcf_name { get; set; }
    public string lastname { get; set; }
    public string prefix { get; set; }
    public string sufix { get; set; }
    public string fn { get; set; }
    public string GUID { get; set; }
    public bool IsDeleteRequest { get; set; }
    public bool  IsSelected { get; set; }
    public int totalRows { get; set; }
    public int CurPage { get; set; }
    public int PerPage { get; set; }
    public string email1 { get; set; }
    [EmailAddress(ErrorMessage = "Invalid Email Address!")]
    public string email2 { get; set; }
     [EmailAddress(ErrorMessage = "Invalid Email Address!")]
    public string email3{ get; set; }
    public int vcf_id { get; set; }
    public string SGID { get; set; }
    public string email1_type { get; set; }
    public string email2_type { get; set; }
    public string email3_type { get; set; }

    public int address1 { get; set; }
    public int address2 { get; set; }
    public int address3 { get; set; }

    public string address1_type { get; set; }
    public string address2_type { get; set; }
    public string address3_type { get; set; }



    public string contactno1_type { get; set; }
    public string contactno2_type { get; set; }
    public string contactno3_type { get; set; }
    public string contactno4_type { get; set; }
    public string contactno5_type { get; set; }

    public string contactno1 { get; set; }
    public string contactno2 { get; set; }
    public string contactno3 { get; set; }
    public string contactno4 { get; set; }
    public string contactno5 { get; set; }

    public string website1 { get; set; }
    public string website2 { get; set; }
    public string website3 { get; set; }

    public int website1_ID { get; set; }
    [Url(ErrorMessage = "Please enter a valid url")]
    public int website2_ID { get; set; }
     [Url(ErrorMessage = "Please enter a valid url")]
    public int website3_ID { get; set; }

    public string street1	{ get; set; }
    public string street2 { get; set; }
    public string jobtitle { get; set; }
    public string city	{ get; set; }
    public string state	{ get; set; }
    public string zipcode	{ get; set; }
    public string country	{ get; set; }

    public string zone { get; set; }
    public string User_id { get; set; }

    public string home_street1 { get; set; }
    public string home_street2 { get; set; }
   
    public string home_jobtitle { get; set; }
   
    public string home_city { get; set; }
    public string home_state { get; set; }
    public string home_zipcode { get; set; }
    public string home_country { get; set; }

    public string work_street1 { get; set; }
    public string work_street2 { get; set; }
    public string work_jobtitle { get; set; }
    public string work_city { get; set; }
    public string work_state { get; set; }
    public string work_zipcode { get; set; }
    public string work_country { get; set; }
        
    public string company { get; set; }
    public string Notes { get; set; }
    public bool status{get;set;}

    public bool IsAPI { get; set; }

    public int addedby	{get;set;}
    public DateTime addedon { get; set; }	
    public int updatedby	{get;set;}
    public DateTime updatedon { get; set; }	

	public List<Contactno> contactlist { get; set; }
	public List<Adress> Adresslist { get; set; }
	public List<URL> URLlist { get; set; }
    public List<Email> Emaillist { get; set; }
            public List<Emails> Emailslist { get; set; }
           public List<Contactnos> Contactnoslist { get; set; }
           public List<Websites> websiteslist { get; set; }
          
	}
    public class ContactList
    {
        public ContactList()
        {
            Adresslist = new List<Adress>();
            websiteslist = new List<Websites>();
            Emailslist = new List<Emails>();
            Contactnoslist = new List<Contactnos>();
        }
        public int vcf_id { get; set; }
     
        public string jobtitle { get; set; }
        public string SGID { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
     
        public string lastname { get; set; }
        public string prefix { get; set; }
        public string sufix { get; set; }
        public string GUID { get; set; }
        public string company { get; set; }
        public int addedby { get; set; }

        public string IsUpdated { get; set; }
        public DateTime addedon { get; set; }
        public List<Adress> Adresslist { get; set; }
        public List<Emails> Emailslist { get; set; }
        public List<Contactnos> Contactnoslist { get; set; }
        public List<Websites> websiteslist { get; set; }
    }
    public class Contactno
    {
       
        public string number_type { get; set; }
        public string UDID { get; set; }
        public string number { get; set; }  
        public int id { get; set; }
        public bool IsExist  { get; set; }
       
        public Contactno(string x, string y)
       {
           this.number_type = x;
           this.number = y;
      }
    }
    public class Emails
    {
        public string email_type { get; set; }
        public string type_name { get; set; }
        public string email { get; set; }  
        public int id { get; set; }
        public string UDID { get; set; }
        public bool IsExist { get; set; }
     
    }
    public class Contactnos
    {
        public string UDID { get; set; }
        public bool IsExist { get; set; }
        public string number_type { get; set; }
        public string type_name { get; set; }
        public string number { get; set; }  
        public int id { get; set; }
    }
    public class Validation
    {
        public string Vcard_id { get; set; }
        public bool IsExist { get; set; }
    }
    public class AddressList
    {
        public string Address_type { get; set; }
        public string Address { get; set; }
        List<AddressList> addressList = new List<AddressList>();
        public AddressList(string x, string y)
        {
            addressList.Add(new AddressList(x, y));
        }
    }
    public class EmailList
    {
        public int id { get; set; }
        public string email_type { get; set; }
        public string email { get; set; }
        public string UDID { get; set; }
        public bool IsExist { get; set; }
        public EmailList(string x, string y)
        {
            this.email_type = x;
            this.email = y;
        }
    }
    public class URL
    {
        public int id { get; set; }
        public string URLs { get; set; }
        public bool IsExist { get; set; }
        public URL(string x)
        {
            this.URLs = x;
        }

    }
    public class Websites
    {
        public int id { get; set; }
        public string URLs { get; set; }
    }
	public class Adress
	{
        public int id { get; set; }
		public string street1 { get; set; }
        public string type_name { get; set; }
        public string address_type { get; set; }
        public string street2 { get; set; }
		public string jobtitle { get; set; }
		public string city { get; set; }
		public string state { get; set; }
		public string zipcode { get; set; }
		public string country { get; set; }
      
	}
    public class VCF
    {
        public string file_name { get; set; }
        public string vcf_id { get; set; }
        public VCF(string x)
        {
            this.file_name = x;
        }
    }
    public class GUIDLIST
    {
        public string GUID { get; set; }
        public string user_id { get; set; }
        public string IsPersonal { get; set; }
    }
    public class Result
    {
        public Result()
		{
			
                DeletedGUID = new List<GUIDLIST>();
                contact = new List<ContactList>();
		} 
        public List <GUIDLIST> DeletedGUID { get; set; }
        public List<ContactList> contact { get; set; }
    }

    public class UserZoneInfo
    {
        public string zone { get; set; }
        public string user_id { get; set; }
        public int TotalRows { get; set; }
        public int CurPage { get; set; }
        public int PerPage{ get; set; }
        public int start  { get; set; }
        public int offset{ get; set; }
    }
    public class DownloadContact
    {
        public DownloadContact()
        {
            contact = new List<ContactList>();
           
        }
        public int TotalRows { get; set; }
        public List<ContactList> contact {get; set;} 
    }



}
