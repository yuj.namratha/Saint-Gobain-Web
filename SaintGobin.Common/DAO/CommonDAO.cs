﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Script.Serialization;

namespace SaintGobin.Common.DAO
{
    public class CommonDAO
    {
        DBConnection db = new DBConnection();

        #region Validation for Is Exist
        public bool ValidateWebsite(string table_name, string field_Name, string value,string filed_Name1,string value1)
        {
            try
            {
                string query = "IF EXISTS (SELECT * FROM " + table_name + " WHERE " + field_Name + " = '" + value + "' and " + filed_Name1 + " = '" + value1 + "' and isdelete='0') select 1 Else select 0";

                int result = Convert.ToInt32(db.executeScalarQuery(query, null));
                if (result == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool Validate(string table_name,string field_Name,string value)
        {
            try
            {
                string query = "IF EXISTS (SELECT * FROM " + table_name + " WHERE " + field_Name + " = '" + value + "' and isdelete='0') select 1 Else select 0";

                int result = Convert.ToInt32(db.executeScalarQuery(query, null));
                if (result == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        public bool ValidateCommon(string table_name, string field_Name, string value)
        {
            try
            {
                string query = "IF EXISTS (SELECT * FROM " + table_name + " WHERE " + field_Name + " = '" + value + "' ) select 1 Else select 0";

                int result = Convert.ToInt32(db.executeScalarQuery(query, null));
                if (result == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Fetch  Primary Id
        public int GetPrimaryID(string table_name, string field_Name, string value)
        {
            int id = 0;
            try
            {
                string query = " SELECT id FROM "+table_name +" WHERE "+ field_Name +" = '" + value + "' and IsDelete='0' ";
                SqlParameter[] param = new SqlParameter[1];

                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    id = Convert.ToInt32(dr[0].ToString());
                }
                return id;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Update table
        public bool UpdateTable(string table_name, string field_Name, string value, string field_Name1, string value1,string field_Name2, string value2)
        {
          
            try
            {
                //string query = " update " + table_name + " set isdelete=1 ," + field_Name1 + " = '" + value1 + "'," + field_Name2 + " = '" + value2 + "'WHERE " + field_Name + " = '" + value + "'   ";
                //bool valid = db.executeUpdateQuery(query, null);

                SqlParameter[] param = new SqlParameter[7];
                string query = "DeleteRecords";
                param[0] = new SqlParameter("@table_names", SqlDbType.VarChar);
                param[1] = new SqlParameter("@field_Name1", SqlDbType.VarChar);
                param[2] = new SqlParameter("@field_Name2", SqlDbType.VarChar);
                param[3] = new SqlParameter("@value2", SqlDbType.VarChar);
                param[4] = new SqlParameter("@value1", SqlDbType.VarChar);
                param[5] = new SqlParameter("@field_Name", SqlDbType.VarChar);
                param[6] = new SqlParameter("@value", SqlDbType.VarChar);

                param[0].Value = string.IsNullOrEmpty(table_name) ? (object)DBNull.Value : table_name;
                param[1].Value = string.IsNullOrEmpty(field_Name1) ? (object)DBNull.Value : field_Name1;
                param[2].Value = string.IsNullOrEmpty(field_Name2) ? (object)DBNull.Value : field_Name2;
                param[3].Value = string.IsNullOrEmpty(value2) ? (object)DBNull.Value : value2;
                param[4].Value = string.IsNullOrEmpty(value1) ? (object)DBNull.Value : value1;
                param[5].Value = string.IsNullOrEmpty(field_Name) ? (object)DBNull.Value : field_Name;
                param[6].Value = string.IsNullOrEmpty(value) ? (object)DBNull.Value : value;

                bool valid = db.executeUpdateQuery(query, param);

                return valid;
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool UpdateTables(string table_name, string field_Name, string value, string field_Name1, string value1, string field_Name2, string value2)
        {

            try
            {
                //string query = " update " + table_name + " set isdelete=1 ," + field_Name1 + " = '" + value1 + "'," + field_Name2 + " = '" + value2 + "'WHERE " + field_Name + " = '" + value + "'   ";
                //bool valid = db.executeUpdateQuery(query, null);

                SqlParameter[] param = new SqlParameter[7];
                string query = "DeleteRecordsNEW";
                param[0] = new SqlParameter("@table_names", SqlDbType.VarChar);
                param[1] = new SqlParameter("@field_Name1", SqlDbType.VarChar);
                param[2] = new SqlParameter("@field_Name2", SqlDbType.VarChar);
                param[3] = new SqlParameter("@value2", SqlDbType.VarChar);
                param[4] = new SqlParameter("@value1", SqlDbType.VarChar);
                param[5] = new SqlParameter("@field_Name", SqlDbType.VarChar);
                param[6] = new SqlParameter("@value", SqlDbType.VarChar);

                param[0].Value = string.IsNullOrEmpty(table_name) ? (object)DBNull.Value : table_name;
                param[1].Value = string.IsNullOrEmpty(field_Name1) ? (object)DBNull.Value : field_Name1;
                param[2].Value = string.IsNullOrEmpty(field_Name2) ? (object)DBNull.Value : field_Name2;
                param[3].Value = string.IsNullOrEmpty(value2) ? (object)DBNull.Value : value2;
                param[4].Value = string.IsNullOrEmpty(value1) ? (object)DBNull.Value : value1;
                param[5].Value = string.IsNullOrEmpty(field_Name) ? (object)DBNull.Value : field_Name;
                param[6].Value = string.IsNullOrEmpty(value) ? (object)DBNull.Value : value;

                bool valid = db.executeUpdateQuery(query, param);

                return valid;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region Get VcardID and Name
        public string GetVcardIDandName(string table_name, string field_Name, string value)
        {
            string id = null;
            string name = null;
            try
            {
                string query = " SELECT GUID FROM " + table_name + " WHERE " + field_Name + " = '" + value + "' and isdelete='0' ";
                SqlParameter[] param = new SqlParameter[1];

                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    id =(dr[0].ToString());
                }
                if (id != null)
                {
                    string query1 = " SELECT GU_id FROM GUIDMaster WHERE id= '" + id + "' and isdelete='0' ";
                    SqlDataReader dr1 = db.executeSelectQuery(query1, null);
                    while (dr1.Read())
                    {
                        name = (dr1[0].ToString());
                    }
                }
                return name;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public string GetVcardID(string table_name, string field_Name, string value)
        {
            string name = null;
            try
            {
                string query = " SELECT id FROM " + table_name + " WHERE " + field_Name + " = '" + value + "' ";
                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    name = (dr[0].ToString());
                }
               
                return name;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Server Log
        public void WriteToFile(string text)
        {
            string targetFolder = HttpContext.Current.Server.MapPath("~/ServerLog");
            if (!Directory.Exists(targetFolder))
            {
                Directory.CreateDirectory(targetFolder);
            }
            string targetPath = Path.Combine(targetFolder, "ServiceLog.txt");
            String folder = Path.GetDirectoryName(targetPath);
            String fileName = Path.GetFileNameWithoutExtension(targetPath);
            String extension = Path.GetExtension(targetPath);
            String newName = fileName + "_" + DateTime.Now.ToString("dd-MM-yyyy-hh-mm-ss") + extension;
            String path = folder + "\\" + newName;

            using (StreamWriter writer = new StreamWriter(path, true))
            {
                writer.WriteLine(string.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")) + Environment.NewLine);
                writer.Close();
            }
        }
        #endregion

        #region Send Notification
        public bool SendNotification(int user_id, string Notification)
        {
            try
            {
                if (user_id != 0)
                {
                    var applicationID = ConfigurationManager.AppSettings["LegacyKey"];
                    var senderId = ConfigurationManager.AppSettings["SenderID"];

                    string deviceId = null;
                    deviceId = this.Get_GcmID(user_id);
                    if (deviceId != null)
                    {
                            WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                            tRequest.Method = "post";
                            tRequest.ContentType = "application/json";
                            var data = new

                            {
                                to = deviceId,
                                notification = new
                                {
                                    body = Notification,
                                    title = "Saint Gobain Notification",
                                    icon = "myicon"
                                }
                            };

                            var serializer = new JavaScriptSerializer();

                            var json = serializer.Serialize(data);

                            Byte[] byteArray = Encoding.UTF8.GetBytes(json);

                            tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));

                            tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                            tRequest.ContentLength = byteArray.Length;


                            using (Stream dataStream = tRequest.GetRequestStream())
                            {

                                dataStream.Write(byteArray, 0, byteArray.Length);


                                using (WebResponse tResponse = tRequest.GetResponse())
                                {

                                    using (Stream dataStreamResponse = tResponse.GetResponseStream())
                                    {

                                        using (StreamReader tReader = new StreamReader(dataStreamResponse))
                                        {

                                            String sResponseFromServer = tReader.ReadToEnd();

                                            string str = sResponseFromServer;

                                        }
                                    }
                                }
                            }
                        }
                    }

                return true;
                }
            

            catch (Exception e)
            {
                this.WriteToFile("Notification Error-" + e.Message);
                return false;
            }
        }
        public string Get_GcmID(int user_id)
        {
            SqlCommand cmd = new SqlCommand();

            string GCM;
            try
            {
                cmd.CommandText = "select  gsm_id from tbldevice where sgid='" + user_id + "' ";
                cmd.Connection = db.openConnection();
                var result = cmd.ExecuteScalar();
                if (result != null)
                {
                    GCM = result.ToString();
                }
                else
                {
                    GCM = null;
                }
                db.closeConnection();
            }
            catch (Exception)
            {
                db.closeConnection();
                throw;

            }
            finally
            {
                db.closeConnection();
            }
            return GCM;
        }
        #endregion

        #region Sync Notification
        public bool SyncNotification()
        {
            string OldDate = DateTime.Today.AddDays(-3).ToString("yyyy-MM-dd");
            List<User> sync = new List<User>();
            bool valid = true;
            try
            {
                string query = "select user_id from syncdetails where CONVERT(date,syncdate)<='" + OldDate + "'";
                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    sync.Add(new User { User_id = Convert.ToInt32(dr[0]) });
                }
                if (sync.Count != 0)
                {
                    foreach (var item in sync)
                    {
                        if (item.User_id != 0)
                        {
                           valid=this.SendNotification(item.User_id, "Please Sync your conacts");
                        }
                    }
                }
                return valid;
            }
            catch (Exception e)
            {
                this.WriteToFile("Sync Notification Error-" + e.Message);
                valid = false;
                return valid;
            }
            finally
            {
                db.closeConnection();
            }
        }

        #endregion

        #region Fetch  Primary Id
        public string GetSGID(string  user_id)
        {
            string id = "0";
            try
            {
                string query = " SELECT SG_id from usermaster WHERE  id = '" + user_id + "'";
                SqlParameter[] param = new SqlParameter[1];

                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    id = Convert.ToString(dr[0].ToString());
                }
                return id;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

    }
}
