﻿using PhoneNumbers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common.DAO
{
    public class ContactsDAO
    {
        DBConnection db = new DBConnection();
        CommonDAO commonDAO = new CommonDAO();

        #region Add Contact Master Information
        public bool AddContacts(Contact contact)
        {
            List<GUIDLIST> GUID = new List<GUIDLIST>();

          

            bool Isvalid = false;
            try
            {
                contact.Contactnoslist = contact.Contactnoslist.Distinct().ToList();
                contact.Contactnoslist.RemoveAll(x => x.number == null); 
                //Validate Phone Number
                var vcard = (List<string>)null;
                if (contact.Contactnoslist != null)
                {
                    for (int i = 0; i < contact.Contactnoslist.Count(); i++)
                    {
                        Validation comparison_result = this.IsValid(contact.Contactnoslist[i].number);
                        if (comparison_result != null)
                        {
                            contact.Contactnoslist[i].IsExist = comparison_result.IsExist;
                            comparison_result.Vcard_id = comparison_result.Vcard_id;
                            contact.Contactnoslist[i].UDID = comparison_result.Vcard_id;
                        }
                    }
                    vcard = contact.Contactnoslist.Where(t => t.IsExist == true).Select(t => t.UDID).ToList();// Duplicates in Phone(fetch vcf file name)

                }


                //Validate Email
                var vcards = (List<string>)null;
                if (contact.Emailslist != null)
                {

                    for (int i = 0; i < contact.Emailslist.Count(); i++)
                    {
                        contact.Emailslist[i].IsExist = commonDAO.Validate("email", "email", contact.Emailslist[i].email);
                        if (contact.Emailslist[i].IsExist)
                        {
                            contact.Emailslist[i].UDID = commonDAO.GetVcardIDandName("email", "email", contact.Emailslist[i].email);
                        }
                    }
                    vcards = contact.Emailslist.Where(t => t.IsExist == true).Select(t => t.UDID).ToList();// Duplicates in Email(fetch vcf file name)
                }


               

                # region All VCF Files with similar Email/Contact Number
                List<VCF> list = new List<VCF>();
                if (vcard != null)
                {
                    foreach (var item in vcard)
                    {
                        list.Add(new VCF(item));
                    }
                }
                if (vcards != null)
                {
                    foreach (var item in vcards)
                    {
                        list.Add(new VCF(item));
                    }
                }

                #endregion
                list.RemoveAll(m => m.file_name == null );
                Isvalid=this.GenerateVCF(list, contact, contact.SGID); 
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            return Isvalid;
        }
        #endregion

        #region Edit Contact Information
        public bool EditContact(Contact contact)
        {
          
            bool Isvalid = false;
            try
            {
                Isvalid = commonDAO.UpdateTable("GUIDMaster", "id", contact.id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("ContactMaster", "GUID", contact.id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("contactno", "GUID", contact.id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("email", "GUID", contact.id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("website", "guid", contact.id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                #region DB Save
                #region SaveVCF
                int values = 0;
                SqlParameter[] param = new SqlParameter[7];
                string query = "spVcfSave";
                //param[0] = new SqlParameter("@name", SqlDbType.VarChar);
                param[0] = new SqlParameter("@ContactID", SqlDbType.Int);
                param[1] = new SqlParameter("@ID", SqlDbType.Int);
                param[2] = new SqlParameter("@GU_id", SqlDbType.VarChar);
                param[3] = new SqlParameter("@zone", SqlDbType.VarChar);
                param[4] = new SqlParameter("@SG_id", SqlDbType.VarChar);
                param[5] = new SqlParameter("@added_by", SqlDbType.VarChar);
                param[6] = new SqlParameter("@IsUpdated", SqlDbType.Bit);

                param[0].Direction = ParameterDirection.Output;
                param[1].Value = 0;
                param[2].Value = contact.GUID;
                param[3].Value = contact.zone;
                param[4].Value = string.IsNullOrEmpty(contact.SGID) ? (object)DBNull.Value : contact.SGID.ToString();
                param[5].Value = contact.User_id;
                param[6].Value = false;
                Isvalid = db.executeInsertQuery(query, param);

                if (Isvalid)
                {
                    values = Convert.ToInt32(param[0].Value.ToString());
                }

                #endregion

                #region Contact Master Save

                int results = this.SaveContactMaster(contact.User_id, contact.firstname, contact.middlename, contact.lastname, contact.prefix, contact.sufix, contact.company, contact.jobtitle, values, contact.SGID);
                if (results == 0)
                {
                    Isvalid = false;
                }
                else
                {
                    Isvalid = true;
                }
                #endregion

                #region Email Save
                foreach (var item in contact.Emailslist )
                {
                    int type_id = this.SaveType(item.type_name);
                   

                    int result = SaveEmail(contact.User_id, item.email, values, type_id);
                    if (result == 0)
                    {
                        Isvalid = false;
                    }
                    else
                    {
                        Isvalid = true;
                    }
                }
                #endregion

                #region Phone Number Save

                foreach (var item in contact.Contactnoslist )
                {
                    int type_id = this.SaveType(item.type_name.ToString());
                    int result = this.SaveContactNo(contact.User_id, item.number, values, type_id);
                    if (result == 0)
                    {
                        Isvalid = false;
                    }
                    else
                    {
                        Isvalid = true;
                    }
                }
                #endregion

                #region URL Save
                foreach (var item in contact.websiteslist)
                {
                   
                    int result = this.SaveWebsite(contact.User_id, item.URLs, values);
                    if (result == 0)
                    {
                        Isvalid = false;
                    }
                    else
                    {
                        Isvalid = true;
                    }
                }
                #endregion

                #region Address Save
                foreach (var item in contact.Adresslist )
                {
                    int type_id = this.SaveType(item.type_name.ToString());
                    int result = this.SaveAddress(contact.User_id, item.street1, item.street2, item.city, item.state, item.zipcode, item.country, values, type_id);
                    if (result == 0)
                    {
                        Isvalid = false;
                    }
                    else
                    {
                        Isvalid = true;
                    }
                }
                #endregion

             
               
                #endregion
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            return Isvalid;
        }
        #endregion

        #region Edit Contact Information
        public bool DeleteContact(int  id)
        {

            bool Isvalid = false;
            try
            {
                Isvalid = commonDAO.UpdateTables("GUIDMaster", "id", id.ToString(), "updatedby", APPSession.Current.User_id.ToString()    , "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("ContactMaster", "GUID", id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("contactno", "GUID", id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("email", "GUID", id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("website", "guid", id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                Isvalid = commonDAO.UpdateTable("address", "GUID", id.ToString(), "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            return Isvalid;
        }
        #endregion

        #region Address

        #region Address Save
        public int SaveAddress(string user_id,string street1, string street2, string city, string state, string zipcode, string country, int vcf_id, int type)
        {

            int value = 0;
            try
            {

                if (!this.ValidateAdress(user_id, street1,  street2,  city,  state,  zipcode,  country,  vcf_id,  type))
                {
                    SqlParameter[] param = new SqlParameter[11];
                    string query = "spAddressSave";
                    param[0] = new SqlParameter("@street1", SqlDbType.VarChar);
                    param[1] = new SqlParameter("@street2", SqlDbType.VarChar);
                    param[2] = new SqlParameter("@city", SqlDbType.VarChar);
                    param[3] = new SqlParameter("@state", SqlDbType.VarChar);
                    param[4] = new SqlParameter("@zipcode", SqlDbType.VarChar);
                    param[5] = new SqlParameter("@country", SqlDbType.VarChar);
                    param[6] = new SqlParameter("@AddressID", SqlDbType.Int);
                    param[7] = new SqlParameter("@ID", SqlDbType.Int);
                    param[8] = new SqlParameter("@Vcfid", SqlDbType.Int);
                    param[9] = new SqlParameter("@added_by", SqlDbType.VarChar);
                    param[10] = new SqlParameter("@type", SqlDbType.Int);


                    param[0].Value = string.IsNullOrEmpty(street1) ? (object)DBNull.Value : street1;
                    param[1].Value = string.IsNullOrEmpty(street2) ? (object)DBNull.Value : street2; ;
                    param[2].Value = string.IsNullOrEmpty(city) ? (object)DBNull.Value : city;
                    param[3].Value = string.IsNullOrEmpty(state) ? (object)DBNull.Value : state;
                    param[4].Value = string.IsNullOrEmpty(zipcode) ? (object)DBNull.Value : zipcode;
                    param[5].Value = string.IsNullOrEmpty(country) ? (object)DBNull.Value : country;
                    param[6].Direction = ParameterDirection.Output;
                    param[7].Value = 0;
                    param[8].Value = (vcf_id);
                    param[9].Value = user_id;
                    param[10].Value = (type);
                    bool result = db.executeInsertQuery(query, param);

                    if (result)
                    {
                        value = Convert.ToInt32(param[6].Value.ToString());
                    }

                }
            
            return value;
                 
               }
            catch (Exception e)
            {

            }
            finally
            {
                db.closeConnection();
            }
            return value;
        }
        #endregion

        #region Address Validation

        public bool ValidateAdress(string user_id,string street1, string street2, string city, string state, string zipcode, string country, int vcf_id, int type)
        {
            try
            {
                string query = "IF EXISTS (select *from address where street1='" + street1 + "' and street2='" + street2 + "' and city='" + city + "'and state='" + state + "' and zipcode='" + zipcode + "'and country='" + country + "'and GUID='" + vcf_id + "' and addedby='" + user_id + "' and type='" + type + "'and isdelete='0'   ) select 1 Else select 0";

                int result = Convert.ToInt32(db.executeScalarQuery(query, null));
                if (result == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateAddress(string street, string Pobox, string Neiborhood, string city, string state, string zipcode, string country)
        {
            try
            {
                string query = "update address set isdelete=1 WHERE street = '" + street + "'and Pobox = '" + Pobox + "'and Neiborhood = '" + Neiborhood + "'and city = '" + city + "'and state = '" + state + "'and zipcode = '" + zipcode + "'and country = '" + country + "'";

                int result = Convert.ToInt32(db.executeScalarQuery(query, null));
                if (result == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Address Table Primary ID
        public int GetAddressID(string street, string Pobox, string Neiborhood, string city, string state, string zipcode, string country)
        {
            int id = 0;
            try
            {
                string query = "SELECT id FROM address WHERE street = '" + street + "'and Pobox = '" + Pobox + "'and Neiborhood = '" + Neiborhood + "'and city = '" + city + "'and state = '" + state + "'and zipcode = '" + zipcode + "'and country = '" + country + "' and IsDelete='0' ";
                SqlParameter[] param = new SqlParameter[1];

                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    id = Convert.ToInt32(dr[0].ToString());
                }
                return id;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
        #endregion

        #region Contact Number
        public int SaveContactNo(string user_id,string contactno,int vcf_id,int type)
        {
            int value = 0;
            try
            {
            if (contactno != null)
            {
                if (!commonDAO.Validate("contactno", "contactno", contactno))
                {
                    SqlParameter[] param = new SqlParameter[6];
                    string query = "spContactNOSave";
                    param[0] = new SqlParameter("@contactno", SqlDbType.VarChar);
                    param[1] = new SqlParameter("@ContactID", SqlDbType.Int);
                    param[2] = new SqlParameter("@ID", SqlDbType.Int);
                    param[3] = new SqlParameter("@Vcfid", SqlDbType.Int);
                    param[4] = new SqlParameter("@added_by", SqlDbType.VarChar);
                    param[5] = new SqlParameter("@type", SqlDbType.Int);

                    param[0].Value = string.IsNullOrEmpty(contactno) ? (object)DBNull.Value : contactno;
                    param[1].Direction = ParameterDirection.Output;
                    param[2].Value = 0;
                    param[3].Value =vcf_id;
                    param[4].Value = user_id;
                    param[5].Value =type;
                    bool result = db.executeInsertQuery(query, param);
                    if (result)
                    {
                        value = Convert.ToInt32(param[1].Value.ToString());
                    }
                }
                else
                {
                    value = commonDAO.GetPrimaryID("contactno", "contactno", contactno);
                }
            }
            return value;
            
               }
            catch (Exception e)
            {

            }
            finally
            {
                db.closeConnection();
            }
            return value;
        }

        #region Fetch All contactno
        public List<Contactnos> getallContact(string value)
        {
            try
            {
                List<Contactnos> contactno = new List<Contactnos>();
                string query = "GetPhoneNumber";
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@contact", SqlDbType.VarChar);
                param[0].Value = string.IsNullOrEmpty(value) ? (object)DBNull.Value : value; ;
                SqlDataReader dr = db.executeSelectQuery(query, param);
                while (dr.Read())
                {
                    contactno.Add(new Contactnos { number = dr[0].ToString() });
                }
                return contactno;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
        }
        #endregion

        #region Validate Contact Number
        public Validation IsValid(string value)
        {
            List<Contactnos> contact = this.getallContact(value);
            contact.RemoveAll(x => x.number == null); 

            Validation validation = new Validation();
            foreach (var item in contact)
            {
                PhoneNumberUtil pnu = PhoneNumberUtil.GetInstance();
                PhoneNumbers.PhoneNumberUtil.MatchType mt = pnu.IsNumberMatch(value, item.number);
                if (mt == PhoneNumbers.PhoneNumberUtil.MatchType.NSN_MATCH || mt == PhoneNumbers.PhoneNumberUtil.MatchType.EXACT_MATCH)
                {
                    validation.IsExist = true;
                    validation.Vcard_id = commonDAO.GetVcardIDandName("contactno", "contactno", item.number);
                    break;
                }
                else
                {
                    validation.IsExist = false;
                }
            }
            return validation;
        }
        #endregion
        #endregion

        #region Contact Master
        public int SaveContactMaster(string user_id,string first_name, string middle_name, string last_name,string prefix,string sufix,string org,string job,int vcf_id,string sg_id)
        {
            int value = 0;
            try
            {
          
                        SqlParameter[] param = new SqlParameter[12];
                        string query = "spSaveContactMaster";
                        param[0] = new SqlParameter("@firstname", SqlDbType.VarChar);
                        param[1] = new SqlParameter("@middlename", SqlDbType.VarChar);
                        param[2] = new SqlParameter("@lastname", SqlDbType.VarChar);
                        param[3] = new SqlParameter("@prefix", SqlDbType.VarChar);
                        param[4] = new SqlParameter("@sufix", SqlDbType.VarChar);
                        param[5] = new SqlParameter("@organisation", SqlDbType.VarChar);
                        param[6] = new SqlParameter("@jobtitle", SqlDbType.VarChar);
                        param[7] = new SqlParameter("@Vcfid", SqlDbType.Int);
                        param[8] = new SqlParameter("@addedby", SqlDbType.Int);
                        param[9] = new SqlParameter("@ContactID", SqlDbType.Int);
                        param[10] = new SqlParameter("@ID", SqlDbType.Int);
                        param[11] = new SqlParameter("@sg_id", SqlDbType.VarChar);

                       param[0].Value = string.IsNullOrEmpty(first_name) ? (object)DBNull.Value : first_name;
                       param[1].Value = string.IsNullOrEmpty(middle_name) ? (object)DBNull.Value : middle_name;
                       param[2].Value = string.IsNullOrEmpty(last_name) ? (object)DBNull.Value : last_name;
                       param[3].Value = string.IsNullOrEmpty(prefix) ? (object)DBNull.Value : prefix;
                       param[4].Value = string.IsNullOrEmpty(sufix) ? (object)DBNull.Value : sufix;
                       param[5].Value = string.IsNullOrEmpty(org) ? (object)DBNull.Value : org;
                       param[6].Value = string.IsNullOrEmpty(job) ? (object)DBNull.Value : job;
                       param[7].Value =vcf_id;
                       param[8].Value =user_id;
                       param[9].Direction = ParameterDirection.Output;
                       param[10].Value = 0;
                       param[11].Value = sg_id;
                        bool result = db.executeInsertQuery(query, param);
                        if (result)
                        {
                            value = Convert.ToInt32(param[9].Value.ToString());
                        }
                 
                return value;

            }
            catch (Exception e)
            {

            }
            finally
            {
                db.closeConnection();
            }
            return value;
        }

  
   
        #endregion
        
        #region Generate VC
        public bool GenerateVCF(List<VCF> vcflist, Contact contact, string SGID)
        {
            bool IsValid = false;
            #region Compare 

            vcflist = vcflist.GroupBy(x => x.file_name).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records

            List<Contactnos> phone = new List<Contactnos>();
            List<Contactnos> phoneTemp = new List<Contactnos>();
            List<Emails> email = new List<Emails>();
            List<Emails> emailTemp = new List<Emails>();
            List<Websites> uRL = new List<Websites>();
            List<Websites> uRLTemp = new List<Websites>();
            List<Adress> address = new List<Adress>();
            List<Adress> addressTemp = new List<Adress>();
           
            Contact firstFile_Content = new Contact();
            firstFile_Content=contact;
            Contact SecondFile_Content = new Contact();
            

            if (firstFile_Content.contactlist != null)
            {
                phone = firstFile_Content.Contactnoslist.Distinct().ToList();// Eliminate Duplicate Records
                phone.RemoveAll(x => x.number == null); //Remove Null Entry Based on Field Name
            }

            if (firstFile_Content.Emailslist != null)
            {
                email = firstFile_Content.Emailslist.Distinct().ToList();// Eliminate Duplicate Records
                email.RemoveAll(x => x.email == null); //Remove Null Entry Based on Field Name
            }

            if (firstFile_Content.URLlist != null)
            {
                uRL = firstFile_Content.websiteslist.Distinct().ToList();// Eliminate Duplicate Records
                uRL.RemoveAll(x => x.URLs == null); //Remove Null Entry Based on Field Name

            }

            if (firstFile_Content.Adresslist != null)
            {
                address = firstFile_Content.Adresslist.Distinct().ToList();// Eliminate Duplicate Records
                address.RemoveAll(x => x.address_type == null && x.city == null && x.country == null && x.state == null && x.zipcode == null && x.street1 == null && x.street2 == null); 
            }
            
            for (int j = 0; j < vcflist.Count; j++)
            {
                if (vcflist.Count >0) 
                {
                    string SecondFile = vcflist[j].file_name; // First File Name Fetch
                    List<Contact> Contact_List = this.GetContactInformationOnGUID(SecondFile);
                  

                    if (Contact_List.Count  != 0)
                    {
                        SecondFile_Content = Contact_List[0];
                        #region Other
                        //if (SecondFile_Content.firstname != null && firstFile_Content.firstname != null)
                        //{
                        //    if (firstFile_Content.firstname.Length >= SecondFile_Content.firstname.Length)
                        //    {
                        //        firstFile_Content.firstname = SecondFile_Content.firstname;
                        //    }
                        //    else
                        //    {
                        // firstFile_Content.firstname = SecondFile_Content.firstname;
                        //    }
                        //}

                        //if (SecondFile_Content.middlename != null && firstFile_Content.middlename != null)
                        //{
                        //    if (firstFile_Content.middlename.Length >= SecondFile_Content.middlename.Length)
                        //    {
                        //        firstFile_Content.middlename = SecondFile_Content.middlename;
                        //    }
                        //    else
                        //    {
                        //        firstFile_Content.middlename = SecondFile_Content.middlename;
                        //    }

                        //}

                        //if (SecondFile_Content.lastname != null && firstFile_Content.lastname != null)
                        //{
                        //    if (firstFile_Content.lastname.Length >= SecondFile_Content.lastname.Length)
                        //    {
                        //        firstFile_Content.lastname = SecondFile_Content.lastname;
                        //    }
                        //    else
                        //    {
                        //        firstFile_Content.lastname = SecondFile_Content.lastname;
                        //    }
                        //}

                        //if (firstFile_Content.prefix == null)
                        //{
                        //    firstFile_Content.prefix = SecondFile_Content.prefix;
                        //}
                        //if (firstFile_Content.sufix == null)
                        //{
                        //    firstFile_Content.sufix = SecondFile_Content.sufix;
                        //}

                        if (firstFile_Content.company == null && SecondFile_Content.company != null)
                        {
                            firstFile_Content.company = SecondFile_Content.company;
                        }

                        if (firstFile_Content.jobtitle == null && SecondFile_Content.jobtitle != null)
                        {
                            firstFile_Content.jobtitle = SecondFile_Content.jobtitle;
                        }


                        #endregion

                        #region Eliminate Duplicates By Combining 2 vcf Phone Numbers
                        if (firstFile_Content.Contactnoslist != null && SecondFile_Content.Contactnoslist != null)
                        {
                            phoneTemp = phone;// Retail Previous assigned value
                            phone = firstFile_Content.Contactnoslist.Union(SecondFile_Content.Contactnoslist != null ? SecondFile_Content.Contactnoslist : Enumerable.Empty<Contactnos>()).ToList(); // Conact 2 List with eliminating duplicates

                            if (phoneTemp != null)
                            {
                                phone = phone.Union(phoneTemp).ToList();

                            }
                            phone = phone.GroupBy(x => x.number).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            phone.RemoveAll(x => x.number == null); //Remove Null Entry Based on Field Name
                            phone = phone.Distinct().ToList();
                        }
                        else if (firstFile_Content.Contactnoslist != null && SecondFile_Content.Contactnoslist == null)
                        {
                            phoneTemp = phone;
                            phone = firstFile_Content.Contactnoslist.ToList();
                            if (phoneTemp != null)
                            {
                                phone = phone.Union(phoneTemp).ToList();

                            }
                            phone = phone.GroupBy(x => x.number).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            phone.RemoveAll(x => x.number == null); //Remove Null Entry Based on Field Name
                            phone = phone.Distinct().ToList();
                        }
                        else if (firstFile_Content.Contactnoslist == null && SecondFile_Content.Contactnoslist != null)
                        {
                            phoneTemp = phone;
                            phone = SecondFile_Content.Contactnoslist.ToList();
                            if (phoneTemp != null)
                            {
                                phone = phone.Union(phoneTemp).ToList();

                            }
                            phone = phone.GroupBy(x => x.number).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            phone.RemoveAll(x => x.number == null); //Remove Null Entry Based on Field Name
                            phone = phone.Distinct().ToList();
                        }

                        #endregion

                        #region Eliminate Duplicates By Combining 2 vcf(Email)
                        if (firstFile_Content.Emailslist != null && SecondFile_Content.Emailslist != null)
                        {
                            emailTemp = email;
                            email = firstFile_Content.Emailslist.Union(SecondFile_Content.Emailslist != null ? SecondFile_Content.Emailslist : Enumerable.Empty<Emails>()).ToList();

                            if (emailTemp != null)
                            {
                                email = email.Union(emailTemp).ToList();

                            }
                            email = email.GroupBy(x => x.email).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            email.RemoveAll(x => x.email == null); //Remove Null Entry Based on Field Name
                            email = email.Distinct().ToList();
                        }
                        else if (firstFile_Content.Emailslist != null && SecondFile_Content.Emailslist == null)
                        {
                            emailTemp = email;
                            email = firstFile_Content.Emailslist.ToList();
                            if (emailTemp != null)
                            {
                                email = email.Union(emailTemp).ToList();

                            }
                            email = email.GroupBy(x => x.email).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            email.RemoveAll(x => x.email == null); //Remove Null Entry Based on Field Name
                            email = email.Distinct().ToList();
                        }
                        else if (firstFile_Content.Emailslist == null && SecondFile_Content.Emailslist != null)
                        {
                            emailTemp = email;
                            email = SecondFile_Content.Emailslist.ToList();
                            if (emailTemp != null)
                            {
                                email = email.Union(emailTemp).ToList();

                            }
                            email = email.GroupBy(x => x.email).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            email.RemoveAll(x => x.email == null); //Remove Null Entry Based on Field Name
                            email = email.Distinct().ToList();
                        }
                        #endregion

                        #region Eliminate Duplicates By Combining 2 vcf(URLS)
                        if (firstFile_Content.websiteslist != null && SecondFile_Content.websiteslist != null)
                        {
                            uRLTemp = uRL;
                            uRL = firstFile_Content.websiteslist.Union(SecondFile_Content.websiteslist != null ? SecondFile_Content.websiteslist : Enumerable.Empty<Websites>()).ToList();
                            if (uRLTemp != null)
                            {
                                uRL = uRL.Union(uRLTemp).ToList();

                            }
                            uRL = uRL.GroupBy(x => x.URLs).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            uRL.RemoveAll(x => x.URLs == null); //Remove Null Entry Based on Field Name
                            uRL = uRL.Distinct().ToList();
                        }
                        else if (firstFile_Content.websiteslist != null && SecondFile_Content.websiteslist == null)
                        {
                            uRLTemp = uRL;
                            uRL = firstFile_Content.websiteslist.ToList();
                            if (uRLTemp != null)
                            {
                                uRL = uRL.Union(uRLTemp).ToList();

                            }
                            uRL = uRL.GroupBy(x => x.URLs).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            uRL.RemoveAll(x => x.URLs == null); //Remove Null Entry Based on Field Name
                            uRL = uRL.Distinct().ToList();
                        }
                        else if (firstFile_Content.websiteslist == null && SecondFile_Content.websiteslist != null)
                        {
                            uRLTemp = uRL;
                            uRL = SecondFile_Content.websiteslist.ToList();
                            if (uRLTemp != null)
                            {
                                uRL = uRL.Union(uRLTemp).ToList();

                            }
                            uRL = uRL.GroupBy(x => x.URLs).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                            uRL.RemoveAll(x => x.URLs == null); //Remove Null Entry Based on Field Name
                            uRL = uRL.Distinct().ToList();
                        }
                        #endregion

                        #region Eliminate Duplicates By Combining 2 vcf(Addresses)
                        if (firstFile_Content.Adresslist != null && SecondFile_Content.Adresslist != null)
                        {
                            addressTemp = address;
                            address = firstFile_Content.Adresslist.Union(SecondFile_Content.Adresslist != null ? SecondFile_Content.Adresslist : Enumerable.Empty<Adress>()).ToList();
                            if (addressTemp != null)
                            {
                                address = address.Union(addressTemp).ToList();
                            }
                            address = address.Distinct().ToList();// Eliminate Duplicate Records
                            address.RemoveAll(x => x.address_type == null && x.city == null && x.country == null && x.state == null && x.zipcode == null && x.street1 == null && x.street2 == null);
                            address = address.GroupBy(d => new { d.country, d.state, d.city, d.street1,d.street2,d.zipcode}).Select(d => d.First()).ToList();
                            address = address.Distinct().ToList();

                        }
                        else if (firstFile_Content.Adresslist != null && SecondFile_Content.Adresslist == null)
                        {
                            addressTemp = address;
                            address = firstFile_Content.Adresslist.ToList();
                            if (addressTemp != null)
                            {
                                address = address.Union(addressTemp).ToList();

                            }
                            address = address.Distinct().ToList();
                            address.RemoveAll(x => x.address_type == null && x.city == null && x.country == null && x.state == null && x.zipcode == null && x.street1 == null && x.street2 == null);
                            address = address.GroupBy(d => new { d.country, d.state, d.city, d.street1, d.street2, d.zipcode }).Select(d => d.First()).ToList();
                            address = address.Distinct().ToList();
                        }
                        else if (firstFile_Content.Adresslist == null && SecondFile_Content.Adresslist != null)
                        {
                            addressTemp = address;
                            address = SecondFile_Content.Adresslist.ToList();
                            if (addressTemp != null)
                            {
                                address = address.Union(addressTemp).ToList();

                            }
                            address = address.Distinct().ToList();// Eliminate Duplicate Records
                            address.RemoveAll(x => x.address_type == null && x.city == null && x.country == null && x.state == null && x.zipcode == null && x.street1 == null && x.street2 == null);
                            address = address.GroupBy(d => new { d.country, d.state, d.city, d.street1, d.street2, d.zipcode }).Select(d => d.First()).ToList();
                            address = address.Distinct().ToList();
                        }
                        #endregion
                    }
                  


                }
            }
            #endregion

            #region Phone Number

             List<Contactnos> NumbertoDelte = new List<Contactnos>();
            foreach (var item in phone)
            {
                List<Contactnos> contacts = this.getallContact(item.number);
                Validation validation = new Validation();
                foreach (var items in contacts)
                {
                    PhoneNumberUtil pnu = PhoneNumberUtil.GetInstance();
                    PhoneNumbers.PhoneNumberUtil.MatchType mt = pnu.IsNumberMatch(item.number, items.number);
                    if (mt == PhoneNumbers.PhoneNumberUtil.MatchType.NSN_MATCH || mt == PhoneNumbers.PhoneNumberUtil.MatchType.EXACT_MATCH)
                    {
                        NumbertoDelte.Add(items);
                    }
                }
            }

            #endregion
            if (!commonDAO.ValidateCommon("guidmaster", "GU_id", contact.GUID))
            {
                #region DB Save
                #region SaveVCF



                int values = 0;
                SqlParameter[] param = new SqlParameter[7];
                string query = "spVcfSave";
                //param[0] = new SqlParameter("@name", SqlDbType.VarChar);
                param[0] = new SqlParameter("@ContactID", SqlDbType.Int);
                param[1] = new SqlParameter("@ID", SqlDbType.Int);
                param[2] = new SqlParameter("@GU_id", SqlDbType.VarChar);
                param[3] = new SqlParameter("@zone", SqlDbType.VarChar);
                param[4] = new SqlParameter("@SG_id", SqlDbType.VarChar);
                param[5] = new SqlParameter("@added_by", SqlDbType.VarChar);
                param[6] = new SqlParameter("@IsUpdated", SqlDbType.Bit);

                param[0].Direction = ParameterDirection.Output;
                param[1].Value = 0;
                param[2].Value = contact.GUID;
                param[3].Value = contact.zone;
                param[4].Value = string.IsNullOrEmpty(SGID) ? (object)DBNull.Value : SGID.ToString();
                param[5].Value = contact.User_id;
                if (vcflist.Count >= 1)
                {
                    param[6].Value = true;
                }
                else
                {
                    param[6].Value = false;
                }


                IsValid = db.executeInsertQuery(query, param);

                if (IsValid)
                {
                    values = Convert.ToInt32(param[0].Value.ToString());
                }

                #endregion

                #region Contact Master Save

                int results = this.SaveContactMaster(contact.User_id, firstFile_Content.firstname, firstFile_Content.middlename, firstFile_Content.lastname, firstFile_Content.prefix, firstFile_Content.sufix, firstFile_Content.company, firstFile_Content.jobtitle, values, SGID);
                if (results == 0)
                {
                    IsValid = false;
                }
                else
                {
                    IsValid = true;
                }
                #endregion

                if (IsValid)
                {
                    #region Email Save
                    foreach (var item in email)
                    {
                        int type_id = this.SaveType(item.type_name);
                        if (commonDAO.Validate("email", "email", item.email))
                        {
                            IsValid = commonDAO.UpdateTable("emailnew", "email", item.email, "updatedby", contact.User_id, "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());
                        }

                        int result = SaveEmail(contact.User_id, item.email, values, type_id);
                        if (result == 0)
                        {
                            IsValid = false;
                        }
                        else
                        {
                            IsValid = true;
                        }
                    }
                    #endregion

                    #region Phone Number Save
                    foreach (var item in NumbertoDelte)
                    {
                        IsValid = commonDAO.UpdateTable("contactnonew", "contactno", item.number, "updatedby", contact.User_id, "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());

                        //string id = commonDAO.GetVcardID("contactno", "contactno", item.number);
                        //IsValid = commonDAO.UpdateTable("GUIDMaster", "id", id, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                        //IsValid = commonDAO.UpdateTable("ContactMaster", "GUID", id, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in contact master table
                        //IsValid = commonDAO.UpdateTable("address", "GUID", id, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in adress table
                    }
                    foreach (var item in phone)
                    {
                        int type_id = this.SaveType(item.type_name.ToString());
                        int result = this.SaveContactNo(contact.User_id, item.number, values, type_id);
                        if (result == 0)
                        {
                            IsValid = false;
                        }
                        else
                        {
                            IsValid = true;
                        }
                    }
                    #endregion

                    #region URL Save
                    foreach (var item in uRL)
                    {
                        if (commonDAO.Validate("website", "website", item.URLs))
                        {
                            IsValid = commonDAO.UpdateTable("websitenew", "website", item.URLs, "updatedby", contact.User_id, "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());
                        }
                        int result = this.SaveWebsite(contact.User_id, item.URLs, values);
                        if (result == 0)
                        {
                            IsValid = false;
                        }
                        else
                        {
                            IsValid = true;
                        }
                    }
                    #endregion

                    #region Address Save
                    foreach (var item in address)
                    {
                        int type_id = this.SaveType(item.type_name.ToString());
                        int result = this.SaveAddress(contact.User_id, item.street1, item.street2, item.city, item.state, item.zipcode, item.country, values, type_id);
                        if (result == 0)
                        {
                            IsValid = false;
                        }
                        else
                        {
                            IsValid = true;
                        }
                    }
                    #endregion

                    List<GUIDLIST> guid = new List<GUIDLIST>();
                    List<ContactList> con = new List<ContactList>();
                    #region VCF File Delete
                    for (int i = 0; i < vcflist.Count; i++)
                    {

                        string FileToDelete = System.IO.Path.GetFileName(vcflist[i].file_name);
                        if (commonDAO.Validate("GUIDMaster", "GU_id", FileToDelete))
                        {
                            IsValid = commonDAO.UpdateTable("GUIDMaster", "GU_id", FileToDelete, "updatedby", contact.User_id, "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                            string vcf_id = commonDAO.GetVcardID("GUIDMaster", "GU_id", FileToDelete); // get vcf ID based on File name
                            guid.Add(new GUIDLIST() { GUID = vcflist[i].file_name });
                            IsValid = commonDAO.UpdateTable("ContactMaster", "GUID", vcf_id, "updatedby", contact.User_id, "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in contact master table
                            IsValid = commonDAO.UpdateTable("address", "GUID", vcf_id, "updatedby", contact.User_id, "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in adress table
                            // IsValid = commonDAO.UpdateTable("website_vcf", "GUID", vcf_id, "updatedby", contact.User_id, "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in website_vcf table

                        }

                    }

                    #endregion
                }

                #endregion
            }
            else
            {
                IsValid = true;
            }
            return IsValid;
        }
        #endregion

        #region Email
        public int SaveEmail(string user_id,string email, int vcf_id, int type)
        {
            int value = 0;
            try
            {
            if (email != null)
            {
                if (!commonDAO.Validate("email", "email", email))
                {
                    SqlParameter[] param = new SqlParameter[6];
                    string query = "spEmailSave";
                    param[0] = new SqlParameter("@email", SqlDbType.VarChar);
                    param[1] = new SqlParameter("@emailID", SqlDbType.Int);
                    param[2] = new SqlParameter("@ID", SqlDbType.Int);
                    param[3] = new SqlParameter("@Vcfid", SqlDbType.Int);
                    param[4] = new SqlParameter("@added_by", SqlDbType.VarChar);
                    param[5] = new SqlParameter("@type", SqlDbType.Int); 

                    param[0].Value = string.IsNullOrEmpty(email) ? (object)DBNull.Value : email;
                    param[1].Direction = ParameterDirection.Output;
                    param[2].Value = 0;
                    param[3].Value =(vcf_id) ;
                    param[4].Value =user_id;
                    param[5].Value = type;
                    bool result = db.executeInsertQuery(query, param);
                    if (result)
                    {
                        value = Convert.ToInt32(param[1].Value.ToString());
                    }
                }
                else
                {
                    value = commonDAO.GetPrimaryID("email", "email", email);
                }
            }
            return value;

               }
            catch (Exception e)
            {

            }
            finally
            {
                db.closeConnection();
            }
            return value;
        }


        #endregion

        #region Website
        public int SaveWebsite(string user_id,string website, int vcf_id)
        {
            int value = 0;
            try 
            {
            
                if (website != null)
                {
                    if (!commonDAO.Validate("website", "website", website))
                    // if (!commonDAO.ValidateWebsite("website", "website", website,"GUID",vcf_id))
                    {
                        SqlParameter[] param = new SqlParameter[5];
                        string query = "spWebsiteSave";
                        param[0] = new SqlParameter("@website", SqlDbType.VarChar);
                        param[1] = new SqlParameter("@websiteID", SqlDbType.Int);
                        param[2] = new SqlParameter("@ID", SqlDbType.Int);
                        param[3] = new SqlParameter("@added_by", SqlDbType.VarChar);
                        param[4] = new SqlParameter("@guid", SqlDbType.Int);
                       

                        param[0].Value = string.IsNullOrEmpty(website) ? (object)DBNull.Value : website;
                        param[1].Direction = ParameterDirection.Output;
                        param[2].Value = 0;
                        param[3].Value = user_id;
                        param[4] .Value= vcf_id;
                      
                        bool result = db.executeInsertQuery(query, param);
                        if (result)
                        {
                            value = Convert.ToInt32(param[1].Value.ToString());
                        }
                    }
                    else
                    {
                        value = this.GetPrimaryID("website", "website", website);
                    }
                }
                //if (value != 0)
                //{
                //    SqlParameter[] para = new SqlParameter[4];
                //    string query = "spWebsiteVCFSave";
                //    para[0] = new SqlParameter("@website_id", SqlDbType.VarChar);
                //    para[1] = new SqlParameter("@vcf_id", SqlDbType.Int);
                //    para[2] = new SqlParameter("@ID", SqlDbType.Int);
                //    para[3] = new SqlParameter("@added_by", SqlDbType.VarChar);
                //    para[0].Value = value;
                //    para[1].Value = vcf_id;
                //    para[2].Direction = ParameterDirection.Output;
                //    para[3].Value = user_id;
                //    bool result = db.executeInsertQuery(query, para);
                //    return value;
                //}
               
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.closeConnection();
            }
            return value;
           
        }
        #endregion

        #region Type
        public int SaveType(string type)
        {
            int type_id = 0;
            try
            {
                if (type != null)
                {
                    if (!this.Validate("type", "type", type))
                    {
                        SqlParameter[] param = new SqlParameter[3];
                        string query = "spTypeSave";
                        param[0] = new SqlParameter("@type", SqlDbType.VarChar);
                        param[1] = new SqlParameter("@typeID", SqlDbType.Int);
                        param[2] = new SqlParameter("@ID", SqlDbType.Int);

                        param[0].Value = string.IsNullOrEmpty(type) ? (object)DBNull.Value : type;
                        param[1].Direction = ParameterDirection.Output;
                        param[2].Value = 0;
                        bool result = db.executeInsertQuery(query, param);
                        if (result)
                        {
                            type_id = Convert.ToInt32(param[1].Value.ToString());
                        }
                    }
                    else
                    {
                        type_id = this.GetPrimaryID("type", "type", type);
                    }
                }
                return type_id;
            }
            catch (Exception e)
            {
              
            }
            finally
            {
                db.closeConnection();
            }
            return type_id;

        }
        #endregion

        #region Validation for Is Exist

        public bool Validate(string table_name, string field_Name, string value)
        {
            try
            {
                string query = "IF EXISTS (SELECT * FROM " + table_name + " WHERE " + field_Name + " = '" + value + "') select 1 Else select 0";

                int result = Convert.ToInt32(db.executeScalarQuery(query, null));
                if (result == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Fetch  Primary Id
        public int GetPrimaryID(string table_name, string field_Name, string value)
        {
            int id = 0;
            try
            {
                string query = " SELECT id FROM " + table_name + " WHERE " + field_Name + " = '" + value + "'";
                SqlParameter[] param = new SqlParameter[1];

                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    id = Convert.ToInt32(dr[0].ToString());
                }
                return id;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Get Contact Information Based On GUID(File Name of the GUID)
        public List<Contact> GetContactInformationOnGUID(string GUID)
        {
            List<Contact> model = new List<Contact>();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@GUID", SqlDbType.VarChar);
                param[0].Value = GUID;
                using (IDataReader reader = db.executeSelectQuery("GetContactInformationBasedOnGUID", param, CommandType.StoredProcedure))
                {
                    while (reader.Read())
                    {
                        Contact contact = null;
                        int vcf_id = Convert.ToInt32(reader["vcf_id"]);
                        contact = model.FirstOrDefault(q => q.vcf_id == vcf_id);
                        if (contact == null)
                        {
                            contact = new Contact();
                            contact.firstname = Convert.ToString(reader["first_name"]);
                            contact.middlename = Convert.ToString(reader["middle_name"]);
                            contact.lastname = Convert.ToString(reader["last_name"]);
                            contact.prefix = Convert.ToString(reader["prefix"]);
                            contact.sufix = Convert.ToString(reader["sufix"]);
                            contact.company = Convert.ToString(reader["organisation"]);
                            contact.jobtitle = Convert.ToString(reader["title"]);
                            contact.SGID = Convert.ToString(reader["SGID"]);
                            contact.vcf_id = Convert.ToInt32(reader["vcf_id"]);
                          //  contact.vcf_name = Convert.ToString(reader["vcfname"]);
                        }
                        Contactnos contactno = null;
                        if (reader["contactno_id"] != DBNull.Value)
                        {
                            int contactno_id = Convert.ToInt32(reader["contactno_id"]);
                            contactno = contact.Contactnoslist.FirstOrDefault(o => o.id == contactno_id);
                            if (contactno == null)
                            {
                                contactno = new Contactnos();
                                contactno.id = Convert.ToInt32(reader["contactno_id"]);
                                contactno.number = reader["contactno"].ToString();
                                contactno.number_type = Convert.ToString(reader["number_type"]);
                                contactno.type_name = this.GetTypeName((reader["number_type"]).ToString());
                                contact.Contactnoslist.Add(contactno);
                            }
                        }

                        Emails emails = null;
                        if (reader["email_id"] != DBNull.Value)
                        {
                            int email_id = Convert.ToInt32(reader["email_id"]);
                            emails = contact.Emailslist.FirstOrDefault(q => q.id == email_id);

                            if (emails == null)
                            {
                                emails = new Emails();
                                emails.id = Convert.ToInt32(reader["email_id"]);
                                emails.email = Convert.ToString(reader["email"]);
                                emails.email_type = Convert.ToString(reader["email_type"]);
                                emails.type_name = this.GetTypeName((reader["email_type"]).ToString());
                                contact.Emailslist.Add(emails);
                            }
                        }

                        Websites websites = null;
                        if (reader["website_id"] != DBNull.Value)
                        {
                            int website_id = Convert.ToInt32(reader["website_id"]);
                            websites = contact.websiteslist.FirstOrDefault(q => q.id == website_id);

                            if (websites == null)
                            {
                                websites = new Websites();
                                websites.id = Convert.ToInt32(reader["website_id"]);
                                websites.URLs = Convert.ToString(reader["website"]);
                                contact.websiteslist.Add(websites);
                            }
                        }

                        Adress adress = null;
                        if (reader["address_id"] != DBNull.Value)
                        {
                            int address_id = Convert.ToInt32(reader["address_id"]);
                            adress = contact.Adresslist.FirstOrDefault(c => c.id == address_id);
                            if (adress == null)
                            {
                                adress = new Adress();
                                adress.id = Convert.ToInt32(reader["address_id"]);
                                adress.city = Convert.ToString(reader["city"]);
                                adress.street1 = Convert.ToString(reader["street1"]);
                                adress.state = Convert.ToString(reader["state"]);
                                adress.country = (reader["country"].ToString());// Convert.ToInt32(reader["domain_id"]);
                                adress.street2 = reader["street2"].ToString();
                                adress.zipcode = reader["zipcode"].ToString();
                                adress.address_type = reader["address_type"].ToString();
                                adress.type_name = this.GetTypeName((reader["address_type"]).ToString());
                                contact.Adresslist.Add(adress);
                            }
                        }
                        contact.Contactnoslist = contact.Contactnoslist.GroupBy(x => x.number, (key, group) => group.First()).ToList();
                        contact.Emailslist = contact.Emailslist.GroupBy(x => x.email, (key, group) => group.First()).ToList();
                        contact.websiteslist = contact.websiteslist.GroupBy(x => x.URLs, (key, group) => group.First()).ToList();
                        model.Add(contact);
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            #region Validation
            var chanellist = from chanel in model
                             from adds in chanel.Contactnoslist
                             where adds.number != null
                             select chanel;
            #endregion
            chanellist = chanellist.GroupBy(x => x.GUID, (key, group) => group.First()).ToList();
            return chanellist.Distinct().ToList();
        }

      
        #endregion

        #region Get Contact Information Based On GUID(File Name of the GUID)
        public List<Contact> GetContactInformationOnID(string GUID)
        {
            List<Contact> model = new List<Contact>();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@ID", SqlDbType.VarChar);
                param[0].Value = GUID;
                using (IDataReader reader = db.executeSelectQuery("GetContactInformationBasedOnID", param, CommandType.StoredProcedure))
                {
                    while (reader.Read())
                    {
                        Contact contact = null;
                        int vcf_id = Convert.ToInt32(reader["vcf_id"]);
                        contact = model.FirstOrDefault(q => q.vcf_id == vcf_id);
                        if (contact == null)
                        {
                            contact = new Contact();
                            contact.firstname = Convert.ToString(reader["first_name"]);
                            contact.middlename = Convert.ToString(reader["middle_name"]);
                            contact.lastname = Convert.ToString(reader["last_name"]);
                            contact.prefix = Convert.ToString(reader["prefix"]);
                            contact.sufix = Convert.ToString(reader["sufix"]);
                            contact.company = Convert.ToString(reader["organisation"]);
                            contact.jobtitle = Convert.ToString(reader["title"]);
                            contact.SGID = Convert.ToString(reader["SGID"]);
                            contact.vcf_id = Convert.ToInt32(reader["vcf_id"]);
                            contact.zone = Convert.ToString(reader["zone"]);
                            contact.User_id = Convert.ToString(reader["user_id"]);
                            contact.GUID = GUID;
                        }
                        Contactnos contactno = null;
                        if (reader["contactno_id"] != DBNull.Value)
                        {
                            int contactno_id = Convert.ToInt32(reader["contactno_id"]);
                            contactno = contact.Contactnoslist.FirstOrDefault(o => o.id == contactno_id);
                            if (contactno == null)
                            {
                                contactno = new Contactnos();
                                contactno.id = Convert.ToInt32(reader["contactno_id"]);
                                contactno.number = reader["contactno"].ToString();
                                contactno.number_type = Convert.ToString(reader["number_type"]);
                                contactno.type_name = this.GetTypeName((reader["number_type"]).ToString());
                                contact.Contactnoslist.Add(contactno);
                            }
                        }

                        Emails emails = null;
                        if (reader["email_id"] != DBNull.Value)
                        {
                            int email_id = Convert.ToInt32(reader["email_id"]);
                            emails = contact.Emailslist.FirstOrDefault(q => q.id == email_id);

                            if (emails == null)
                            {
                                emails = new Emails();
                                emails.id = Convert.ToInt32(reader["email_id"]);
                                emails.email = Convert.ToString(reader["email"]);
                                emails.email_type = Convert.ToString(reader["email_type"]);
                                emails.type_name = this.GetTypeName((reader["email_type"]).ToString());
                                contact.Emailslist.Add(emails);
                            }
                        }

                        Websites websites = null;
                        if (reader["website_id"] != DBNull.Value)
                        {
                            int website_id = Convert.ToInt32(reader["website_id"]);
                            websites = contact.websiteslist.FirstOrDefault(q => q.id == website_id);

                            if (websites == null)
                            {
                                websites = new Websites();
                                websites.id = Convert.ToInt32(reader["website_id"]);
                                websites.URLs = Convert.ToString(reader["website"]);
                                contact.websiteslist.Add(websites);
                            }
                        }

                        Adress adress = null;
                        if (reader["address_id"] != DBNull.Value)
                        {
                            int address_id = Convert.ToInt32(reader["address_id"]);
                            adress = contact.Adresslist.FirstOrDefault(c => c.id == address_id);
                            if (adress == null)
                            {
                                adress = new Adress();
                                adress.id = Convert.ToInt32(reader["address_id"]);
                                adress.city = Convert.ToString(reader["city"]);
                                adress.street1 = Convert.ToString(reader["street1"]);
                                adress.state = Convert.ToString(reader["state"]);
                                adress.country = (reader["country"].ToString());// Convert.ToInt32(reader["domain_id"]);
                                adress.street2 = reader["street2"].ToString();
                                adress.zipcode = reader["zipcode"].ToString();
                                adress.address_type = reader["address_type"].ToString();
                                adress.type_name = this.GetTypeName((reader["address_type"]).ToString());
                                contact.Adresslist.Add(adress);
                            }
                        }
                        contact.Contactnoslist = contact.Contactnoslist.GroupBy(x => x.number, (key, group) => group.First()).ToList();
                        contact.Emailslist = contact.Emailslist.GroupBy(x => x.email, (key, group) => group.First()).ToList();
                        contact.websiteslist = contact.websiteslist.GroupBy(x => x.URLs, (key, group) => group.First()).ToList();
                        model.Add(contact);
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            #region Validation
            var chanellist = from chanel in model
                             from adds in chanel.Contactnoslist
                             where adds.number != null
                             select chanel;
            #endregion
            chanellist = chanellist.GroupBy(x => x.GUID, (key, group) => group.First()).ToList();
            return chanellist.Distinct().ToList();
        }

        
        #endregion

        #region Get Contact Information
        public List<Contact> GetContactInformation()
        {
            List<Contact> model = new List<Contact>();
            List<SGIDList> models = new List<SGIDList>();
            try
            {
                if (APPSession.Current.Employee_Name == "SUPERADMIN")
                {
                    using (IDataReader reader = db.executeSelectQuery("GetContactInformation", null, CommandType.StoredProcedure))
                    {
                        while (reader.Read())
                        {
                            Contact contact = null;
                            int vcf_id = Convert.ToInt32(reader["vcf_id"]);
                            contact = model.FirstOrDefault(q => q.vcf_id == vcf_id);
                            if (contact == null)
                            {
                                contact = new Contact();
                                contact.firstname = Convert.ToString(reader["first_name"]);
                                contact.middlename = Convert.ToString(reader["middle_name"]);
                                contact.lastname = Convert.ToString(reader["last_name"]);
                                contact.prefix = Convert.ToString(reader["prefix"]);
                                contact.sufix = Convert.ToString(reader["sufix"]);
                                contact.company = Convert.ToString(reader["organisation"]);
                                contact.jobtitle = Convert.ToString(reader["title"]);
                                contact.SGID = Convert.ToString(reader["SGID"]);
                                contact.vcf_id = Convert.ToInt32(reader["vcf_id"]);
                                contact.addedby = Convert.ToInt32(reader["addedby"]);
                                contact.zone = Convert.ToString(reader["zone"]);
                                contact.addedon = Convert.ToDateTime(reader["addedon"]);
                                contact.GUID = Convert.ToString(reader["GUID"]);
                                contact.IsDeleteRequest = string.IsNullOrEmpty((reader["IsDeleteRequest"]).ToString() ) ? false : Convert.ToBoolean(reader["IsDeleteRequest"]); 
                            }
                            Contactnos contactno = null;
                            if (reader["contactno_id"] != DBNull.Value)
                            {
                                int contactno_id = Convert.ToInt32(reader["contactno_id"]);
                                contactno = contact.Contactnoslist.FirstOrDefault(o => o.id == contactno_id);
                                if (contactno == null)
                                {
                                    contactno = new Contactnos();
                                    contactno.id = Convert.ToInt32(reader["contactno_id"]);
                                    contactno.number = reader["contactno"].ToString();
                                    contactno.number_type = Convert.ToString(reader["number_type"]);
                                    contactno.type_name = this.GetTypeName((reader["number_type"]).ToString());
                                    contact.Contactnoslist.Add(contactno);
                                }
                            }

                            Emails emails = null;
                            if (reader["email_id"] != DBNull.Value)
                            {
                                int email_id = Convert.ToInt32(reader["email_id"]);
                                emails = contact.Emailslist.FirstOrDefault(q => q.id == email_id);

                                if (emails == null)
                                {
                                    emails = new Emails();
                                    emails.id = Convert.ToInt32(reader["email_id"]);
                                    emails.email = Convert.ToString(reader["email"]);
                                    emails.email_type = Convert.ToString(reader["email_type"]);
                                    string typename = this.GetTypeName((reader["email_type"]).ToString());
                                    emails.type_name = typename;
                                    contact.Emailslist.Add(emails);
                                }
                            }

                            Websites websites = null;
                            if (reader["website_id"] != DBNull.Value)
                            {
                                int website_id = Convert.ToInt32(reader["website_id"]);
                                websites = contact.websiteslist.FirstOrDefault(q => q.id == website_id);

                                if (websites == null)
                                {
                                    websites = new Websites();
                                    websites.id = Convert.ToInt32(reader["website_id"]);
                                    websites.URLs = Convert.ToString(reader["website"]);
                                    contact.websiteslist.Add(websites);
                                }
                            }

                            Adress adress = null;
                            if (reader["address_id"] != DBNull.Value)
                            {
                                int address_id = Convert.ToInt32(reader["address_id"]);
                                adress = contact.Adresslist.FirstOrDefault(c => c.id == address_id);
                                if (adress == null)
                                {
                                    adress = new Adress();
                                    adress.id = Convert.ToInt32(reader["address_id"]);
                                    adress.city = Convert.ToString(reader["city"]);
                                    adress.street1 = Convert.ToString(reader["street1"]);
                                    adress.street2 = Convert.ToString(reader["street2"]);
                                    adress.state = Convert.ToString(reader["state"]);
                                    adress.country = (reader["country"].ToString());// Convert.ToInt32(reader["domain_id"]);
                                    adress.zipcode = reader["zipcode"].ToString();
                                    adress.address_type = reader["address_type"].ToString();
                                    string type_name = this.GetTypeName((reader["address_type"]).ToString());
                                    adress.type_name = type_name;
                                    contact.Adresslist.Add(adress);
                                }
                            }

                            model.Add(contact);
                        }

                    }
                }
                else
                {
                    SqlParameter[] sqlParameter = new SqlParameter[1];
                    sqlParameter[0] = new SqlParameter("@SGID", SqlDbType.VarChar);
                    sqlParameter[0].Value = APPSession.Current.SGID   ;
                    using (IDataReader readers = db.executeSelectQuery("GetSubordinates", sqlParameter, CommandType.StoredProcedure))
                    {
                     
                        while (readers.Read())
                        {
                            models.Add(new SGIDList { sgid = readers["id"].ToString() });
                        }
                    }

                    SqlParameter[] sqlParameterNEW = new SqlParameter[2];
                    sqlParameterNEW[0] = new SqlParameter("@SGID", SqlDbType.VarChar);
                    sqlParameterNEW[0].Value = APPSession.Current.SGID;
                    sqlParameterNEW[1] = new SqlParameter("@zone", SqlDbType.VarChar);
                    sqlParameterNEW[1].Value = APPSession.Current.zone;
                    using (IDataReader readerNew = db.executeSelectQuery("GetWithoutSubordinate", sqlParameterNEW, CommandType.StoredProcedure))
                    {
                        while (readerNew.Read())
                        {
                            models.Add(new SGIDList { sgid = readerNew["id"].ToString() });
                        }
                    }

                  

                    models.Add(new SGIDList { sgid = APPSession.Current.User_id.ToString()  });

                    model.Distinct().ToList();
                System.Data.DataTable _myDataTable = new System.Data.DataTable();// Convert array to Data Table
                _myDataTable.Columns.Add("SGID");

                for (int i = 0; i < models.Count; i++)
                {
                    string user_id =(models[i].sgid);
                    _myDataTable.Rows.Add(user_id);
                }
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@List", SqlDbType.Structured);
                param[0].Value = _myDataTable;
                using (IDataReader reader = db.executeSelectQuery("GetContactInformationBasedOnSupervisor", param, CommandType.StoredProcedure))
                {
                    while (reader.Read())
                    {
                        Contact contact = null;
                        int vcf_id = Convert.ToInt32(reader["vcf_id"]);
                        contact = model.FirstOrDefault(q => q.vcf_id == vcf_id);
                        if (contact == null)
                        {
                            contact = new Contact();
                            contact.firstname = Convert.ToString(reader["first_name"]);
                            contact.middlename = Convert.ToString(reader["middle_name"]);
                            contact.lastname = Convert.ToString(reader["last_name"]);
                            contact.prefix = Convert.ToString(reader["prefix"]);
                            contact.sufix = Convert.ToString(reader["sufix"]);
                            contact.company = Convert.ToString(reader["organisation"]);
                            contact.jobtitle = Convert.ToString(reader["title"]);
                            contact.SGID = Convert.ToString(reader["SGID"]);
                            contact.vcf_id = Convert.ToInt32(reader["vcf_id"]);
                            contact.addedby = Convert.ToInt32(reader["addedby"]);
                            contact.addedon = Convert.ToDateTime(reader["addedon"]);
                            contact.GUID = Convert.ToString(reader["GUID"]);
                            contact.IsDeleteRequest = string.IsNullOrEmpty((reader["IsDeleteRequest"]).ToString()) ? false : Convert.ToBoolean(reader["IsDeleteRequest"]);
                        }
                        Contactnos contactno = null;
                        if (reader["contactno_id"] != DBNull.Value)
                        {
                            int contactno_id = Convert.ToInt32(reader["contactno_id"]);
                            contactno = contact.Contactnoslist.FirstOrDefault(o => o.id == contactno_id);
                            if (contactno == null)
                            {
                                contactno = new Contactnos();
                                contactno.id = Convert.ToInt32(reader["contactno_id"]);
                                contactno.number = reader["contactno"].ToString();
                                contactno.number_type = Convert.ToString(reader["number_type"]);
                                contactno.type_name = this.GetTypeName((reader["number_type"]).ToString());
                                contact.Contactnoslist.Add(contactno);
                            }
                        }

                        Emails emails = null;
                        if (reader["email_id"] != DBNull.Value)
                        {
                            int email_id = Convert.ToInt32(reader["email_id"]);
                            emails = contact.Emailslist.FirstOrDefault(q => q.id == email_id);

                            if (emails == null)
                            {
                                emails = new Emails();
                                emails.id = Convert.ToInt32(reader["email_id"]);
                                emails.email = Convert.ToString(reader["email"]);
                                emails.email_type = Convert.ToString(reader["email_type"]);
                                string typename = this.GetTypeName((reader["email_type"]).ToString());
                                emails.type_name = typename;
                                contact.Emailslist.Add(emails);
                            }
                        }

                        Websites websites = null;
                        if (reader["website_id"] != DBNull.Value)
                        {
                            int website_id = Convert.ToInt32(reader["website_id"]);
                            websites = contact.websiteslist.FirstOrDefault(q => q.id == website_id);

                            if (websites == null)
                            {
                                websites = new Websites();
                                websites.id = Convert.ToInt32(reader["website_id"]);
                                websites.URLs = Convert.ToString(reader["website"]);
                                contact.websiteslist.Add(websites);
                            }
                        }

                        Adress adress = null;
                        if (reader["address_id"] != DBNull.Value)
                        {
                            int address_id = Convert.ToInt32(reader["address_id"]);
                            adress = contact.Adresslist.FirstOrDefault(c => c.id == address_id);
                            if (adress == null)
                            {
                                adress = new Adress();
                                adress.id = Convert.ToInt32(reader["address_id"]);
                                adress.city = Convert.ToString(reader["city"]);
                                adress.street1 = Convert.ToString(reader["street1"]);
                                adress.street2 = Convert.ToString(reader["street2"]);
                                adress.state = Convert.ToString(reader["state"]);
                                adress.country = (reader["country"].ToString());// Convert.ToInt32(reader["domain_id"]);
                                adress.zipcode = reader["zipcode"].ToString();
                                adress.address_type = reader["address_type"].ToString();
                                string type_name = this.GetTypeName((reader["address_type"]).ToString());
                                adress.type_name = type_name;
                                contact.Adresslist.Add(adress);
                            }
                        }

                        model.Add(contact);
                    }

                }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }

            #region Validation
            var chanellist = from chanel in model
                             from adds in chanel.Contactnoslist
                             where adds.number != null
                             select chanel;
            #endregion
            chanellist = chanellist.GroupBy(x => x.GUID, (key, group) => group.First()).ToList();
            return chanellist.Distinct().ToList();
        }

        public List<Contact> GetContactInformationOnUsers(int user_id)
        {
            List<Contact> model = new List<Contact>();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@userid", SqlDbType.Int);
                param[0].Value = user_id;
                using (IDataReader reader = db.executeSelectQuery("GetContactInformationOnUser", param, CommandType.StoredProcedure))
                {
                    while (reader.Read())
                    {
                        Contact contact = null;
                        int vcf_id = Convert.ToInt32(reader["vcf_id"]);
                        contact = model.FirstOrDefault(q => q.vcf_id == vcf_id);
                        if (contact == null)
                        {
                            contact = new Contact();
                            contact.firstname = Convert.ToString(reader["first_name"]);
                            contact.middlename = Convert.ToString(reader["middle_name"]);
                            contact.lastname = Convert.ToString(reader["last_name"]);
                            contact.prefix = Convert.ToString(reader["prefix"]);
                            contact.sufix = Convert.ToString(reader["sufix"]);
                            contact.company = Convert.ToString(reader["organisation"]);
                            contact.jobtitle = Convert.ToString(reader["title"]);
                            contact.SGID = Convert.ToString(reader["SGID"]);
                            contact.vcf_id = Convert.ToInt32(reader["vcf_id"]);
                            contact.addedby = Convert.ToInt32(reader["addedby"]);
                            contact.addedon = Convert.ToDateTime(reader["addedon"]);
                            contact.GUID = Convert.ToString(reader["GUID"]);
                            contact.IsDeleteRequest = string.IsNullOrEmpty((reader["IsDeleteRequest"]).ToString()) ? false : Convert.ToBoolean(reader["IsDeleteRequest"]);
                        }
                        Contactnos contactno = null;
                        if (reader["contactno_id"] != DBNull.Value)
                        {
                            int contactno_id = Convert.ToInt32(reader["contactno_id"]);
                            contactno = contact.Contactnoslist.FirstOrDefault(o => o.id == contactno_id);
                            if (contactno == null)
                            {
                                contactno = new Contactnos();
                                contactno.id = Convert.ToInt32(reader["contactno_id"]);
                                contactno.number = reader["contactno"].ToString();
                                contactno.number_type = Convert.ToString(reader["number_type"]);
                                contactno.type_name = this.GetTypeName((reader["number_type"]).ToString());
                                contact.Contactnoslist.Add(contactno);
                            }
                        }

                        Emails emails = null;
                        if (reader["email_id"] != DBNull.Value)
                        {
                            int email_id = Convert.ToInt32(reader["email_id"]);
                            emails = contact.Emailslist.FirstOrDefault(q => q.id == email_id);

                            if (emails == null)
                            {
                                emails = new Emails();
                                emails.id = Convert.ToInt32(reader["email_id"]);
                                emails.email = Convert.ToString(reader["email"]);
                                emails.email_type = Convert.ToString(reader["email_type"]);
                                string typename = this.GetTypeName((reader["email_type"]).ToString());
                                emails.type_name = typename;
                                contact.Emailslist.Add(emails);
                            }
                        }

                        Websites websites = null;
                        if (reader["website_id"] != DBNull.Value)
                        {
                            int website_id = Convert.ToInt32(reader["website_id"]);
                            websites = contact.websiteslist.FirstOrDefault(q => q.id == website_id);

                            if (websites == null)
                            {
                                websites = new Websites();
                                websites.id = Convert.ToInt32(reader["website_id"]);
                                websites.URLs = Convert.ToString(reader["website"]);
                                contact.websiteslist.Add(websites);
                            }
                        }

                        Adress adress = null;
                        if (reader["address_id"] != DBNull.Value)
                        {
                            int address_id = Convert.ToInt32(reader["address_id"]);
                            adress = contact.Adresslist.FirstOrDefault(c => c.id == address_id);
                            if (adress == null)
                            {
                                adress = new Adress();
                                adress.id = Convert.ToInt32(reader["address_id"]);
                                adress.city = Convert.ToString(reader["city"]);
                                adress.street1 = Convert.ToString(reader["street1"]);
                                adress.street2 = Convert.ToString(reader["street2"]);
                                adress.state = Convert.ToString(reader["state"]);
                                adress.country = (reader["country"].ToString());// Convert.ToInt32(reader["domain_id"]);
                                adress.zipcode = reader["zipcode"].ToString();
                                adress.address_type = reader["address_type"].ToString();
                                string type_name = this.GetTypeName((reader["address_type"]).ToString());
                                adress.type_name = type_name;
                                contact.Adresslist.Add(adress);
                            }
                        }
                        contact.Contactnoslist = contact.Contactnoslist.GroupBy(x => x.number, (key, group) => group.First()).ToList();
                        contact.Emailslist = contact.Emailslist.GroupBy(x => x.email, (key, group) => group.First()).ToList();
                        contact.websiteslist = contact.websiteslist.GroupBy(x => x.URLs, (key, group) => group.First()).ToList();
                        model.Add(contact);
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            #region Validation
            var chanellist = from chanel in model
                             from adds in chanel.Contactnoslist
                             where adds.number != null
                             select chanel;
            #endregion
            chanellist = chanellist.GroupBy(x => x.GUID, (key, group) => group.First()).ToList();
            return chanellist.Distinct().ToList();
        }
        #endregion

        #region get Type Name

        public string GetTypeName(string id)
        {
            string name = null;
            try
            {
                string query = " SELECT type from type where id='"+id+"' ";
                SqlParameter[] param = new SqlParameter[1];

                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    name = (dr[0].ToString());
                }
                return name;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Syncdetails Save
        public bool SaveSyncDetails(string user_id, string zone)
        {
            bool result = true;
            try
            {
                if (user_id != null)
                {
                    if (!commonDAO.ValidateCommon("syncdetails", "user_id", user_id))
                    {
                        SqlParameter[] param = new SqlParameter[2];
                        string query = "spSyncSave";
                        param[0] = new SqlParameter("@user_id", SqlDbType.VarChar);
                        param[1] = new SqlParameter("@zone", SqlDbType.VarChar);
                        param[0].Value = string.IsNullOrEmpty(user_id) ? (object)DBNull.Value : user_id;
                        param[1].Value = string.IsNullOrEmpty(zone) ? (object)DBNull.Value : zone;
                        result = db.executeInsertQuery(query, param);
                    }
                    else
                    {
                        SqlParameter[] param = new SqlParameter[2];
                        string query = "spSyncUpdate";
                        param[0] = new SqlParameter("@user_id", SqlDbType.VarChar);
                        param[1] = new SqlParameter("@zone", SqlDbType.VarChar);
                        param[0].Value = string.IsNullOrEmpty(user_id) ? (object)DBNull.Value : user_id;
                        param[1].Value = string.IsNullOrEmpty(zone) ? (object)DBNull.Value : zone;
                        result = db.executeUpdateQuery(query, param);
                    }
                }
                return result;
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.closeConnection();
            }
            return result;
        }


        #endregion

        #region Get Pending Sync Record
        public List<ContactList> GetPendingSyncRecords(string syncdate, string user_id,string zone)
        {
            List<ContactList> model = new List<ContactList>();
            List<SGIDList> models = new List<SGIDList>();
            try
            {

                string SGID = commonDAO.GetSGID(user_id);
                SqlParameter[] sqlParameter = new SqlParameter[1];
                sqlParameter[0] = new SqlParameter("@SGID", SqlDbType.VarChar);
                sqlParameter[0].Value = SGID;
                using (IDataReader readers = db.executeSelectQuery("GetSubordinates", sqlParameter, CommandType.StoredProcedure))
                {
                    while (readers.Read())
                    {
                        models.Add(new SGIDList { sgid = readers["id"].ToString() });
                    }
                }
                SqlParameter[] sqlParameterNEW = new SqlParameter[2];
                sqlParameterNEW[0] = new SqlParameter("@SGID", SqlDbType.VarChar);
                sqlParameterNEW[0].Value = SGID;
                sqlParameterNEW[1] = new SqlParameter("@zone", SqlDbType.VarChar);
                sqlParameterNEW[1].Value = zone;
                using (IDataReader readerNew = db.executeSelectQuery("GetWithoutSubordinate", sqlParameterNEW, CommandType.StoredProcedure))
                {
                    while (readerNew.Read())
                    {
                        models.Add(new SGIDList { sgid = readerNew["id"].ToString() });
                    }
                }
                models.Add(new SGIDList { sgid = "0" });
                model.Distinct().ToList();  
                models.Add(new SGIDList { sgid = user_id });


                System.Data.DataTable _myDataTable = new System.Data.DataTable();// Convert array to Data Table
                _myDataTable.Columns.Add("SGID");

                for (int i = 0; i < models.Count; i++)
                {
                    string userid = (models[i].sgid);
                    _myDataTable.Rows.Add(userid);
                }

                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@syncdate", SqlDbType.DateTime);
                if (!string.IsNullOrEmpty(syncdate))
                {
                    DateTime syncdates = Convert.ToDateTime(syncdate);
                    DateTime date = DateTime.ParseExact(syncdates.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    syncdate = date.ToString("yyyy-MM-dd HH:mm:ss");
                }
               
                param[0].Value = string.IsNullOrEmpty(syncdate) ? (object)DBNull.Value : syncdate; ;
                param[1] = new SqlParameter("@user_id", SqlDbType.Int);
                param[1].Value = string.IsNullOrEmpty(user_id) ? (object)DBNull.Value : Convert.ToInt32(user_id);
                param[2] = new SqlParameter("@zone", SqlDbType.VarChar);
                param[2].Value = string.IsNullOrEmpty(zone) ? (object)DBNull.Value : (zone);
                param[3] = new SqlParameter("@List", SqlDbType.Structured);
                param[3].Value = _myDataTable;
                using (IDataReader reader = db.executeSelectQuery("GetPendingSync Records", param, CommandType.StoredProcedure))
                {
                    while (reader.Read())
                    {
                        ContactList contact = null;
                        int vcf_id = Convert.ToInt32(reader["vcf_id"]);
                        contact = model.FirstOrDefault(q => q.vcf_id == vcf_id);
                        if (contact == null)
                        {
                            contact = new ContactList();
                            contact.firstname = Convert.ToString(reader["first_name"]);
                            contact.middlename = Convert.ToString(reader["middle_name"]);
                            contact.lastname = Convert.ToString(reader["last_name"]);
                            contact.prefix = Convert.ToString(reader["prefix"]);
                            contact.sufix = Convert.ToString(reader["sufix"]);
                            contact.company = Convert.ToString(reader["organisation"]);
                            contact.jobtitle = Convert.ToString(reader["title"]);
                            contact.SGID = Convert.ToString(reader["SGID"]);
                            contact.addedby = Convert.ToInt32(reader["addedby"]);
                            contact.addedon = Convert.ToDateTime(reader["addedon"]);
                            contact.GUID = Convert.ToString(reader["GUID"]);
                            contact.vcf_id = Convert.ToInt32(reader["vcf_id"]);
                            contact.IsUpdated = Convert.ToString(reader["IsUpdated"]);
                        }
                        Contactnos contactno = null;
                        if (reader["contactno_id"] != DBNull.Value)
                        {
                            int contactno_id = Convert.ToInt32(reader["contactno_id"]);
                            contactno = contact.Contactnoslist.FirstOrDefault(o => o.id == contactno_id);
                            if (contactno == null)
                            {
                                contactno = new Contactnos();
                                contactno.id = Convert.ToInt32(reader["contactno_id"]);
                                contactno.number = reader["contactno"].ToString();
                                contactno.number_type = Convert.ToString(reader["number_type"]);
                                contactno.type_name = this.GetTypeName((reader["number_type"]).ToString());
                                contact.Contactnoslist.Add(contactno);
                            }
                        }

                        Emails emails = null;
                        if (reader["email_id"] != DBNull.Value)
                        {
                            int email_id = Convert.ToInt32(reader["email_id"]);
                            emails = contact.Emailslist.FirstOrDefault(q => q.id == email_id);

                            if (emails == null)
                            {
                                emails = new Emails();
                                emails.id = Convert.ToInt32(reader["email_id"]);
                                emails.email = Convert.ToString(reader["email"]);
                                emails.email_type = Convert.ToString(reader["email_type"]);
                                emails.type_name = this.GetTypeName((reader["email_type"]).ToString());
                                contact.Emailslist.Add(emails);
                            }
                        }

                        Websites websites = null;
                        if (reader["website_id"] != DBNull.Value)
                        {
                            int website_id = Convert.ToInt32(reader["website_id"]);
                            websites = contact.websiteslist.FirstOrDefault(q => q.id == website_id);

                            if (websites == null)
                            {
                                websites = new Websites();
                                websites.id = Convert.ToInt32(reader["website_id"]);
                                websites.URLs = Convert.ToString(reader["website"]);
                                contact.websiteslist.Add(websites);
                            }
                        }

                        Adress adress = null;
                        if (reader["address_id"] != DBNull.Value)
                        {
                            int address_id = Convert.ToInt32(reader["address_id"]);
                            adress = contact.Adresslist.FirstOrDefault(c => c.id == address_id);
                            if (adress == null)
                            {
                                adress = new Adress();
                                adress.id = Convert.ToInt32(reader["address_id"]);
                                adress.city = Convert.ToString(reader["city"]);
                                adress.street1 = Convert.ToString(reader["street1"]);
                                adress.state = Convert.ToString(reader["state"]);
                                adress.country = (reader["country"].ToString());// Convert.ToInt32(reader["domain_id"]);
                                adress.street2 = reader["street2"].ToString();
                                adress.zipcode = reader["zipcode"].ToString();
                                adress.address_type = reader["address_type"].ToString();
                                adress.type_name = this.GetTypeName((reader["address_type"]).ToString());
                                contact.Adresslist.Add(adress);
                            }
                        }
                        contact.Contactnoslist = contact.Contactnoslist.GroupBy(x => x.number, (key, group) => group.First()).ToList();
                        contact.Emailslist = contact.Emailslist.GroupBy(x => x.email, (key, group) => group.First()).ToList();
                        contact.websiteslist = contact.websiteslist.GroupBy(x => x.URLs, (key, group) => group.First()).ToList();

                        model.Add(contact);
                    }

                }
            }
            catch (Exception ex)
            {
                commonDAO.WriteToFile("GetPendingSyncRecords error"+ ex.Message);
            }
            finally
            {
                db.closeConnection();
            }
            #region Validation
            var chanellist = from chanel in model
                             from adds in chanel.Contactnoslist
                             where adds.number != null
                             select chanel;
            #endregion
            chanellist = chanellist.GroupBy(x => x.GUID, (key, group) => group.First()).ToList();
            return chanellist.Distinct().ToList();
        }
        #endregion

        #region Get Pending Sync Record
        public List<ContactList> GetAllRecords(string user_id, string zone)
        {
            List<ContactList> model = new List<ContactList>();
            List<SGIDList> models = new List<SGIDList>();
            try
            {
                string SGID = commonDAO.GetSGID(user_id);
                SqlParameter[] sqlParameter = new SqlParameter[1];
                sqlParameter[0] = new SqlParameter("@SGID", SqlDbType.VarChar);
                sqlParameter[0].Value = SGID;
                using (IDataReader readers = db.executeSelectQuery("GetSubordinates", sqlParameter, CommandType.StoredProcedure))
                {

                    while (readers.Read())
                    {
                        models.Add(new SGIDList { sgid = readers["id"].ToString() });
                    }
                }


                SqlParameter[] sqlParameterNEW = new SqlParameter[2];
                sqlParameterNEW[0] = new SqlParameter("@SGID", SqlDbType.VarChar);
                sqlParameterNEW[0].Value = SGID;
                sqlParameterNEW[1] = new SqlParameter("@zone", SqlDbType.VarChar);
                sqlParameterNEW[1].Value = zone;
                using (IDataReader readerNew = db.executeSelectQuery("GetWithoutSubordinate", sqlParameterNEW, CommandType.StoredProcedure))
                {
                    while (readerNew.Read())
                    {
                        models.Add(new SGIDList { sgid = readerNew["id"].ToString() });
                    }
                }

                model.Distinct().ToList();  
                models.Add(new SGIDList { sgid = user_id });


                System.Data.DataTable _myDataTable = new System.Data.DataTable();// Convert array to Data Table
                _myDataTable.Columns.Add("SGID");

                for (int i = 0; i < models.Count; i++)
                {
                    string userid = (models[i].sgid);
                    _myDataTable.Rows.Add(userid);
                }

                SqlParameter[] param = new SqlParameter[3];
                param[0] = new SqlParameter("@user_id", SqlDbType.Int);
                param[0].Value = string.IsNullOrEmpty(user_id) ? (object)DBNull.Value : Convert.ToInt32(user_id);
                param[1] = new SqlParameter("@zone", SqlDbType.VarChar);
                param[1].Value = string.IsNullOrEmpty(zone) ? (object)DBNull.Value : (zone);
                param[2] = new SqlParameter("@List", SqlDbType.Structured);
                param[2].Value = _myDataTable;
                using (IDataReader reader = db.executeSelectQuery("GetAllRecords", param, CommandType.StoredProcedure))
                {
                    while (reader.Read())
                    {
                        ContactList contact = null;
                        int vcf_id = Convert.ToInt32(reader["vcf_id"]);
                        contact = model.FirstOrDefault(q => q.vcf_id == vcf_id);
                        if (contact == null)
                        {
                            contact = new ContactList();
                            contact.firstname = Convert.ToString(reader["first_name"]);
                            contact.middlename = Convert.ToString(reader["middle_name"]);
                            contact.lastname = Convert.ToString(reader["last_name"]);
                            contact.prefix = Convert.ToString(reader["prefix"]);
                            contact.sufix = Convert.ToString(reader["sufix"]);
                            contact.company = Convert.ToString(reader["organisation"]);
                            contact.jobtitle = Convert.ToString(reader["title"]);
                            contact.SGID = Convert.ToString(reader["SGID"]);
                            contact.addedby = Convert.ToInt32(reader["addedby"]);
                            contact.addedon = Convert.ToDateTime(reader["addedon"]);
                            contact.GUID = Convert.ToString(reader["GUID"]);
                            contact.vcf_id = Convert.ToInt32(reader["vcf_id"]);
                            contact.IsUpdated = Convert.ToString(reader["IsUpdated"]);
                        }
                        Contactnos contactno = null;
                        if (reader["contactno_id"] != DBNull.Value)
                        {
                            int contactno_id = Convert.ToInt32(reader["contactno_id"]);
                            contactno = contact.Contactnoslist.FirstOrDefault(o => o.id == contactno_id);
                            if (contactno == null)
                            {
                                contactno = new Contactnos();
                                contactno.id = Convert.ToInt32(reader["contactno_id"]);
                                contactno.number = reader["contactno"].ToString();
                                contactno.number_type = Convert.ToString(reader["number_type"]);
                                contactno.type_name = this.GetTypeName((reader["number_type"]).ToString());
                                contact.Contactnoslist.Add(contactno);
                            }
                        }

                        Emails emails = null;
                        if (reader["email_id"] != DBNull.Value)
                        {
                            int email_id = Convert.ToInt32(reader["email_id"]);
                            emails = contact.Emailslist.FirstOrDefault(q => q.id == email_id);

                            if (emails == null)
                            {
                                emails = new Emails();
                                emails.id = Convert.ToInt32(reader["email_id"]);
                                emails.email = Convert.ToString(reader["email"]);
                                emails.email_type = Convert.ToString(reader["email_type"]);
                                emails.type_name = this.GetTypeName((reader["email_type"]).ToString());
                                contact.Emailslist.Add(emails);
                            }
                        }

                        Websites websites = null;
                        if (reader["website_id"] != DBNull.Value)
                        {
                            int website_id = Convert.ToInt32(reader["website_id"]);
                            websites = contact.websiteslist.FirstOrDefault(q => q.id == website_id);

                            if (websites == null)
                            {
                                websites = new Websites();
                                websites.id = Convert.ToInt32(reader["website_id"]);
                                websites.URLs = Convert.ToString(reader["website"]);
                                contact.websiteslist.Add(websites);
                            }
                        }

                        Adress adress = null;
                        if (reader["address_id"] != DBNull.Value)
                        {
                            int address_id = Convert.ToInt32(reader["address_id"]);
                            adress = contact.Adresslist.FirstOrDefault(c => c.id == address_id);
                            if (adress == null)
                            {
                                adress = new Adress();
                                adress.id = Convert.ToInt32(reader["address_id"]);
                                adress.city = Convert.ToString(reader["city"]);
                                adress.street1 = Convert.ToString(reader["street1"]);
                                adress.state = Convert.ToString(reader["state"]);
                                adress.country = (reader["country"].ToString());// Convert.ToInt32(reader["domain_id"]);
                                adress.street2 = reader["street2"].ToString();
                                adress.zipcode = reader["zipcode"].ToString();
                                adress.address_type = reader["address_type"].ToString();
                                adress.type_name = this.GetTypeName((reader["address_type"]).ToString());
                                contact.Adresslist.Add(adress);
                            }
                        }
                        contact.Contactnoslist = contact.Contactnoslist.GroupBy(x => x.number, (key, group) => group.First()).ToList();
                        contact.Emailslist = contact.Emailslist.GroupBy(x => x.email, (key, group) => group.First()).ToList();
                        contact.websiteslist = contact.websiteslist.GroupBy(x => x.URLs, (key, group) => group.First()).ToList();

                        model.Add(contact);
                    }

                }
            }
            catch (Exception ex)
            {
                commonDAO.WriteToFile("GetPendingSyncRecords error" + ex.Message);
            }
            finally
            {
                db.closeConnection();
            }
            #region Validation
            var chanellist = from chanel in model
                             from adds in chanel.Contactnoslist
                             where adds.number != null
                             select chanel;
            #endregion
            chanellist = chanellist.GroupBy(x => x.GUID, (key, group) => group.First()).ToList();
            return chanellist.Distinct().ToList();
        }
        #endregion

        #region GetDeletedRecords
        public List<GUIDLIST> GetDeletedRecords(string syncdate, string user_id, string zone)
        {
            List<GUIDLIST> model = new List<GUIDLIST>();
            List<SGIDList> models = new List<SGIDList>();
            try
            {
                string SGID = commonDAO.GetSGID(user_id);
                SqlParameter[] sqlParameter = new SqlParameter[1];
                sqlParameter[0] = new SqlParameter("@SGID", SqlDbType.VarChar);
                sqlParameter[0].Value = SGID;
                using (IDataReader readers = db.executeSelectQuery("GetSubordinates", sqlParameter, CommandType.StoredProcedure))
                {
                    while (readers.Read())
                    {
                        models.Add(new SGIDList { sgid = readers["id"].ToString() });
                    }
                }
                models.Add(new SGIDList { sgid = user_id });
                models.Add(new SGIDList { sgid = "0" });

                SqlParameter[] sqlParameterNEW = new SqlParameter[2];
                sqlParameterNEW[0] = new SqlParameter("@SGID", SqlDbType.VarChar);
                sqlParameterNEW[0].Value = SGID;
                sqlParameterNEW[1] = new SqlParameter("@zone", SqlDbType.VarChar);
                sqlParameterNEW[1].Value = zone;
                using (IDataReader readerNew = db.executeSelectQuery("GetWithoutSubordinate", sqlParameterNEW, CommandType.StoredProcedure))
                {
                    while (readerNew.Read())
                    {
                        models.Add(new SGIDList { sgid = readerNew["id"].ToString() });
                    }
                }

               model.Distinct().ToList() ;  


                System.Data.DataTable _myDataTable = new System.Data.DataTable();// Convert array to Data Table
                _myDataTable.Columns.Add("SGID");

                for (int i = 0; i < models.Count; i++)
                {
                    string userid = (models[i].sgid);
                    _myDataTable.Rows.Add(userid);
                }

                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@syncdate", SqlDbType.DateTime);
                if (!string.IsNullOrEmpty(syncdate))
                {
                    DateTime syncdates = Convert.ToDateTime(syncdate);
                    DateTime date = DateTime.ParseExact(syncdates.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                    syncdate = date.ToString("yyyy-MM-dd HH:mm:ss");
                }

                param[0].Value = string.IsNullOrEmpty(syncdate) ? (object)DBNull.Value : syncdate;
                param[1] = new SqlParameter("@user_id", SqlDbType.Int);
                param[1].Value = string.IsNullOrEmpty(user_id) ? (object)DBNull.Value : Convert.ToInt32(user_id);
                param[2] = new SqlParameter("@zone", SqlDbType.VarChar);
                param[2].Value = string.IsNullOrEmpty(zone) ? (object)DBNull.Value : (zone);
                param[3] = new SqlParameter("@List", SqlDbType.Structured);
                param[3].Value = _myDataTable;

               // commonDAO.WriteToFile(syncdate+ "SyncDate");
               
               // string query="select vcf.GU_id as GUID from  GUIDMaster vcf where vcf.isdelete =1 and CAST('2017-11-29 13:30:30' AS datetime ) >=isnull(CAST('2017-11-29 13:30:30' AS datetime), vcf.updatedon) and vcf.zone='WEST'";
                using (IDataReader reader = db.executeSelectQuery("GetDeletedRecords", param, CommandType.Text))
                {
                    while (reader.Read())
                    {
                        GUIDLIST contact = null;
                        contact = new GUIDLIST();
                        contact.GUID = Convert.ToString(reader["GUID"]);
                        contact.IsPersonal = Convert.ToString(reader["IsPersonal"]);
                        contact.user_id = user_id; 
                        model.Add(contact);
                    }

                }
            }
            catch (Exception ex)
            {
              commonDAO.WriteToFile(ex.Message +"Deleted Record Exception");
              commonDAO.WriteToFile(syncdate + "Sync Date"); 
            }
            finally
            {
                db.closeConnection();
            }
            model = model.GroupBy(x => x.GUID, (key, group) => group.First()).ToList();
            return model.Distinct().ToList();
        }
        #endregion

        #region GetLastSyncdate
        public string GetLastSyncdate(string user_id)
        {
            string syncDate = null;
            try
            {
                string query = "SELECT syncdate from syncdetails where user_id='" + user_id + "' ";
                SqlParameter[] param = new SqlParameter[1];

                SqlDataReader dr = db.executeSelectQuery(query, null);
                while (dr.Read())
                {
                    syncDate = (dr[0].ToString());
                }
                return syncDate;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region DeleteRequest
        public bool DeleteRequest(string GUID, string user_id)
        {

            try
            {
                string vcf_id = commonDAO.GetVcardID("GUIDMaster", "GU_id", GUID); // get vcf ID based on File name
                string query = " update ContactMaster  set IsDeleteRequest=1  where GUID ='" + vcf_id + "'";
                bool valid = db.executeUpdateQuery(query, null);

                string query1 = " update GUIDMaster  set DRequestedBy ='" + user_id + "'  where GU_id ='" + GUID + "'";
                valid = db.executeUpdateQuery(query1, null);
                return valid;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region DeleteRequestDeny
        public bool DeleteRequestDeny(string GUID)
        {
            try
            {
                string query = " update ContactMaster  set IsDeleteRequest=0  where GUID ='" + GUID + "'";
                bool valid = db.executeUpdateQuery(query, null);
                return valid;
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region GetDeleteformation
        public List<Contact> GetDeleteformation()
        {
            List<Contact> model = new List<Contact>();
            List<SGIDList> models = new List<SGIDList>();
            try
            {
                if (APPSession.Current.Employee_Name == "SUPERADMIN")
                {
                    using (IDataReader reader = db.executeSelectQuery("GetDeleteformation", null, CommandType.StoredProcedure))
                    {
                        while (reader.Read())
                        {
                            Contact contact = null;
                            int vcf_id = Convert.ToInt32(reader["vcf_id"]);
                            contact = model.FirstOrDefault(q => q.vcf_id == vcf_id);
                            if (contact == null)
                            {
                                contact = new Contact();
                                contact.firstname = Convert.ToString(reader["first_name"]);
                                contact.middlename = Convert.ToString(reader["middle_name"]);
                                contact.lastname = Convert.ToString(reader["last_name"]);
                                contact.prefix = Convert.ToString(reader["prefix"]);
                                contact.sufix = Convert.ToString(reader["sufix"]);
                                contact.company = Convert.ToString(reader["organisation"]);
                                contact.jobtitle = Convert.ToString(reader["title"]);
                                contact.SGID = Convert.ToString(reader["SGID"]);
                                contact.vcf_id = Convert.ToInt32(reader["vcf_id"]);
                                contact.addedby = Convert.ToInt32(reader["addedby"]);
                                contact.addedon = Convert.ToDateTime(reader["addedon"]);
                                contact.GUID = Convert.ToString(reader["GUID"]);
                                contact.IsDeleteRequest = string.IsNullOrEmpty((reader["IsDeleteRequest"]).ToString()) ? false : Convert.ToBoolean(reader["IsDeleteRequest"]);
                            }
                            Contactnos contactno = null;
                            if (reader["contactno_id"] != DBNull.Value)
                            {
                                int contactno_id = Convert.ToInt32(reader["contactno_id"]);
                                contactno = contact.Contactnoslist.FirstOrDefault(o => o.id == contactno_id);
                                if (contactno == null)
                                {
                                    contactno = new Contactnos();
                                    contactno.id = Convert.ToInt32(reader["contactno_id"]);
                                    contactno.number = reader["contactno"].ToString();
                                    contactno.number_type = Convert.ToString(reader["number_type"]);
                                    contactno.type_name = this.GetTypeName((reader["number_type"]).ToString());
                                    contact.Contactnoslist.Add(contactno);
                                }
                            }

                            Emails emails = null;
                            if (reader["email_id"] != DBNull.Value)
                            {
                                int email_id = Convert.ToInt32(reader["email_id"]);
                                emails = contact.Emailslist.FirstOrDefault(q => q.id == email_id);

                                if (emails == null)
                                {
                                    emails = new Emails();
                                    emails.id = Convert.ToInt32(reader["email_id"]);
                                    emails.email = Convert.ToString(reader["email"]);
                                    emails.email_type = Convert.ToString(reader["email_type"]);
                                    string typename = this.GetTypeName((reader["email_type"]).ToString());
                                    emails.type_name = typename;
                                    contact.Emailslist.Add(emails);
                                }
                            }

                            Websites websites = null;
                            if (reader["website_id"] != DBNull.Value)
                            {
                                int website_id = Convert.ToInt32(reader["website_id"]);
                                websites = contact.websiteslist.FirstOrDefault(q => q.id == website_id);

                                if (websites == null)
                                {
                                    websites = new Websites();
                                    websites.id = Convert.ToInt32(reader["website_id"]);
                                    websites.URLs = Convert.ToString(reader["website"]);
                                    contact.websiteslist.Add(websites);
                                }
                            }

                            Adress adress = null;
                            if (reader["address_id"] != DBNull.Value)
                            {
                                int address_id = Convert.ToInt32(reader["address_id"]);
                                adress = contact.Adresslist.FirstOrDefault(c => c.id == address_id);
                                if (adress == null)
                                {
                                    adress = new Adress();
                                    adress.id = Convert.ToInt32(reader["address_id"]);
                                    adress.city = Convert.ToString(reader["city"]);
                                    adress.street1 = Convert.ToString(reader["street1"]);
                                    adress.street2 = Convert.ToString(reader["street2"]);
                                    adress.state = Convert.ToString(reader["state"]);
                                    adress.country = (reader["country"].ToString());// Convert.ToInt32(reader["domain_id"]);
                                    adress.zipcode = reader["zipcode"].ToString();
                                    adress.address_type = reader["address_type"].ToString();
                                    string type_name = this.GetTypeName((reader["address_type"]).ToString());
                                    adress.type_name = type_name;
                                    contact.Adresslist.Add(adress);
                                }
                            }

                            model.Add(contact);
                        }

                    }
                }
               
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            #region Validation
            var chanellist = from chanel in model
                             from adds in chanel.Contactnoslist
                             where adds.number != null
                             select chanel;
            #endregion
            chanellist = chanellist.GroupBy(x => x.GUID, (key, group) => group.First()).ToList();
            return chanellist.Distinct().ToList();
        }

      
        #endregion

        #region Merger
        public int  Merger(List<VCF> vcflist)
        {
            bool IsValid = false;


            #region Compare

            vcflist = vcflist.GroupBy(x => x.file_name).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records

            List<Contactnos> phone = new List<Contactnos>();
            List<Contactnos> phoneTemp = new List<Contactnos>();
            List<Emails> email = new List<Emails>();
            List<Emails> emailTemp = new List<Emails>();
            List<Websites> uRL = new List<Websites>();
            List<Websites> uRLTemp = new List<Websites>();
            List<Adress> address = new List<Adress>();
            List<Adress> addressTemp = new List<Adress>();

            Contact firstFile_Content = new Contact();
            string first_file = vcflist[0].file_name; // First File Name Fetch
            List<Contact> contact = this.GetContactInformationOnID(first_file);
            firstFile_Content = contact[0];
            Contact SecondFile_Content = new Contact();


            if (firstFile_Content.contactlist != null)
            {
                phone = firstFile_Content.Contactnoslist.Distinct().ToList();// Eliminate Duplicate Records
                phone.RemoveAll(x => x.number == null); //Remove Null Entry Based on Field Name
            }

            if (firstFile_Content.Emailslist != null)
            {
                email = firstFile_Content.Emailslist.Distinct().ToList();// Eliminate Duplicate Records
                email.RemoveAll(x => x.email == null); //Remove Null Entry Based on Field Name
            }

            if (firstFile_Content.URLlist != null)
            {
                uRL = firstFile_Content.websiteslist.Distinct().ToList();// Eliminate Duplicate Records
                uRL.RemoveAll(x => x.URLs == null); //Remove Null Entry Based on Field Name

            }

            if (firstFile_Content.Adresslist != null)
            {
                address = firstFile_Content.Adresslist.Distinct().ToList();// Eliminate Duplicate Records
                address.RemoveAll(x => x.address_type == null && x.city == null && x.country == null && x.state == null && x.zipcode == null && x.street1 == null && x.street2 == null);
            }

            for (int j = 1; j < vcflist.Count; j++)
            {
                if (vcflist.Count > 0)
                {
                    string SecondFile = vcflist[j].file_name; // First File Name Fetch
                    List<Contact> Contact_List = this.GetContactInformationOnID(SecondFile);
                    SecondFile_Content = Contact_List[0];


                    #region Other
                    if (SecondFile_Content.firstname != null && firstFile_Content.firstname != null)
                    {
                        if (firstFile_Content.firstname.Length >= SecondFile_Content.firstname.Length)
                        {
                            firstFile_Content.firstname = SecondFile_Content.firstname;
                        }
                        else
                        {
                            firstFile_Content.firstname = SecondFile_Content.firstname;
                        }
                    }

                    if (SecondFile_Content.middlename != null && firstFile_Content.middlename != null)
                    {
                        if (firstFile_Content.middlename.Length >= SecondFile_Content.middlename.Length)
                        {
                            firstFile_Content.middlename = SecondFile_Content.middlename;
                        }
                        else
                        {
                            firstFile_Content.middlename = SecondFile_Content.middlename;
                        }

                    }

                    if (SecondFile_Content.lastname != null && firstFile_Content.lastname != null)
                    {
                        if (firstFile_Content.lastname.Length >= SecondFile_Content.lastname.Length)
                        {
                            firstFile_Content.lastname = SecondFile_Content.lastname;
                        }
                        else
                        {
                            firstFile_Content.lastname = SecondFile_Content.lastname;
                        }
                    }

                    if (firstFile_Content.prefix == null)
                    {
                        firstFile_Content.prefix = SecondFile_Content.prefix;
                    }
                    if (firstFile_Content.sufix == null)
                    {
                        firstFile_Content.sufix = SecondFile_Content.sufix;
                    }

                    if (firstFile_Content.company == null && SecondFile_Content.company != null)
                    {
                        firstFile_Content.company = SecondFile_Content.company;
                    }

                    if (firstFile_Content.jobtitle == null && SecondFile_Content.jobtitle != null)
                    {
                        firstFile_Content.jobtitle = SecondFile_Content.jobtitle;
                    }


                    #endregion

                    #region Eliminate Duplicates By Combining 2 vcf Phone Numbers
                    if (firstFile_Content.Contactnoslist != null && SecondFile_Content.Contactnoslist != null)
                    {
                        phoneTemp = phone;// Retail Previous assigned value
                        phone = firstFile_Content.Contactnoslist.Union(SecondFile_Content.Contactnoslist != null ? SecondFile_Content.Contactnoslist : Enumerable.Empty<Contactnos>()).ToList(); // Conact 2 List with eliminating duplicates

                        if (phoneTemp != null)
                        {
                            phone = phone.Union(phoneTemp).ToList();

                        }
                        phone = phone.GroupBy(x => x.number).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        phone.RemoveAll(x => x.number == null); //Remove Null Entry Based on Field Name
                        phone = phone.Distinct().ToList();
                    }
                    else if (firstFile_Content.Contactnoslist != null && SecondFile_Content.Contactnoslist == null)
                    {
                        phoneTemp = phone;
                        phone = firstFile_Content.Contactnoslist.ToList();
                        if (phoneTemp != null)
                        {
                            phone = phone.Union(phoneTemp).ToList();

                        }
                        phone = phone.GroupBy(x => x.number).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        phone.RemoveAll(x => x.number == null); //Remove Null Entry Based on Field Name
                        phone = phone.Distinct().ToList();
                    }
                    else if (firstFile_Content.Contactnoslist == null && SecondFile_Content.Contactnoslist != null)
                    {
                        phoneTemp = phone;
                        phone = SecondFile_Content.Contactnoslist.ToList();
                        if (phoneTemp != null)
                        {
                            phone = phone.Union(phoneTemp).ToList();

                        }
                        phone = phone.GroupBy(x => x.number).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        phone.RemoveAll(x => x.number == null); //Remove Null Entry Based on Field Name
                        phone = phone.Distinct().ToList();
                    }

                    #endregion

                    #region Eliminate Duplicates By Combining 2 vcf(Email)
                    if (firstFile_Content.Emailslist != null && SecondFile_Content.Emailslist != null)
                    {
                        emailTemp = email;
                        email = firstFile_Content.Emailslist.Union(SecondFile_Content.Emailslist != null ? SecondFile_Content.Emailslist : Enumerable.Empty<Emails>()).ToList();

                        if (emailTemp != null)
                        {
                            email = email.Union(emailTemp).ToList();

                        }
                        email = email.GroupBy(x => x.email).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        email.RemoveAll(x => x.email == null); //Remove Null Entry Based on Field Name
                        email = email.Distinct().ToList();
                    }
                    else if (firstFile_Content.Emailslist != null && SecondFile_Content.Emailslist == null)
                    {
                        emailTemp = email;
                        email = firstFile_Content.Emailslist.ToList();
                        if (emailTemp != null)
                        {
                            email = email.Union(emailTemp).ToList();

                        }
                        email = email.GroupBy(x => x.email).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        email.RemoveAll(x => x.email == null); //Remove Null Entry Based on Field Name
                        email = email.Distinct().ToList();
                    }
                    else if (firstFile_Content.Emailslist == null && SecondFile_Content.Emailslist != null)
                    {
                        emailTemp = email;
                        email = SecondFile_Content.Emailslist.ToList();
                        if (emailTemp != null)
                        {
                            email = email.Union(emailTemp).ToList();

                        }
                        email = email.GroupBy(x => x.email).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        email.RemoveAll(x => x.email == null); //Remove Null Entry Based on Field Name
                        email = email.Distinct().ToList();
                    }
                    #endregion

                    #region Eliminate Duplicates By Combining 2 vcf(URLS)
                    if (firstFile_Content.websiteslist != null && SecondFile_Content.websiteslist != null)
                    {
                        uRLTemp = uRL;
                        uRL = firstFile_Content.websiteslist.Union(SecondFile_Content.websiteslist != null ? SecondFile_Content.websiteslist : Enumerable.Empty<Websites>()).ToList();
                        if (uRLTemp != null)
                        {
                            uRL = uRL.Union(uRLTemp).ToList();

                        }
                        uRL = uRL.GroupBy(x => x.URLs).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        uRL.RemoveAll(x => x.URLs == null); //Remove Null Entry Based on Field Name
                        uRL = uRL.Distinct().ToList();
                    }
                    else if (firstFile_Content.websiteslist != null && SecondFile_Content.websiteslist == null)
                    {
                        uRLTemp = uRL;
                        uRL = firstFile_Content.websiteslist.ToList();
                        if (uRLTemp != null)
                        {
                            uRL = uRL.Union(uRLTemp).ToList();

                        }
                        uRL = uRL.GroupBy(x => x.URLs).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        uRL.RemoveAll(x => x.URLs == null); //Remove Null Entry Based on Field Name
                        uRL = uRL.Distinct().ToList();
                    }
                    else if (firstFile_Content.websiteslist == null && SecondFile_Content.websiteslist != null)
                    {
                        uRLTemp = uRL;
                        uRL = SecondFile_Content.websiteslist.ToList();
                        if (uRLTemp != null)
                        {
                            uRL = uRL.Union(uRLTemp).ToList();

                        }
                        uRL = uRL.GroupBy(x => x.URLs).Select(x => x.FirstOrDefault()).ToList();// Eliminate Duplicate Records
                        uRL.RemoveAll(x => x.URLs == null); //Remove Null Entry Based on Field Name
                        uRL = uRL.Distinct().ToList();
                    }
                    #endregion

                    #region Eliminate Duplicates By Combining 2 vcf(Addresses)
                    if (firstFile_Content.Adresslist != null && SecondFile_Content.Adresslist != null)
                    {
                        addressTemp = address;
                        address = firstFile_Content.Adresslist.Union(SecondFile_Content.Adresslist != null ? SecondFile_Content.Adresslist : Enumerable.Empty<Adress>()).ToList();
                        if (addressTemp != null)
                        {
                            address = address.Union(addressTemp).ToList();
                        }
                        address = address.Distinct().ToList();// Eliminate Duplicate Records
                        address.RemoveAll(x => x.address_type == null && x.city == null && x.country == null && x.state == null && x.zipcode == null && x.street1 == null && x.street2 == null);
                        address = address.GroupBy(d => new { d.country, d.state, d.city, d.street1, d.street2, d.zipcode }).Select(d => d.First()).ToList();
                        address = address.Distinct().ToList();

                    }
                    else if (firstFile_Content.Adresslist != null && SecondFile_Content.Adresslist == null)
                    {
                        addressTemp = address;
                        address = firstFile_Content.Adresslist.ToList();
                        if (addressTemp != null)
                        {
                            address = address.Union(addressTemp).ToList();

                        }
                        address = address.Distinct().ToList();
                        address.RemoveAll(x => x.address_type == null && x.city == null && x.country == null && x.state == null && x.zipcode == null && x.street1 == null && x.street2 == null);
                        address = address.GroupBy(d => new { d.country, d.state, d.city, d.street1, d.street2, d.zipcode }).Select(d => d.First()).ToList();
                        address = address.Distinct().ToList();
                    }
                    else if (firstFile_Content.Adresslist == null && SecondFile_Content.Adresslist != null)
                    {
                        addressTemp = address;
                        address = SecondFile_Content.Adresslist.ToList();
                        if (addressTemp != null)
                        {
                            address = address.Union(addressTemp).ToList();

                        }
                        address = address.Distinct().ToList();// Eliminate Duplicate Records
                        address.RemoveAll(x => x.address_type == null && x.city == null && x.country == null && x.state == null && x.zipcode == null && x.street1 == null && x.street2 == null);
                        address = address.GroupBy(d => new { d.country, d.state, d.city, d.street1, d.street2, d.zipcode }).Select(d => d.First()).ToList();
                        address = address.Distinct().ToList();
                    }
                    #endregion


                }
            }
            #endregion

            #region Phone Number

            List<Contactnos> NumbertoDelte = new List<Contactnos>();
            foreach (var item in phone)
            {
                List<Contactnos> contacts = this.getallContact(item.number);
                Validation validation = new Validation();
                foreach (var items in contacts)
                {
                    PhoneNumberUtil pnu = PhoneNumberUtil.GetInstance();
                    PhoneNumbers.PhoneNumberUtil.MatchType mt = pnu.IsNumberMatch(item.number, items.number);
                    if (mt == PhoneNumbers.PhoneNumberUtil.MatchType.NSN_MATCH || mt == PhoneNumbers.PhoneNumberUtil.MatchType.EXACT_MATCH)
                    {
                        NumbertoDelte.Add(items);
                    }
                }
            }

            #endregion

            #region DB Save
            #region SaveVCF
            int values = 0;
            SqlParameter[] param = new SqlParameter[7];
            string query = "spVcfSave";
            //param[0] = new SqlParameter("@name", SqlDbType.VarChar);
            param[0] = new SqlParameter("@ContactID", SqlDbType.Int);
            param[1] = new SqlParameter("@ID", SqlDbType.Int);
            param[2] = new SqlParameter("@GU_id", SqlDbType.VarChar);
            param[3] = new SqlParameter("@zone", SqlDbType.VarChar);
            param[4] = new SqlParameter("@SG_id", SqlDbType.VarChar);
            param[5] = new SqlParameter("@added_by", SqlDbType.VarChar);
            param[6] = new SqlParameter("@IsUpdated", SqlDbType.Bit);

            param[0].Direction = ParameterDirection.Output;
            param[1].Value = 0;
            param[2].Value = Guid.NewGuid().ToString();
            param[3].Value = contact[0].zone;
            param[4].Value = APPSession.Current.SGID.ToString();
            param[5].Value = APPSession.Current.User_id.ToString();
            param[6].Value =false;
            IsValid = db.executeInsertQuery(query, param);

            if (IsValid)
            {
                values = Convert.ToInt32(param[0].Value.ToString());
            }

            #endregion

            #region Contact Master Save

            int results = this.SaveContactMaster(APPSession.Current.User_id.ToString(), firstFile_Content.firstname, firstFile_Content.middlename, firstFile_Content.lastname, firstFile_Content.prefix, firstFile_Content.sufix, firstFile_Content.company, firstFile_Content.jobtitle, values, APPSession.Current.SGID  );
            if (results == 0)
            {
                IsValid = false;
            }
            else
            {
                IsValid = true;
            }
            #endregion

            #region Email Save
            foreach (var item in email)
            {
                int type_id = this.SaveType(item.type_name);
                if (commonDAO.Validate("email", "email", item.email))
                {
                    IsValid = commonDAO.UpdateTable("emailnew", "email", item.email, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());
                    //string id = commonDAO.GetVcardID("email", "email", item.email);
                    //IsValid = commonDAO.UpdateTable("GUIDMaster", "id", id, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                    //IsValid = commonDAO.UpdateTable("ContactMaster", "GUID", id, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in contact master table
                    //IsValid = commonDAO.UpdateTable("address", "GUID", id, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in adress table
            
                }

                int result = SaveEmail(APPSession.Current.User_id.ToString(), item.email, values, type_id);
                if (result == 0)
                {
                    IsValid = false;
                }
                else
                {
                    IsValid = true;
                }
            }
            #endregion

            #region Phone Number Save
            foreach (var item in NumbertoDelte)
            {
                IsValid = commonDAO.UpdateTable("contactnonew", "contactno", item.number, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());

                string id = commonDAO.GetVcardID("contactno", "contactno", item.number);
                
            }
            foreach (var item in phone)
            {
                int type_id = this.SaveType(item.type_name.ToString());
                int result = this.SaveContactNo(APPSession.Current.User_id.ToString(), item.number, values, type_id);
                if (result == 0)
                {
                    IsValid = false;
                }
                else
                {
                    IsValid = true;
                }
            }
            #endregion

            #region URL Save
            foreach (var item in uRL)
            {
                if (commonDAO.Validate("website", "website", item.URLs))
                {
                    IsValid = commonDAO.UpdateTable("websitenew", "website", item.URLs, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());
                }
                int result = this.SaveWebsite(APPSession.Current.User_id.ToString(), item.URLs, values);
                if (result == 0)
                {
                    IsValid = false;
                }
                else
                {
                    IsValid = true;
                }
            }
            #endregion

            #region Address Save
            foreach (var item in address)
            {
                int type_id = this.SaveType(item.type_name.ToString());
                int result = this.SaveAddress(APPSession.Current.User_id.ToString(), item.street1, item.street2, item.city, item.state, item.zipcode, item.country, values, type_id);
                if (result == 0)
                {
                    IsValid = false;
                }
                else
                {
                    IsValid = true;
                }
            }
            #endregion


            List<GUIDLIST> guid = new List<GUIDLIST>();
            List<ContactList> con = new List<ContactList>();
            #region VCF File Delete
            for (int i = 0; i < vcflist.Count; i++)
            {

                string FileToDelete = System.IO.Path.GetFileName(vcflist[i].file_name);
                if (commonDAO.Validate("GUIDMaster", "id", FileToDelete))
                {
                    IsValid = commonDAO.UpdateTables("GUIDMaster", "id", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                    guid.Add(new GUIDLIST() { GUID = vcflist[i].file_name });
                    IsValid = commonDAO.UpdateTable("ContactMaster", "GUID", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in contact master table
                    IsValid = commonDAO.UpdateTable("address", "GUID", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in adress table

                }

            }

            #endregion
            #endregion

            return values;
        }
        #endregion


        #region Merger
        public bool Delete(List<VCF> vcflist)
        {
            bool IsValid = false;

            #region DB Save

            List<GUIDLIST> guid = new List<GUIDLIST>();
            List<ContactList> con = new List<ContactList>();
            #region VCF File Delete
            for (int i = 0; i < vcflist.Count; i++)
            {

                string FileToDelete = System.IO.Path.GetFileName(vcflist[i].file_name);
                if (commonDAO.Validate("GUIDMaster", "id", FileToDelete))
                {
                    IsValid = commonDAO.UpdateTables("GUIDMaster", "id", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString()); // Update Deleted status of vcf file  in VCFMater Table
                    IsValid = commonDAO.UpdateTable("email", "GUID", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in contact master table
                    IsValid = commonDAO.UpdateTable("contactno", "GUID", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in contact master table
                    IsValid = commonDAO.UpdateTable("website", "GUID", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in contact master table
                    IsValid = commonDAO.UpdateTable("ContactMaster", "GUID", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in contact master table
                    IsValid = commonDAO.UpdateTable("address", "GUID", FileToDelete, "updatedby", APPSession.Current.User_id.ToString(), "updatedon", DateTime.ParseExact(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"), "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture).ToString());// Update Deleted status of vcf file  in adress table
                }

            }

            #endregion
            #endregion

            return IsValid;
        }
        #endregion

        #region Call Log
        public bool CallLog(LogDetails logDetails)
        {
            bool valid=true;
            foreach (var item in logDetails.callLog)
            {
                try
                {
                    SqlParameter[] param = new SqlParameter[6];
                    string query = "spcatalogSaveSave";
                    param[0] = new SqlParameter("@empname", SqlDbType.VarChar);
                    param[1] = new SqlParameter("@mobno", SqlDbType.VarChar);
                    param[2] = new SqlParameter("@duration", SqlDbType.VarChar);
                    param[3] = new SqlParameter("@calldate", SqlDbType.DateTime);
                    param[4] = new SqlParameter("@user_id", SqlDbType.Int);
                    param[5] = new SqlParameter("@zone", SqlDbType.VarChar);

                    param[0].Value = string.IsNullOrEmpty(logDetails.empname) ? (object)DBNull.Value : logDetails.empname;
                    param[1].Value = item.mobno;
                    param[2].Value = item.duration;
                    param[3].Value = string.IsNullOrEmpty(item.calldate) ? (object)DBNull.Value : Convert.ToDateTime(item.calldate).ToString("MM/dd/yyyy HH:mm:ss");
                    param[4].Value = logDetails.user_id;
                    param[5].Value = logDetails.zone;
                    valid = db.executeInsertQuery(query, param);
                   // return valid;
                }
                catch (Exception e)
                {
                    valid = false;
                }
                finally
                {
                    db.closeConnection();
                }
            }
            return valid;

        }
        #endregion

        public List<CallLogHistory> GetCallLog(string user_id)
        {
            List<CallLogHistory> model = new List<CallLogHistory>();
            List<SGIDList> models = new List<SGIDList>();
            try
            {
              
                     SqlParameter[] sqlParameter = new SqlParameter[1];
                   sqlParameter[0] = new SqlParameter("@user_id", SqlDbType.VarChar);
                   sqlParameter[0].Value = user_id;
                   using (IDataReader reader = db.executeSelectQuery("getcalllog", sqlParameter, CommandType.StoredProcedure))
                   {
                       while (reader.Read())
                       {
                           CallLogHistory callLogHistory = new CallLogHistory();
                           callLogHistory.id = string.IsNullOrEmpty((reader["id"]).ToString()) ? 0 : Convert.ToInt32(reader["id"].ToString());
                           callLogHistory.empname = string.IsNullOrEmpty((reader["empname"]).ToString()) ? null : (reader["empname"].ToString());
                           callLogHistory.mobno = string.IsNullOrEmpty((reader["mobno"]).ToString()) ? null : (reader["mobno"].ToString());
                           callLogHistory.duration = string.IsNullOrEmpty((reader["duration"]).ToString()) ? null : (reader["duration"].ToString());
                           callLogHistory.calldate = Convert.ToDateTime(reader["calldate"].ToString());
                           callLogHistory.user_id = string.IsNullOrEmpty((reader["user_id"]).ToString()) ? 0 : Convert.ToInt32(reader["user_id"].ToString());
                           callLogHistory.zone = string.IsNullOrEmpty((reader["zone"]).ToString()) ? null : (reader["zone"].ToString());
                           callLogHistory.added_on = string.IsNullOrEmpty((reader["addedon"]).ToString()) ? null : (reader["addedon"].ToString());
                           callLogHistory.SGID = string.IsNullOrEmpty((reader["SG_id"]).ToString()) ? null : (reader["SG_id"].ToString());

                           model.Add(callLogHistory);
                       }

                   }
                }
               
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            model = model.GroupBy(x => x.id, (key, group) => group.First()).ToList();
            return model.Distinct().ToList();
        }


        public List<OTP> GETOTP()
        {
            List<OTP> model = new List<OTP>();
            List<OTP> models = new List<OTP>();
            try
            {
                using (IDataReader reader = db.executeSelectQuery("select sgid,mobileno,message ,reqdatetime from sms", null, CommandType.Text))
                {
                    while (reader.Read())
                    {
                        OTP callLogHistory = new OTP();

                        callLogHistory.SGID = string.IsNullOrEmpty((reader["sgid"]).ToString()) ? null : (reader["sgid"].ToString());
                        callLogHistory.mobileno = string.IsNullOrEmpty((reader["mobileno"]).ToString()) ? null : (reader["mobileno"].ToString());
                        callLogHistory.createdon = string.IsNullOrEmpty((reader["reqdatetime"]).ToString()) ? null : (reader["reqdatetime"].ToString());
                        callLogHistory.otp = string.IsNullOrEmpty((reader["message"]).ToString()) ? null : (reader["message"].ToString());
                        model.Add(callLogHistory);
                    }

                }
            }

            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            return model.Distinct().ToList();
        }


        public bool truncate()
        {
            bool valid = true;
            try
            {
               
                string query = "truncatetables";
                valid = db.executeInsertQuery(query, null);
            }

            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            return valid;
        }
    }
}
