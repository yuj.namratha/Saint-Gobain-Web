﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common.DAO
{
    public class RoleDAO
    {
        DBConnection db = new DBConnection();
        public UserRoleMapping GetUserRoles(int user_id)
        {
            UserRoleMapping model = new UserRoleMapping();
            try
            {
                SqlParameter[] param_2 = new SqlParameter[1];
                param_2[0] = new SqlParameter("@user_id", SqlDbType.Int);
                param_2[0].Value = user_id;
                SqlDataReader dr = db.executeSelectQuery("getUserRoles", param_2);
                while (dr.Read())
                {
                    model.role_id = Convert.ToInt32(dr["role_id"]);
                    model.user_id = Convert.ToInt32(dr["user_id"]);
                    model.user_role_id = Convert.ToInt32(dr["user_role_id"]);
                    int qid = Convert.ToInt32(dr["permission_id"]);
                    Permission permission = null;
                    permission = model.permissions.SingleOrDefault(p => p.permission_id == qid);
                    if (permission == null)
                    {
                        permission = new Permission();
                        permission.permission_id = Convert.ToInt32(dr["permission_id"]);
                        permission.permission_name = dr["permission"].ToString();
                        model.permissions.Add(permission);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            return model;
        }
    }
}
