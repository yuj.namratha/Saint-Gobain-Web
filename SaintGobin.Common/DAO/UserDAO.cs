﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.DirectoryServices;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common.DAO
{
    public class UserDAO
    {
        DBConnection db = new DBConnection();
        CommonDAO commonDAO = new CommonDAO();
        #region Get Profile
        public User getProfile(string userName)
        {
            SqlDataReader dr;
            User currentuser = new User();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@userName", SqlDbType.VarChar);
                param[0].Value = string.IsNullOrEmpty(userName) ? (object)DBNull.Value : userName;
                dr = db.executeSelectQuery("GetProfile", param);
                while (dr.Read())
                {
                    currentuser.User_id = Convert.ToInt32(dr[0]);
                    currentuser.SGID=  dr[1].ToString();
                    currentuser.zone=  dr[8].ToString();
                    currentuser.firstname=  dr[5].ToString();
                    currentuser.middlename=  dr[6].ToString();
                    currentuser.lastname=  dr[7].ToString();
                    currentuser.reporting_person=  dr[3].ToString();
                    currentuser.reporting_person_id=  dr[4].ToString();
                    currentuser.date_of_joining=  dr[9].ToString();
                    currentuser.date_of_relieving=  dr[10].ToString();
                    currentuser.mobile_no = dr[2].ToString();
                    currentuser.Employee_Name = dr[5].ToString().PadRight(5) + dr[6].ToString().PadRight(5) + dr[7].ToString();
                    currentuser.IsExist = commonDAO.ValidateCommon("syncdetails", "user_id", Convert.ToInt32(dr[0]).ToString());

                }
            }
            catch (Exception ex)
            {
                commonDAO.WriteToFile(ex.Message + "GetProfile Error"); ;
            }
            finally
            {
                db.closeConnection();
            }
            return currentuser;
        }
        #endregion

        #region Get Users
        public List<User>  getUsers()
        {

            SqlDataReader dr;
            List<User> model = new List<User>();
            try
            {
                SqlParameter[] param = new SqlParameter[1];
                param[0] = new SqlParameter("@userName", SqlDbType.VarChar);
                param[0].Value = DBNull.Value ;
                dr = db.executeSelectQuery("GetProfile", param);
                User currentuser =null;
                while (dr.Read())
                {
                    currentuser = new User();
                    currentuser.User_id = Convert.ToInt32(dr[0]);
                    currentuser.SGID = dr[1].ToString();
                    currentuser.zone = dr[8].ToString();
                    currentuser.firstname = dr[5].ToString();
                    currentuser.middlename = dr[6].ToString();
                    currentuser.lastname = dr[7].ToString();
                    currentuser.reporting_person = dr[3].ToString();
                    currentuser.reporting_person_id = dr[4].ToString();
                    currentuser.date_of_joining = dr[9].ToString();
                    currentuser.date_of_relieving = dr[10].ToString();
                    currentuser.mobile_no = dr[2].ToString();
                    model.Add(currentuser); 
                    
                }
                
            }
            catch (Exception ex)
            {
                commonDAO.WriteToFile(ex.Message + "getUsers Error");
            }
            finally
            {
                db.closeConnection();
            }
            return model;
        }
        #endregion

        #region Validate User
        public bool ValidateUser(string LoginId, string Password)
        {
            bool valid = true;

            if (ConfigurationManager.AppSettings["SLDAP"] == "Yes")
            {
                string strLDAPPath = ConfigurationManager.ConnectionStrings["LDAPConnection"].ConnectionString;
                string strDomain = ConfigurationManager.AppSettings["Domain"];

                try
                {
                    DirectoryEntry objLDAP = new DirectoryEntry(strLDAPPath, strDomain + "\\" + LoginId.Trim(), Password.Trim());
                    DirectorySearcher objSearcher = new DirectorySearcher(objLDAP);
                    if (ConfigurationManager.AppSettings["SLDAP"] == "Yes")
                        objLDAP.AuthenticationType = AuthenticationTypes.Secure | AuthenticationTypes.SecureSocketsLayer;
                    objSearcher.Filter = "(&(objectClass=user)(SAMAccountName=" + LoginId.Trim() + "))";
                    SearchResult objLDAPResult = objSearcher.FindOne();

                    if (objLDAPResult != null)
                    {
                        valid = true;
                    }
                    else
                    {
                        valid = false;
                    }
                }
                catch (Exception ex)
                {
                    valid = false;
                    commonDAO.WriteToFile(ex.Message + "LDAP Authentication Failed");
                }
            }
            else 
            {
               if (ValidateUserExist(LoginId,Password))
                {
                    valid = true;
                }
                else 
                {
                    valid = false;
                }
            }
            return valid;
        }

        public bool ValidateUserExist(string SG_id, string mobile_no)
        {
            try
            {
                string query = "IF EXISTS (select *from usermaster where SG_id='"+SG_id+"' and mobile_no='"+mobile_no+"' ) select 1 Else select 0";

                int result = Convert.ToInt32(db.executeScalarQuery(query, null));
                if (result == 0)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region  Insert Device Details
        public bool Submit_DeviceDetails(DeviceDetails data)
        {
            DataTable dt = new DataTable();
            DBConnection db = new DBConnection();
            int IsUserDeviceEntryExist = 0;
            bool IsValid = true;
            IsUserDeviceEntryExist = this.IsUserExistUser(data.user_id);
            try
            {
                if (IsUserDeviceEntryExist != 1)
                {
                    int id = db.generateMaxID("tbldevice", "SL_NO ");
                     SqlParameter[] param = new SqlParameter[3];
                     string query = "sptDeviceDetailSave";
                        param[0] = new SqlParameter("@SL_NO", SqlDbType.Int);
                        param[1] = new SqlParameter("@sgid", SqlDbType.VarChar);
                        param[2] = new SqlParameter("@gsm_id", SqlDbType.VarChar);

                        param[0].Value =id;
                        param[1].Value = data.user_id;
                        param[2].Value =  data.gsm_ids;
                        IsValid = db.executeInsertQuery(query, param);
                }
                else
                {
                    IsValid = UpdateGSMID(data.user_id.ToString() , data.gsm_ids);
                }
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.closeConnection();
            }
          

            return IsValid;
        }
        #endregion

        #region Is User Device Details Exist
        public int IsUserExistUser(string sgid)
        {
            try
            {
                string query = " IF EXISTS(SELECT 1 FROM dbo.tbldevice  WHERE  sgid= " + sgid + ")" +
                             "BEGIN select 1 as result END ELSE BEGIN select 0 as result END";
                var result = Convert.ToInt32(db.executeScalarQuery(query, null));
                return result;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }

        }

        #endregion

        #region Update Device Details
        public static bool UpdateGSMID(string sgid, string gsm_id)
        {
            DBConnection db = new DBConnection();
            var result = false;
            try
            {

                string query = "update tbldevice set gsm_id='" + gsm_id + "'  where sgid='" + sgid + "'";

                result = db.executeUpdateQuery(query, null);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                db.closeConnection();
            }
            return result;
        }

        #endregion

        #region  Insert SMS Details
        public bool SubmitSMSDetails(string sgid, string mobileno, string message, bool IsSent, DateTime reqdatetime, DateTime processdatetime, string returnMsg)
        {
            bool IsValid = true;
            try
            {
                    SqlParameter[] param = new SqlParameter[7];
                    string query = "spSMSDetailSave";
                    param[0] = new SqlParameter("@sgid", SqlDbType.VarChar);
                    param[1] = new SqlParameter("@mobileno", SqlDbType.VarChar);
                    param[2] = new SqlParameter("@message", SqlDbType.VarChar);
                    param[3] = new SqlParameter("@IsSent", SqlDbType.VarChar);
                    param[4] = new SqlParameter("@reqdatetime", SqlDbType.DateTime);
                    param[5] = new SqlParameter("@processdatetime", SqlDbType.DateTime);
                    param[6] = new SqlParameter("@returnMsg", SqlDbType.VarChar);

                    param[0].Value = sgid;
                    param[1].Value = mobileno;
                    param[2].Value = message;
                    param[3].Value = IsSent;
                    param[4].Value = reqdatetime;
                    param[5].Value = processdatetime;
                    param[6].Value = returnMsg;
                    IsValid = db.executeInsertQuery(query, param);
            }
            catch (Exception e)
            {
            }
            finally
            {
                db.closeConnection();
            }
            return IsValid;
        }
        #endregion
    }
}
