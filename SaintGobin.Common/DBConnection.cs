﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common
{
    public class DBConnection
    {
        private SqlConnection conn;

        public DBConnection()
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConn"].ConnectionString);
        }

        public DBConnection(string getDb)
        {
            conn = new SqlConnection(ConfigurationManager.ConnectionStrings["DBConnUser"].ConnectionString);
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        public SqlConnection openConnection()
        {
            if (conn.State == ConnectionState.Closed || conn.State == ConnectionState.Broken)
            {
                conn.Open();
            }
            return conn;
        }

        /// <method>
        /// Open Database Connection if Closed or Broken
        /// </method>
        public void closeConnection()
        {
            if (conn.State == ConnectionState.Open)
            {
                conn.Close();
                //conn.Dispose();
                //SqlConnection.ClearPool(conn);
            }
        }
        public bool checkExist(string _tblName, string _idCol, int id)
        {
            SqlCommand cmd = new SqlCommand();
            try
            {
                cmd.CommandText = " IF EXISTS(SELECT 1 FROM " + _tblName + " WHERE  " + _idCol + " = " + id + ")" +
                             "BEGIN select 1 as result END ELSE BEGIN select 0 as result END";
                cmd.Connection = openConnection();
                var result = cmd.ExecuteScalar();

                return Convert.ToBoolean(result);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                closeConnection();
            }
        }

        /// <method>
        /// Select Query
        /// </method>
        public SqlDataReader executeSelectQuery(String _query, SqlParameter[] sqlParameter, CommandType cmdType = CommandType.Text)
        {
            SqlCommand myCommand = new SqlCommand();

            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                myCommand.CommandType = cmdType;
                //myCommand.CommandTimeout = 1000000000;
                if (sqlParameter != null)
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.Parameters.AddRange(sqlParameter);
                }
                SqlDataReader dr = myCommand.ExecuteReader();

                return dr;
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeSelectQuery - Query:  " + _query + " \nException: " + e.StackTrace.ToString());
                return null;
            }


        }


        public int generateMaxID(string tblName, string columnName)
        {
            SqlCommand cmd = new SqlCommand();
            int id;
            try
            {
                cmd.CommandText = "select max(" + columnName + ")from " + tblName + "";
                cmd.Connection = openConnection();
                var result = cmd.ExecuteScalar();
                if (!Convert.IsDBNull(result))
                {
                    id = Convert.ToInt32(result) + 1;
                }
                else
                {
                    id = 1;
                }
                closeConnection();
            }
            catch (Exception)
            {
                closeConnection();
                throw;

            }
            finally
            {
                closeConnection();
            }
            return id;
        }

        /// <method>
        /// Insert Query
        /// </method>
        public bool executeInsertQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                if (sqlParameter != null)
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.Parameters.AddRange(sqlParameter);
                }
                //myAdapter.InsertCommand = myCommand;
                myCommand.ExecuteNonQuery();

            }

            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeInsertQuery - Query: " + _query + " \nException: \n" + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                closeConnection();
            }
            return true;
        }

        /// <method>
        /// Update Query
        /// </method>
        public bool executeUpdateQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                if (sqlParameter != null)
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.Parameters.AddRange(sqlParameter);
                }
                //myAdapter.UpdateCommand = myCommand;
                int count = myCommand.ExecuteNonQuery();


                closeConnection();
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeUpdateQuery - Query" + _query + " \nException: " + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                closeConnection();
            }
            return true;
        }


        /// <method>
        /// Update Query
        /// </method>
        public string executeScalarQuery(String _query, SqlParameter[] sqlParameter, CommandType cmdType = CommandType.Text)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                string count = "0";
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;

                myCommand.CommandType = cmdType;

                if (sqlParameter != null)
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.Parameters.AddRange(sqlParameter);
                }



                var result = myCommand.ExecuteScalar();
                if (result != null)
                {
                    count = result.ToString();
                }
                return count;
            }
            catch (SqlException)
            {

                throw;
            }
            finally
            {
                closeConnection();
            }
        }

        /// <method>
        /// Update Query
        /// </method>
        public bool executeDeleteQuery(String _query, SqlParameter[] sqlParameter)
        {
            SqlCommand myCommand = new SqlCommand();
            try
            {
                myCommand.Connection = openConnection();
                myCommand.CommandText = _query;
                if (sqlParameter != null)
                {
                    myCommand.CommandType = CommandType.StoredProcedure;
                    myCommand.Parameters.AddRange(sqlParameter);
                }
                myCommand.ExecuteNonQuery();
            }
            catch (SqlException e)
            {
                Console.Write("Error - Connection.executeUpdateQuery - Query" + _query + " \nException: " + e.StackTrace.ToString());
                return false;
            }
            finally
            {
                closeConnection();
            }
            return true;
        }
    }
}
