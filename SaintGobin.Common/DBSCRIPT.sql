USE [master]
GO
/****** Object:  Database [Saint-Gobin]    Script Date: 5/29/2017 4:16:43 PM ******/
CREATE DATABASE [Saint-Gobin]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'Saint-Gobin', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Saint-Gobin.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'Saint-Gobin_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\Saint-Gobin_log.ldf' , SIZE = 832KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [Saint-Gobin] SET COMPATIBILITY_LEVEL = 100
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Saint-Gobin].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Saint-Gobin] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Saint-Gobin] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Saint-Gobin] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Saint-Gobin] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Saint-Gobin] SET ARITHABORT OFF 
GO
ALTER DATABASE [Saint-Gobin] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Saint-Gobin] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Saint-Gobin] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Saint-Gobin] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Saint-Gobin] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Saint-Gobin] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Saint-Gobin] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Saint-Gobin] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Saint-Gobin] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Saint-Gobin] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Saint-Gobin] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Saint-Gobin] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Saint-Gobin] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Saint-Gobin] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Saint-Gobin] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Saint-Gobin] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Saint-Gobin] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Saint-Gobin] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Saint-Gobin] SET  MULTI_USER 
GO
ALTER DATABASE [Saint-Gobin] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Saint-Gobin] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Saint-Gobin] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Saint-Gobin] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Saint-Gobin] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Saint-Gobin]
GO
/****** Object:  Table [dbo].[address]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[address](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[street] [varchar](30) NULL,
	[Pobox] [varchar](10) NULL,
	[Neiborhood] [varchar](50) NULL,
	[city] [varchar](10) NULL,
	[state] [varchar](10) NULL,
	[zipcode] [int] NULL,
	[country] [varchar](50) NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_address] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[contactmaster]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contactmaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[firstname] [varchar](20) NOT NULL,
	[middlename] [varchar](20) NULL,
	[lastname] [varchar](20) NULL,
	[email1] [int] NULL,
	[email2] [int] NULL,
	[email3] [int] NULL,
	[email1_type] [int] NULL,
	[email2_type] [int] NULL,
	[email3_type] [int] NULL,
	[add1] [int] NULL,
	[add2] [int] NULL,
	[add3] [int] NULL,
	[add1_type] [int] NULL,
	[add2_type] [int] NULL,
	[add3_type] [int] NULL,
	[website1] [int] NULL,
	[website2] [varchar](50) NULL,
	[website3] [varchar](50) NULL,
	[contactno1] [int] NULL,
	[contactno2] [int] NULL,
	[contactno3] [int] NULL,
	[contactno4] [int] NULL,
	[contactno5] [int] NULL,
	[contact1_type] [int] NULL,
	[contact2_type] [int] NULL,
	[contact3_type] [int] NULL,
	[contact4_type] [int] NULL,
	[contact5_type] [int] NULL,
	[organisation] [varchar](50) NULL,
	[jobtitle] [varchar](20) NULL,
	[Notes] [text] NULL,
	[status] [bit] NULL,
	[addedby] [int] NULL,
	[addedon] [date] NULL,
	[updatedby] [int] NULL,
	[updatedon] [date] NULL,
 CONSTRAINT [PK_contactmaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[contactno]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contactno](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[contactno] [varchar](15) NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_contactno] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[email]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[email](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](50) NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_email] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[type]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [varchar](15) NULL,
 CONSTRAINT [PK_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[user]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[user](
	[id] [int] NOT NULL,
	[name] [varchar](50) NULL,
	[contactno] [nvarchar](20) NULL,
	[email] [varchar](20) NULL,
	[password] [nvarchar](50) NULL,
	[status] [bit] NULL,
	[addedby] [int] NULL,
	[addedon] [date] NULL,
	[updatedby] [int] NULL,
	[updatedon] [date] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[website]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[website](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[website] [varchar](100) NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_website] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[address] ON 

INSERT [dbo].[address] ([id], [street], [Pobox], [Neiborhood], [city], [state], [zipcode], [country], [IsDelete]) VALUES (1, N'Mangalore', N'575020', N'Ullal', N'Mangalore', N'Karnataka', 5890, N'India', 0)
INSERT [dbo].[address] ([id], [street], [Pobox], [Neiborhood], [city], [state], [zipcode], [country], [IsDelete]) VALUES (2, N'Manjeshwar', N'89752', N'Hosangadi', N'Manjcity', N'Karanataka', 5894720, N'India', 0)
INSERT [dbo].[address] ([id], [street], [Pobox], [Neiborhood], [city], [state], [zipcode], [country], [IsDelete]) VALUES (3, N'kothrude', N'589010', N'Saudamini', N'Mangalore', N'Karnataka', 78952, N'India', 0)
INSERT [dbo].[address] ([id], [street], [Pobox], [Neiborhood], [city], [state], [zipcode], [country], [IsDelete]) VALUES (4, N'Mangalore', N'575020', N'Ullal', N'Mangalore', N'Karnataka', 5890, N'India', 0)
INSERT [dbo].[address] ([id], [street], [Pobox], [Neiborhood], [city], [state], [zipcode], [country], [IsDelete]) VALUES (5, N'Manjeshwar', N'89752', N'Hosangadi', N'Manjcity', N'Karanataka', 5894720, N'India', 0)
SET IDENTITY_INSERT [dbo].[address] OFF
SET IDENTITY_INSERT [dbo].[contactmaster] ON 

INSERT [dbo].[contactmaster] ([id], [firstname], [middlename], [lastname], [email1], [email2], [email3], [email1_type], [email2_type], [email3_type], [add1], [add2], [add3], [add1_type], [add2_type], [add3_type], [website1], [website2], [website3], [contactno1], [contactno2], [contactno3], [contactno4], [contactno5], [contact1_type], [contact2_type], [contact3_type], [contact4_type], [contact5_type], [organisation], [jobtitle], [Notes], [status], [addedby], [addedon], [updatedby], [updatedon]) VALUES (1, N'Namratha', N'U', N'P', 57, 58, 59, 3, 1, 2, 3, 0, 0, 3, 1, 2, 0, N'1', N'2', 5, 6, 7, 8, 9, 1, 2, 3, 4, 5, N'YUJ IT Solutions', N'Software Engineer', NULL, 0, 0, CAST(N'2017-05-29' AS Date), 0, NULL)
SET IDENTITY_INSERT [dbo].[contactmaster] OFF
SET IDENTITY_INSERT [dbo].[contactno] ON 

INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (1, N'9481976767', 0)
INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (2, N'08242465111', 0)
INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (3, N'9964667060', 0)
INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (4, N'9902509884', 0)
INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (5, N'953564405', 0)
INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (6, N'9481976767', 0)
INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (7, N'08242465111', 0)
INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (8, N'9964667060', 0)
INSERT [dbo].[contactno] ([id], [contactno], [IsDelete]) VALUES (9, N'9902509884', 0)
SET IDENTITY_INSERT [dbo].[contactno] OFF
SET IDENTITY_INSERT [dbo].[email] ON 

INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (1, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (2, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (3, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (4, N'nam.com', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (5, N'google.com', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (6, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (7, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (8, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (9, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (10, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (11, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (12, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (13, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (14, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (15, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (16, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (17, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (18, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (19, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (20, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (21, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (22, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (23, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (24, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (25, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (26, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (27, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (28, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (29, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (30, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (31, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (32, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (33, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (34, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (35, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (36, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (37, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (38, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (39, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (40, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (41, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (42, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (43, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (44, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (45, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (46, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (47, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (48, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (49, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (50, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (51, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (52, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (53, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (54, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (55, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (56, N'namratha.up@ini', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (57, N'namratha.up@its', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (58, N'namratha6767@gm', 0)
INSERT [dbo].[email] ([id], [email], [IsDelete]) VALUES (59, N'namratha.up@ini', 0)
SET IDENTITY_INSERT [dbo].[email] OFF
SET IDENTITY_INSERT [dbo].[type] ON 

INSERT [dbo].[type] ([id], [type]) VALUES (1, N'Home')
INSERT [dbo].[type] ([id], [type]) VALUES (2, N'Work')
INSERT [dbo].[type] ([id], [type]) VALUES (3, N'Other')
INSERT [dbo].[type] ([id], [type]) VALUES (4, N'Main')
INSERT [dbo].[type] ([id], [type]) VALUES (5, N'custom')
SET IDENTITY_INSERT [dbo].[type] OFF
SET IDENTITY_INSERT [dbo].[website] ON 

INSERT [dbo].[website] ([id], [website], [IsDelete]) VALUES (1, N'nam.com', 0)
INSERT [dbo].[website] ([id], [website], [IsDelete]) VALUES (2, N'google.com', 0)
SET IDENTITY_INSERT [dbo].[website] OFF
/****** Object:  StoredProcedure [dbo].[spAddressSave]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spAddressSave]    
	@street	varchar(50),
    @Pobox	varchar(50),
    @Neiborhood	varchar(50),
    @city	varchar(50),
    @state	varchar(50),
    @zipcode	varchar(50),
    @country	varchar(50) ,  
	@AddressID int output ,
	@ID as int 
AS    
  
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int     
 if(@ID=0)
 BEGIN
 INSERT INTO [dbo].[address]    
           ([street]    
           ,[Pobox]  
           ,[Neiborhood]    
			,[city]    
           ,[state]    
			,[zipcode],
			[country],
			[IsDelete] )    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@street    
           ,@Pobox  
           ,@Neiborhood    
			,@city    
           ,@state    
			,@zipcode,
			@country ,
			'0'   
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @AddressID = @insertedKey  
 END
 ELSE
BEGIN 
 UPDATE  [dbo].[address] 
          set 
          street=isnull(@street,street)
           , Pobox=isnull(@Pobox,street) 
           , Neiborhood=isnull(@Neiborhood,street)
			, city=isnull(@city,street)  
           , state=isnull(@state,street) 
			, zipcode=isnull(@zipcode,street)
			, country=isnull(@country,street)
			where id=@ID
			 Set @AddressID = @ID  
  END
  END

GO
/****** Object:  StoredProcedure [dbo].[spContactNOSave]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spContactNOSave]    
	@contactno	varchar(15),
	@ContactID int output ,
	@ID as int   
AS    
   
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int  
   if(@ID=0)
   BEGIN   
 INSERT INTO [dbo].[contactno]    
          ([contactno],[IsDelete])    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@contactno ,'0'   
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @ContactID = @insertedKey  
  END
  ELSE
  BEGIN
  Update [contactno] set [contactno]=isnull(@contactno,contactno) where id=@ID 
   Set @ContactID = @ID  
  END
  END

GO
/****** Object:  StoredProcedure [dbo].[spEmailSave]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spEmailSave]    
	@email	varchar(15),
	@emailID int output ,
	@ID as int    
AS    
   
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int     
  if(@ID=0)
  BEGIN
 INSERT INTO [dbo].email    
          (email,[IsDelete])    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@email ,'0'   
             
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @emailID = @insertedKey  
 END
   ELSE
  BEGIN
  Update email set email=isnull(@email,email) where id=@ID 
   Set @emailID = @ID  
  END
  END

GO
/****** Object:  StoredProcedure [dbo].[spSaveContactMaster]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spSaveContactMaster]
@firstname	as varchar(20) ,
@middlename	as varchar(20), 
@lastname	as varchar(20) ,
@email1	as int, 
@email2	as int, 
@email3	as int, 
@email1_type as int, 
@email2_type as int, 
@email3_type as int, 
@add1 as int, 
@add2 as int, 
@add3 as int, 
@add1_type as int, 
@add2_type as int, 
@add3_type as int, 
@website1	as int, 
@website2 as int, 
@website3 as int, 
@contactno1	 as int, 
@contactno2	 as int, 
@contactno3	as int, 
@contactno4	as int, 
@contactno5	as int, 
@contact1_type as int, 
@contact2_type as int, 
@contact3_type as int, 
@contact4_type as int, 
@contact5_type as int, 
@organisation  as varchar(50),
@jobtitle	as varchar(20),
@Notes	as text,
--status
@addedby as int, 
--@addedon		as date,
@updatedby as int, 
--@updatedon		as date,
@ContactID int output ,
@ID as int as
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int     
 IF(@ID=0)
 BEGIN
 INSERT INTO [dbo].contactmaster    
           (
			firstname,	
			middlename,	
			lastname,	
			email1	,
			email2	,
			email3	,
			email1_type,
			email2_type,
			email3_type,
			add1,
			add2,
			add3,
			add1_type,
			add2_type,
			add3_type,
			website1,	
			website2,
			website3,
			contactno1,	
			contactno2,	
			contactno3,	
			contactno4,	
			contactno5,	
			contact1_type,
			contact2_type,
			contact3_type,
			contact4_type,
			contact5_type,
			organisation,
			jobtitle	,
			Notes	,
			status,
			addedby,
			addedon	,	
			updatedby,
			updatedon	
			)    
			output inserted.id into @NewIdentityValues     
		 VALUES    
          (
		@firstname,	
		@middlename,	
		@lastname,	
		@email1	,
		@email2	,
		@email3	,
		@email1_type,
		@email2_type,
		@email3_type,
		@add1,
		@add2,
		@add3,
		@add1_type,
		@add2_type,
		@add3_type,
		@website1,	
		@website2,
		@website3,
		@contactno1,	
		@contactno2,	
		@contactno3,	
		@contactno4,	
		@contactno5,	
		@contact1_type,
		@contact2_type,
		@contact3_type,
		@contact4_type,
		@contact5_type,
		@organisation,
		@jobtitle	,
		@Notes	,
		'0',
		@addedby,
		getdate()	,	
		@updatedby,
		null	
	) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @ContactID = @insertedKey  
 END
 ELSE
BEGIN 
 UPDATE  [dbo].contactmaster 
          set 
		    firstname=isnull(@firstname,firstname)	,
			middlename=isnull(@middlename,middlename	),
			lastname=isnull(@lastname,lastname	),
			email1=isnull(@email1	,email1),
			email2=isnull(@email2	,email2),
			email3=isnull(@email3	,email3),
			email1_type=isnull(@email1_type,email1_type),
			email2_type=isnull(@email2_type,email2_type),
			email3_type=isnull(@email3_type,email3_type),
			add1=isnull(@add1,add1),
			add2=isnull(@add2,add2),
			add3=isnull(@add3,add3),
			add1_type=isnull(@add1_type,add1_type),
			add2_type=isnull(@add2_type,add2_type),
			add3_type=isnull(@add3_type,add3_type),
			website1=isnull(@website1,website1)	,
			website2=isnull(@website2,website2),
			website3=isnull(@website3,website3),
			contactno1=isnull(@contactno1,	contactno1),
			contactno2=isnull(@contactno2,	contactno2),
			contactno3=isnull(@contactno3,	contactno3),
			contactno4=isnull(@contactno4,	contactno4),
			contactno5=isnull(@contactno5,contactno5),	
			contact1_type=isnull(@contact1_type,contact1_type),
			contact2_type=isnull(@contact2_type,contact2_type),
			contact3_type=isnull(@contact3_type,contact3_type),
			contact4_type=isnull(@contact4_type,contact4_type),
			contact5_type=isnull(@contact5_type,organisation),
			organisation=isnull(@organisation,organisation),
			jobtitle=isnull(@jobtitle	,jobtitle),
			Notes=isnull(@Notes	,Notes),
			updatedby=isnull(@updatedby,updatedby),
			updatedon=getdate()		
			where id=@ID
			 Set @ContactID = @ID  
  END
  END

GO
/****** Object:  StoredProcedure [dbo].[spWebsiteSave]    Script Date: 5/29/2017 4:16:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spWebsiteSave]    
	@website	varchar(15),
	@websiteID int output    ,
	@ID as int   
AS    
   
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int 
   if(@ID=0)
  BEGIN    
 INSERT INTO [dbo].website    
          (website,[IsDelete])    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@website ,'0'   
             
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @websiteID = @insertedKey  
   END
   ELSE
  BEGIN
  Update website set website=isnull(@website,website) where id=@ID 
   Set @websiteID = @ID  
  END
  END

GO
USE [master]
GO
ALTER DATABASE [Saint-Gobin] SET  READ_WRITE 
GO
