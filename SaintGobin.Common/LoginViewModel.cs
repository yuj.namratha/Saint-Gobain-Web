﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "User name is required")]
        [Display(Name = "User name")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
    }
    public class DeviceDetails
    {
        //Here you can use name also
        [Required(AllowEmptyStrings = false)]
        public string user_id { get; set; }
        public string gsm_ids { get; set; }
    }
    public class TestIOS
    {
        //Here you can use name also
        
        public string test { get; set; }
      
    }
}
