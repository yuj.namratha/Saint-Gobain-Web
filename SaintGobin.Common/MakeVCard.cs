﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace SaintGobin.Common
{
    public class MakeVCard : IHttpHandler
    {
        const string firstName = "Wile";
        const string lastName = "Coyote";
        const string orgName = "Acme Jet Packs";
        const string jobTitle = "Test Dummy";
        const string workPhone = "(123) 456-7890";
        const string cellPhone = "(098) 765-4321";
        const string workEmail = "wile.e.coyote@acmeinc.com";
        const string homePhone = "(234) 567-8901";
        const string homeAddr1 = "123 Main St";
        const string homeAddr2 = "APT 1";
        const string homeCity = "Anytown";
        const string homeState = "US";
        const string homeZip = "12345";
        const string homeCountry = "United States";
        const string workAddr1 = "456 Broad St";
        const string workAddr2 = "Suite B";
        const string workCity = "Big City";
        const string workState = "US";
        const string workZip = "67890";
        const string workCountry = "United States";
        const string fileName = "D:\\Files\\me1.vcf";

        public void ProcessRequest(HttpContext context)
        {
            string fileName = String.Format("{0}_{1}.vcf", firstName, lastName);

            context.Response.ContentType = "text/x-vcard";
            context.Response.AddHeader("content-disposition", String.Format("inline; filename={0}", fileName));

            StringBuilder vcard = new StringBuilder();

            vcard.AppendLine("BEGIN:VCARD");
            vcard.AppendLine("VERSION:3.0");
            vcard.AppendFormat("N:{0};{1}\r\n", lastName, firstName);
            vcard.AppendFormat("FN:{0} {1}\r\n", firstName, lastName);
            vcard.AppendFormat("ORG:{0}\r\n", orgName);
            vcard.AppendFormat("TITLE:{0}\r\n", jobTitle);
            vcard.AppendFormat("TEL;WORK;VOICE:{0}\r\n", workPhone);
            vcard.AppendFormat("TEL;CELL:{0}\r\n", cellPhone);
            vcard.AppendFormat("EMAIL;WORK:{0}\r\n", workEmail);
            vcard.AppendFormat("TEL;HOME;VOICE:{0}\r\n", homePhone);

            vcard.AppendFormat("ADR;HOME;ENCODING=QUOTED-PRINTABLE:;;{0}=0D=0A{1};{2};{3};{4};{5}\r\n",
                homeAddr1, homeAddr2, homeCity, homeState, homeZip, homeCountry);
            vcard.AppendFormat("ADR;HOME;ENCODING=QUOTED-PRINTABLE:;;{0}=0D=0A{1};{2};{3};{4};{5}\r\n",
                workAddr1, workAddr2, workCity, workState, workZip, workCountry);

            vcard.AppendLine("END:VCARD");

            context.Response.Write(vcard.ToString());
            context.Response.End();
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}