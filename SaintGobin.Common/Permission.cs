﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common
{
    public class Role
    {

        public int role_id { get; set; }

        [Required(ErrorMessage = "Role name is required.")]
        public string role_name { get; set; }

        public string description { get; set; }

        public DateTime? start_date { get; set; }

        public DateTime? end_date { get; set; }

        public bool allTime { get; set; }
        public List<RoleDetails> role_details { get; set; }
        public Role()
        {
            role_details = new List<RoleDetails>();
        }
    }

    public class RoleDetails
    {

        public int role_detailsID { get; set; }

        public int role_id { get; set; }

        public int permission_id { get; set; }

        public bool isSelected { get; set; }
    }

    public class Permission
    {

        public int permission_id { get; set; }

        public string permission_name { get; set; }
    }

    public class RoleView
    {

        public int role_id { get; set; }

        public string role_name { get; set; }

        public string permission { get; set; }
    }
}
