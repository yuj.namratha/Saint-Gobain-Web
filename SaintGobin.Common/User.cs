﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common
{
    public class User
    {

        public bool IsAdmin { get; set; }
        public int User_id { get; set; }
        public string SGID { get; set; }
        public string zone { get; set; }
        public string firstname { get; set; }
        public string middlename { get; set; }
        public string lastname { get; set; }
        public string reporting_person { get; set; }
        public string reporting_person_id { get; set; }
        public string date_of_joining { get; set; }
        public string date_of_relieving { get; set; }
        public string mobile_no { get; set; }
        public string Employee_Name { get; set; }
        public bool  IsExist { get; set; }
        public int OTP { get; set; }
    }

    public class SGIDList
    {

        public string sgid { get; set; }     

        
       
    }

    public class sms
    {

        public string sgid { get; set; }     
       // public string SendSMSResult { get; set; }
    }
    public class Example
    {
        public sms AddressValidationResponse { get; set; }
    }

    public class OTP
    {

        public string otp { get; set; }
        public string SGID { get; set; }
        public string createdon { get; set; }

        public string mobileno { get; set; }


    }
}
