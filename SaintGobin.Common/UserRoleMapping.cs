﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common
{
    public class UserRoleMapping
    {
        public UserRoleMapping()
        {
            permissions = new List<Permission>(0);
        }
        public int user_role_id { get; set; }
        public int user_id { get; set; }
        public int role_id { get; set; }
        public bool isSelected { get; set; }
        public List<Permission> permissions { get; set; }

    }

}
