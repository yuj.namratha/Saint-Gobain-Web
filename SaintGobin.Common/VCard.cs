﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SaintGobin.Common
{
   
    public class VCard
    {
        public string firstName { get; set; }
        public string lastName { get; set; }
        public string orgName { get; set; }
        public string jobTitle { get; set; }
        public string workPhone { get; set; }
        public string cellPhone { get; set; }
        public string workEmail { get; set; }
        public string homePhone { get; set; }
        public string homeAddr1 { get; set; }
        public string homeAddr2 { get; set; }
        public string homeCity { get; set; }
        public string homeState { get; set; }
        public string homeZip { get; set; }
        public string homeCountry { get; set; }
        public string workAddr1 { get; set; }
        public string workAddr2 { get; set; }
        public string workCity { get; set; }
        public string workState { get; set; }
        public string workZip { get; set; }
        public string workCountry { get; set; }
        public string homeEmail { get; set; }
        public string email { get; set; }
        //public string email { get; set; }
        public string PREF { get; set; }

        public override string ToString()
        {
            StringBuilder vcard = new StringBuilder();

            vcard.AppendLine("BEGIN:VCARD");
            vcard.AppendLine("VERSION:3.0");
            vcard.AppendFormat("N:{0};{1}\r\n", lastName, firstName);
            //vcard.AppendFormat("FN:{0} {1}\r\n", firstName, lastName);
            vcard.AppendFormat("ORG:{0}\r\n", orgName);
            vcard.AppendFormat("TITLE:{0}\r\n", jobTitle);

            vcard.AppendFormat("TEL;WORK;VOICE:{0}\r\n", workPhone);
            vcard.AppendFormat("TEL;HOME;VOICE:{0}\r\n", homePhone);
            vcard.AppendFormat("TEL;CELL:{0}\r\n", cellPhone);
            vcard.AppendFormat("TEL;PREF:{0}\r\n", PREF);

            vcard.AppendFormat("EMAIL;WORK:{0}\r\n", workEmail);
            vcard.AppendFormat("EMAIL;HOME:{0}\r\n", homeEmail);
            vcard.AppendFormat("EMAIL:{0}\r\n", homeEmail);

            vcard.AppendFormat("ADR;WORK;ENCODING=QUOTED-PRINTABLE:;;{0}=0D=0A{1};{2};{3};{4};{5}\r\n",
              workAddr1, workAddr2, workCity, workState, workZip, workCountry);
            vcard.AppendFormat("ADR;HOME;ENCODING=QUOTED-PRINTABLE:;;{0}=0D=0A{1};{2};{3};{4};{5}\r\n",
                homeAddr1, homeAddr2, homeCity, homeState, homeZip, homeCountry);
          
            vcard.AppendLine("END:VCARD");

            return vcard.ToString();
        }
    }
}