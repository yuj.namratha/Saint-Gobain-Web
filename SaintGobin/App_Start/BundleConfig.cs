﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace SaintGobin.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/Scripts").Include("~/Scripts/Lib/bootstrap.min.js",
            "~/Scripts/Lib/jquery.validate.unobtrusive.min.js", "~/Scripts/Lib/jquery.dataTables.min.js", "~/Scripts/Lib/jquery-1.12.3.min.js",
            "~/Scripts/Lib/jquery-ui.js", "~/Scripts/Lib/jquery.validate.min.js", "~/Scripts/Lib/custom.js"));
            bundles.Add(
                new StyleBundle("~/Content").Include("~/Content/bootstrap.min.css", "~/Content/style.css",
                "~/Content/bootstrap.min.css", "~/Content/buttons.dataTables.min.css",
                "~/Content/fakeLoader.css", "~/Content/font-awesome.min.css",
               "~/Content/jquery-ui.css", "~/Content/jquery.dataTables.min.css"));

            BundleTable.EnableOptimizations = true;
        }
    }
}