﻿using SaintGobin.Common;
using SaintGobin.Common.BL;
using SaintGobin.Common.DAO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.UI;

namespace SaintGobin.Controllers
{
    public class AccountController : Controller
    {

        CommonDAO dao = new CommonDAO(); 
        

        // GET: Account
        [HttpGet]
        [AllowAnonymous]
      
        public ActionResult Login()
        {
            return View("Login");
        }


        [HttpPost]
        [AllowAnonymous]
    
        public ActionResult Login(LoginViewModel model)
        {
            bool loginsuccess = false;
            try
            {
                LoginBL lbl = new LoginBL();
                if (!this.ModelState.IsValid)
                {
                    return this.View(model);
                }
                if (model.UserName.ToUpper() == "SUPERADMIN")
                {
                   if (lbl.ValidateUser(model.Password))
                    {
                        APPSession.Current.IsAdmin = true;
                        loginsuccess = true;
                        getProfile(model.UserName);
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    //if (lbl.ValidateUser(model.UserName, model.Password))
                    //{
                    if ((model.UserName == model.Password))
                    {
                        loginsuccess = true;
                        getProfile(model.UserName);
                        return RedirectToAction("Index", "Home");
                    }
                }
                if (!loginsuccess)
                {
                    dao.WriteToFile("Not Validated");
                    ModelState.Clear();
                    model.UserName = string.Empty;
                    model.Password = string.Empty;
                    ViewData["Error"] = "Invalid username or password.";
                }
            }
            catch (Exception ex)
            {
                dao.WriteToFile(ex.Message + "Exception while fetching");


                if (loginsuccess)
                {
                    //this.ModelState.AddModelError(string.Empty, "Error occured while getting user profile.");
                    ViewData["Error"] = "Error occured while getting user profile.";
                }
                else
                {
                    //this.ModelState.AddModelError(string.Empty, "Unable to login please check domain connection.");
                    ViewData["Error"] = "Unable to login please check domain connection.";
                }
            }
            return this.View(model);
        }

        private void getProfile(string userName)
        {
            #region Get Profile
            bool isAdmin = false;
            bool Is_Func_Supervisor = false;
            bool Is_Imm_Supervisor = false;

            try
            {
                if (userName.ToUpper() == "SUPERADMIN")
                {
                    // this.WriteToFile("Super Admin Profile stored in session");
                    APPSession.Current.User_id= 0 ;
                    APPSession.Current.SGID = "0";
                    APPSession.Current.zone = "NA";
                    APPSession.Current.firstname = "0";
                    APPSession.Current.middlename = "0";
                    APPSession.Current.lastname = "0";
                    APPSession.Current.reporting_person = "0";
                    APPSession.Current.reporting_person_id = "0";
                    APPSession.Current.date_of_joining = "0";
                    APPSession.Current.date_of_relieving = "0";
                    APPSession.Current.mobile_no = "0";
                    APPSession.Current.IsAdmin = true;
                    APPSession.Current.Employee_Name ="SUPERADMIN";
                }
                else
                {
                    UserBL ubl = new UserBL();
                    User currentuser = ubl.getProfile(userName);
                    APPSession.Current.User_id = currentuser.User_id;
                    APPSession.Current.SGID = currentuser.SGID;
                    APPSession.Current.zone = currentuser.zone;
                    APPSession.Current.firstname = currentuser.firstname;
                    APPSession.Current.middlename = currentuser.middlename;
                    APPSession.Current.lastname = currentuser.lastname;
                    APPSession.Current.reporting_person = currentuser.reporting_person;
                    APPSession.Current.reporting_person_id = currentuser.reporting_person_id;
                    APPSession.Current.date_of_joining = currentuser.date_of_joining;
                    APPSession.Current.date_of_relieving = currentuser.date_of_relieving;
                    APPSession.Current.mobile_no = currentuser.mobile_no;
                    APPSession.Current.Employee_Name = currentuser.firstname.ToString().PadRight(10) + currentuser.middlename.ToString().PadRight(10) + currentuser.lastname.ToString();

                    LoginBL bl = new LoginBL();
                
                    RoleBL user = new RoleBL();
                    var lstUsreRoles = user.GetUserRoles(Convert.ToInt32(APPSession.Current.User_id));
                    var roles = lstUsreRoles.permissions.Select(r => r.permission_name).ToArray();
                    
                    foreach (var innerRole in roles)
                    {
                        if (string.Equals(innerRole, "Admin", StringComparison.OrdinalIgnoreCase))
                        {
                            isAdmin = true;
                            APPSession.Current.IsAdmin = true;
                            break;
                        }
                    }

                    
                }
            #endregion

                FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(APPSession.Current.User_id, APPSession.Current.Employee_Name, DateTime.Now, DateTime.Now.AddMinutes(300), false, APPSession.Current.User_id.ToString());
                try
                {
                    if (ticket.Name != null && ticket.UserData != null && ticket.IssueDate != null && APPSession.Current.Employee_Name != null)
                    {
                        string data = FormsAuthentication.Encrypt(ticket);
                        HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, data);
                        cookie.Path = "/";
                        Response.Cookies.Add(cookie);
                    }
                    else
                    {
                        throw new System.ArgumentException("Parameter cannot be null", "original");
                    }
                }
                catch (Exception ex)
                {
                    dao.WriteToFile(ex.Message + "-ticket is null");
                    ViewData["Error"] = "Error occured while getting user profile..";
                    throw;
                }
            }
            catch (Exception ex)
            {
                dao.WriteToFile(ex.Message + "-Get Profile Error");
                throw;
            }
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            
            
            
            Response.Cookies["adAuthCookie"].Expires = DateTime.Now.AddDays(-1);
            FormsAuthentication.RedirectToLoginPage();
            System.Web.HttpContext.Current.ApplicationInstance.CompleteRequest();
            System.Web.HttpContext.Current.Session.Clear();
            System.Web.HttpContext.Current.Session.Abandon();
            System.Web.HttpContext.Current.Response.Cookies.Add(new HttpCookie("ASP.NET_SessionId", ""));

            return this.RedirectToAction("Login", "Account");
        }





        //public void WriteToFile(string text)
        //{
        //    var fileName ="Log_"+ DateTime.Now.ToString("dd/MM/yyyy_hh_mm_ss") +".txt";
        //    var path = Path.Combine(Server.MapPath("~/ServerLog"), fileName);
            
        //    using (StreamWriter writer = new StreamWriter(path, true))
        //    {
        //        writer.WriteLine(string.Format(text, DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt")));
        //        writer.Close();
        //    }
        //}

        public ActionResult getUserInfo()
        {
            string userName = "";
            userName = APPSession.Current.Employee_Name;
            return Content(userName);
        }
    
    }
}