﻿using SaintGobin.Common;
using SaintGobin.Common.BL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using Thought.vCards;
using PhoneNumbers;
using SaintGobin.Core;
using System.Linq.Dynamic;
using System.Web.UI.WebControls;
using System.Web.UI;
using SaintGobin.Common.DAO;   
namespace SaintGobin.Controllers
{
 [Authorize]
    public class ContactsController : Controller
    {
        ContactsBL contactsBL = new ContactsBL();
        List<Contact> contact_export = new List<Contact>();

        static  List<CallLogHistory> callLogHistory_Export = new List<CallLogHistory>();

        #region Add Contact
        [HttpGet]
        public ActionResult AddContact()
        {
            Contact contact = new Contact();
            return View(contact);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult AddContact(Contact contact)
        {
            try
            {

                #region Contact Number
                List<Contactnos> contactno = new List<Contactnos>();
                contactno.Add(new Contactnos() { type_name = contact.contactno1_type,  number = contact.contactno1 });
                contactno.Add( new Contactnos()  {type_name = contact.contactno2_type, number = contact.contactno2} );
                contactno.Add( new Contactnos(){  type_name = contact.contactno3_type,   number = contact.contactno3  } );
                contactno.Add( new Contactnos() {type_name = contact.contactno4_type,number = contact.contactno4});
                contactno.Add(new Contactnos(){ type_name = contact.contactno5_type, number = contact.contactno5 } );
                contact.Contactnoslist=contactno;
                #endregion

                #region Email
                List<Emails> emailList = new List<Emails>();
                emailList.Add(new Emails() { type_name = contact.email1_type, email = contact.email1 });
                emailList.Add(new Emails() { type_name = contact.email2_type, email = contact.email2});
                emailList.Add(new Emails() { type_name = contact.email3_type, email = contact.email3 });
                contact.Emailslist=emailList;
                #endregion

                #region URl
                List<Websites> uRL = new List<Websites>();
                Websites websites = new Websites();
                uRL.Add(new Websites() { URLs = contact.website1 });
                uRL.Add(new Websites() { URLs = contact.website2 });
                uRL.Add(new Websites() { URLs = contact.website3 });
                contact.websiteslist=uRL;   
                #endregion

                #region Address
                List<Adress> address = new List<Adress>();
                Adress add = new Adress();
                if (contact.street1 != null || contact.street2 != null  || contact.city != null || contact.state != null || contact.zipcode != null || contact.country != null)
                {
                    address.Add(new Adress()
                    {
                        street1 = contact.street1,
                        street2 = contact.street2,
                        city = contact.city,
                        state = contact.state,
                        zipcode = contact.zipcode,
                        country = contact.country,
                        type_name = contact.address1_type
                  });
                }
                if (contact.home_street1 != null || contact.home_street2 != null || contact.home_city != null || contact.home_state != null || contact.home_zipcode != null || contact.home_country != null)
                {
                    address.Add(new Adress()
                    {
                        street1 = contact.home_street1,
                        street2 = contact.home_street2,
                        city = contact.home_city,
                        state = contact.home_state,
                        zipcode = contact.home_zipcode,
                        country = contact.home_country,
                        type_name = contact.address2_type
                    });
                }

                if (contact.work_street1 != null || contact.work_street2 != null || contact.work_city != null || contact.work_state != null || contact.work_zipcode != null || contact.work_country != null)
                {
                    address.Add(new Adress()
                    {
                        street1 = contact.work_street1,
                        street2 = contact.work_street2,
                        city = contact.work_city,
                        state = contact.work_state,
                        zipcode = contact.work_zipcode,
                        country = contact.work_country,
                        type_name = contact.address3_type
                    });
                }
                contact.Adresslist = address;

                #endregion
                contact.GUID=  Guid.NewGuid().ToString();
                contact.User_id =APPSession.Current.User_id.ToString() ;
               contact.SGID = APPSession.Current.SGID;
               contactsBL.AddContacts(contact);
          
                ModelState.Clear();
                ViewData["Success"] = "Contact Added successfully.";
            }
            catch (Exception)
            {
                ViewData["Error"] = "Error occured while Adding Contact.";
            }

            return RedirectToAction("GetConactInformation", "Contacts");

            //return View(contact);
        }
       

        #endregion

        #region View Contact

        public ActionResult GetConactInformation()
        {
            List<Contact> contact = contactsBL.GetContactInformation();
           contact= contact.OrderBy(M => M.IsDeleteRequest).ToList() ;  
            return View(contact);
        }

        [HttpPost]
        public ActionResult GetConactInformations()
        {
            List<Contact> contact = contactsBL.GetContactInformation();
            contact = contact.OrderByDescending (m => m.IsDeleteRequest==true).ToList();
            string draw = Request.Form.GetValues("draw").FirstOrDefault();
            string start = Request.Form.GetValues("start").FirstOrDefault();
            string length = Request.Form.GetValues("length").FirstOrDefault();
            string sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0].[column]").FirstOrDefault() + ")[name]").FirstOrDefault();
            string sortColumnDir = Request.Form.GetValues("order[0].[dir]").FirstOrDefault();
            string contactname = Request.Form.GetValues("columns[0].[search][value]").FirstOrDefault();
            string country = Request.Form.GetValues("columns[0].[search][value]").FirstOrDefault();
            int pagesize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt32(start) : 0;
            int recordsTotal = 0;
            if (!string.IsNullOrEmpty(contactname))
            {
                contact = contact.Where(a => a.firstname == contactname).ToList();
            }
            if (!string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir))
            {
                contact = contact.OrderBy(sortColumn + " " + sortColumnDir).ToList()  ;
            }
            recordsTotal =contact.Count ;
            var data = contact.Skip(skip).Take(pagesize).ToList();
            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal,data=data },JsonRequestBehavior.AllowGet  );
          
        }


        [HttpPost]
        public ActionResult LoadData()
        {
            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault()
                                    + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            //find search columns info
            //var bg_id = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();
            //var vertical_id = Request.Form.GetValues("columns[5][search][value]").FirstOrDefault();
            //var zone_id = Request.Form.GetValues("columns[6][search][value]").FirstOrDefault();
            //var city_id = Request.Form.GetValues("columns[7][search][value]").FirstOrDefault();
            //var loc_id = Request.Form.GetValues("columns[8][search][value]").FirstOrDefault();
            //var quiz_id = Request.Form.GetValues("columns[10][search][value]").FirstOrDefault();
            //var weigtage = Request.Form.GetValues("columns[1][search][value]").FirstOrDefault();

            //filter parameter
            var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();


            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;

           // List<Questionreport> dc = rBL.Questionreport().OrderBy(x => x.quiz_id).ToList();
            List<Contact> dc = contactsBL.GetContactInformation().ToList();
            //SEARCHING...
            //search
            dc = (from a in dc select a).ToList();
            //search
            //if (!string.IsNullOrEmpty(searchValue))
            //{
            //    dc = dc.Where(a =>
            //        a.emp_name.Contains(searchValue) ||
            //        a.quiz_name.Contains(searchValue) ||
            //        a.Question.Contains(searchValue) ||
            //        a.emp_name.Contains(searchValue)
            //        ).ToList();
            //}

            //if (weigtage != null && weigtage != "null" && weigtage != "")
            //{
            //    dc = dc.Where(a => a.Weigthage.Contains(weigtage)).ToList();
            //}
            //if (bg_id != "0" && bg_id != "" && bg_id != null)
            //{
            //    dc = dc.Where(a => a.bg_id.Contains(bg_id)).ToList();
            //}
            //if (vertical_id != "0" && vertical_id != null && vertical_id != "null" && vertical_id != "")
            //{
            //    dc = dc.Where(a => a.vertical_id == vertical_id).ToList();
            //}
            //if (zone_id != "0" && zone_id != "" && zone_id != null)
            //{

            //    dc = dc.Where(a => a.zone.Contains(zone_id)).ToList();
            //}
            //if (city_id != "0" && city_id != null && city_id != "null" && city_id != "")
            //{
            //    dc = dc.Where(a => a.city == city_id).ToList();
            //}
            //if (loc_id != "0" && loc_id != null && loc_id != "null" && loc_id != "")
            //{
            //    dc = dc.Where(a => a.location.Contains(loc_id)).ToList();
            //}
            //if (quiz_id != "0" && quiz_id != null && quiz_id != "null" && quiz_id != "")
            //{
            //    dc = dc.Where(a => a.quiz_id == quiz_id).ToList();
            //}

            //SORTING...  (For sorting we need to add a reference System.Linq.Dynamic)
            if (!(string.IsNullOrEmpty(sortColumn)) && sortColumn!=""&& !(string.IsNullOrEmpty(sortColumnDir)))
            {
                dc = dc.OrderBy(sortColumn + " " + sortColumnDir).ToList();
            }
            contact_export = dc;
            recordsTotal = dc.Count();
            var data = dc.Skip(skip).Take(pageSize).ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                JsonRequestBehavior.AllowGet);
        }




        public ActionResult ExportData()
        {
            GridView gv = new GridView();
            if (contact_export.Count != 0)
            {
                gv.DataSource = contact_export;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=QuestionReport.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
            return RedirectToAction("StudentDetails");
        }

        #endregion

        #region UserList
        [HttpGet]
       [CustomAuthorization("Admin,SUPERADMIN,Add Contact")]
        public ActionResult User()
        {
            List<User> user = new  List<User>();
            UserBL userBL = new UserBL();
            user = userBL.GetUsers(); 
            return View(user);
        }

        public ActionResult GetConactInformationonser_id()
        {
            List<Contact> contact = contactsBL.GetContactInformation();
            return View(contact);
        }

        public ActionResult GetContactOnUser(int user_id)
        
        {
            List<Contact> contact = contactsBL.GetContactInformations(user_id);
            return View(contact);
        }

        #endregion

        #region EDIT Contact
        [HttpGet]
        //[CustomAuthorization("Admin,SUPERADMIN,Add Contact")]
        public ActionResult EditContact(string id)
        {
            Contact contact = new Contact();
            contact = contactsBL.GetContactInformationOnID(id)[0];
            contact.id = Convert.ToInt32(id);  
            return View(contact);
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        //[CustomAuthorization("Admin,SUPERADMIN,Add Contact")]
        //[ValidateInput(false)]
        public ActionResult EditContact(Contact contact)
        {
            CommonDAO commonDAO = new CommonDAO();
            commonDAO.WriteToFile("SyncDateAPI HIT---- " + DateTime.Now ); 
            try
            {
                contact.GUID = Guid.NewGuid().ToString();
                bool valid=contactsBL.EditContact(contact);
                ModelState.Clear();
                if (valid)
                {
                    ViewData["Success"] = "Contact Updated  successfully.";
                }
                else
                {
                    ViewData["Error"] = "Error occured while updating Contact.";
                }
            }
            catch (Exception)
            {
                ViewData["Error"] = "Error occured while Updating Contact.";
            }
            List<Contact> contacts = contactsBL.GetContactInformation();
            return RedirectToAction("GetConactInformation", "Contacts");
        }


        #endregion

        #region Delete Contact
        [HttpGet]
        public ActionResult DeleteContact(int id)
        {
            try
            {
                bool valid = contactsBL.DeleteContact(id);
                ModelState.Clear();
                if (valid)
                {
                    ViewData["Success"] = "Contact Updated  successfully.";
                }
                else
                {
                    ViewData["Error"] = "Error occured while updating Contact.";
                }
            }
            catch (Exception)
            {
                ViewData["Error"] = "Error occured while Updating Contact.";
            }
            return RedirectToAction("GetConactInformation", "Contacts");
        }


        #endregion

        #region DeleteContact Deny
        [HttpGet]
        public ActionResult DeleteContactDeny(string id)
        {
            try
            {
                bool valid = contactsBL.DeleteRequestDeny(id);
                ModelState.Clear();
                if (valid)
                {
                    ViewData["Success"] = "Contact Updated  successfully.";
                }
                else
                {
                    ViewData["Error"] = "Error occured while updating Contact.";
                }
            }
            catch (Exception)
            {
                ViewData["Error"] = "Error occured while Updating Contact.";
            }
            return RedirectToAction("GetConactInformation", "Contacts");
        }


        #endregion

        #region DeleteRequest


        public ActionResult DeleteRequest()
        {
            List<Contact> contact = contactsBL.GetDeleteformation();
            return View(contact);
        }
        #endregion

        public ActionResult ContactTab()
        {
          
            return View();
        }


        #region merge
        public ActionResult Merge(string vcflist )
        {
            List<Contact> contact = new List<Contact>(); 
            if (!string.IsNullOrEmpty(vcflist) && vcflist!="")
            {
                List<string> list = vcflist.Split(',').ToList();
                if (list.Count > 1)
                {
                    List<VCF> vcf = new List<VCF>();
                    list.ForEach(l => vcf.Add(new VCF(l)));
                    int valid = contactsBL.Merger(vcf);
                    if (valid != 0)
                    {
                        contact = contactsBL.GetContactInformationOnID(valid.ToString());  
                    }
                }
            }
           return View("MergedContact",contact); 
        }
        #endregion

        #region Delete

        public ActionResult Delete(string vcflist)
        {
            List<Contact> contact = new List<Contact>();
            if (!string.IsNullOrEmpty(vcflist) && vcflist != "")
            {
                List<string> list = vcflist.Split(',').ToList();
                if (list.Count > 0)
                {
                    List<VCF> vcf = new List<VCF>();
                    list.ForEach(l => vcf.Add(new VCF(l)));
                    bool  valid = contactsBL.Delete(vcf);
                }
            }
            return RedirectToAction("GetConactInformation", "Contacts");
        }




        #endregion


        #region CalllogHistory


        public ActionResult CalllogHistory(string user_id)
        {
            APPSession.Current.TemporaryVariable = user_id;   
            return View(); 
        }

        [HttpPost]
        public ActionResult LoadDatas()
        {
            //jQuery DataTables Param
            var draw = Request.Form.GetValues("draw").FirstOrDefault();
            //Find paging info
            var start = Request.Form.GetValues("start").FirstOrDefault();
            var length = Request.Form.GetValues("length").FirstOrDefault();
            //Find order columns info
            var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault()
                                    + "][name]").FirstOrDefault();
            var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
            //find search columns info
            var daterange = Request.Form.GetValues("columns[3][search][value]").FirstOrDefault();

            //find search columns info
            var zone = Request.Form.GetValues("columns[4][search][value]").FirstOrDefault();

            string [] daterangearray = daterange.Split('-'); 


            //filter parameter
            var searchValue = Request.Form.GetValues("search[value]").FirstOrDefault();

            int pageSize = length != null ? Convert.ToInt32(length) : 0;
            int skip = start != null ? Convert.ToInt16(start) : 0;
            int recordsTotal = 0;

            List<CallLogHistory> dc = contactsBL.GetCallLog(APPSession.Current.TemporaryVariable).OrderBy(x => x.user_id).ToList();

            //SEARCHING...
            //search
            dc = (from a in dc select a).ToList();
            //search
            if (!string.IsNullOrEmpty(searchValue))
            {
                dc = dc.Where(a =>
                    a.empname.Contains(searchValue) ||
                    a.duration.Contains(searchValue) ||
                    a.calldate.ToShortDateString().Contains(searchValue) ||
                    a.mobno.Contains(searchValue)
                    ).ToList();
            }


            if (daterange != "0" && daterange != null && daterange != "null" && daterange != "")
            {
            DateTime Startdate=Convert.ToDateTime (daterangearray[0]);
            DateTime Enddate = Convert.ToDateTime(daterangearray[1]);
            dc = dc.Where(x => x.calldate.Date >= Startdate.Date).ToList(); 
            dc= dc.Where(x => x.calldate.Date <= Enddate.Date).ToList(); 
            }
          

            //SORTING...  (For sorting we need to add a reference System.Linq.Dynamic)
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                dc = dc.OrderBy(sortColumn + " " + sortColumnDir).ToList();
            }
            callLogHistory_Export = dc;
            recordsTotal = dc.Count();
            var data = dc.Skip(skip).Take(pageSize).ToList();

            return Json(new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data },
                JsonRequestBehavior.AllowGet);


        }


        public ActionResult ExportDatas()
        {
            GridView gv = new GridView();
            if (callLogHistory_Export.Count != 0)
            {
                var newList = callLogHistory_Export.Select(l => new
                {
                    l.empname,
                    l.mobno,
                    l.calldate,
                    l.duration,
                    l.zone,
                    l.SGID
                }).ToList();

                gv.DataSource = newList;
                gv.DataBind();
                Response.ClearContent();
                Response.Buffer = true;
                Response.AddHeader("content-disposition", "attachment; filename=CalllogHistory.xls");
                Response.ContentType = "application/ms-excel";
                Response.Charset = "";
                StringWriter sw = new StringWriter();
                HtmlTextWriter htw = new HtmlTextWriter(sw);
                gv.RenderControl(htw);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();

            }

            return RedirectToAction("CalllogHistory");
        }

        #endregion

        public ActionResult OTP()
        {
            List<OTP> otp = contactsBL.GETOTP();
            return View(otp); 
        }
        public ActionResult truncate()
        {
         bool otp = contactsBL.truncate();
            return View();
        }    

    }
}


    
   