﻿using SaintGobin.Common;
using SaintGobin.Common.BL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace SaintGobin.Core
{
    public class CustomAuthorization : AuthorizeAttribute
    {
        readonly string[] _roles = null;

        public CustomAuthorization(string role)
        {
            _roles = role.Split(',');
        }

        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //if allow anonymous attribute is added on controller or action method then do nothing
            if (filterContext.ActionDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true) ||
                filterContext.ActionDescriptor.ControllerDescriptor.IsDefined(typeof(AllowAnonymousAttribute), true))
                return;

            GenericPrincipal principal = (GenericPrincipal)filterContext.RequestContext.HttpContext.User;
            var identity = principal.Identity as FormsIdentity;

            // var user_id = identity.Ticket.UserData;
            var user_id = APPSession.Current.User_id.ToString();
            bool isinrole = false;

            if (user_id == "0")
            {
                if (identity.Ticket.Name == "SUPERADMIN")
                {
                    return;
                }
            }
            else
            {
                //if (identity.Ticket.Name == "Supervisor")
                //{


                //    List<string> roles1 = new List<string>();
                //    roles1.Add("View Quiz");
                //    roles1.Add("View Question");
                //    roles1.Add("Add Question");
                //    roles1.Add("Create Quiz");
                //    roles1.Add("Assign Quiz");

                //    foreach (var outerRole in roles1)
                //    {
                //        foreach (var innerRole in _roles)
                //        {
                //            if (string.Equals(innerRole, outerRole, StringComparison.OrdinalIgnoreCase))
                //            {
                //                isinrole = true;
                //                break;
                //            }
                //        }

                //        if (isinrole)
                //            break;
                //    }
                //}

                RoleBL user = new RoleBL();
                var lstUsreRoles = user.GetUserRoles(Convert.ToInt32(user_id));
                var roles = lstUsreRoles.permissions.Select(r => r.permission_name).ToArray();
                foreach (var outerRole in roles)
                {
                    foreach (var innerRole in _roles)
                    {
                        if (string.Equals(innerRole, outerRole, StringComparison.OrdinalIgnoreCase))
                        {
                            isinrole = true;
                            break;
                        }
                    }

                    if (isinrole)
                        break;
                }
                if (isinrole)
                {
                    return;
                }
                else
                {
                    filterContext.Result = new RedirectResult("~/Unauthorized.html");
                    filterContext.RequestContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
                }
            }

        }
    }
}