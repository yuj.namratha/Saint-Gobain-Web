﻿using SaintGobin.App_Start;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using System.Web.SessionState;

namespace SaintGobin
{
    public class MvcApplication : System.Web.HttpApplication
    {

        internal static readonly object m_ajaxRequest = new object();
        internal static readonly object m_cookieExpired = new object();

        public bool m_tAjaxRequest = false;
        private bool m_tCookieExpired = false;

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            // Autofac and Automapper configurations
            //Bootstrapper.Run();
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Response.Cache.SetExpires(DateTime.UtcNow.AddMinutes(-1));
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetNoStore();

            if (Request.Headers["X-Requested-With"] == "XMLHttpRequest")
            {
                m_tAjaxRequest = true;
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                Response.Cache.SetExpires(DateTime.Now.AddHours(-1));
                Response.Cache.SetNoStore();
                HttpContext.Current.Items[m_ajaxRequest] = m_ajaxRequest;
            }
        }

        protected void Application_PreRequestHandlerExecute(object sender, EventArgs e)
        {
            if (Context.Handler is IRequiresSessionState || Context.Handler is IReadOnlySessionState)
            {
                HttpCookie authenticationCookie = Request.Cookies[FormsAuthentication.FormsCookieName];
                if (authenticationCookie != null)
                {
                    FormsAuthenticationTicket authenticationTicket = FormsAuthentication.Decrypt(authenticationCookie.Value);
                    if (!authenticationTicket.Expired)
                    {
                        if (Session["LOGIN"] == null)
                        {
                            FormsAuthentication.SignOut();
                            if (m_tAjaxRequest)
                            {
                                Context.Items[m_cookieExpired] = m_cookieExpired;
                                HttpContext.Current.Response.SuppressFormsAuthenticationRedirect = true;
                            }
                            else
                            {
                                Context.Response.Redirect("~/sessionexpired.html", true);
                            }
                        }
                    }
                }
            }
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
            if (HttpContext.Current.User != null)
            {
                if (HttpContext.Current.User.Identity.IsAuthenticated)
                {
                    if (HttpContext.Current.User.Identity is FormsIdentity)
                    {
                        FormsIdentity formsId = (FormsIdentity)HttpContext.Current.User.Identity;
                        FormsAuthenticationTicket ticket = formsId.Ticket;
                        if (ticket.Expired == false)
                        {
                            string strUserData = ticket.UserData;
                            //string[] aStrRoles = strUserData.Split(',');
                            HttpContext.Current.User = new GenericPrincipal(formsId, new string[] { strUserData });
                        }
                    }
                }
            }
        }

        protected void Application_EndRequest(object sender, EventArgs e)
        {
            if (m_tAjaxRequest && Context.Items.Contains(m_tCookieExpired))
            {
                Context.Response.Clear();
                Context.Response.StatusDescription = "Your session has been expired";
                Context.Response.StatusCode = 401;
            }
        }
    }
}
