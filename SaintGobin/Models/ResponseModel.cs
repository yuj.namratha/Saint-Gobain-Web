﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SaintGobin.Models
{
     public enum EnumResult
    {
        Succes,
        Failure,
        Exception,
    }

    public class ResponseModel
    {
        public EnumResult Result { get; private set; }
        public object Response { get; set; }
        public int StatusCode { get; set; }

        public ResponseModel(EnumResult result)
        {
            Result = result;
        }
    }

    public class OkResponseModel : ResponseModel
    {
        public OkResponseModel()
            : this(null)
        {
        }

        public OkResponseModel(object model)
            : this(EnumResult.Succes, model)
        {
        }

        public OkResponseModel(EnumResult result, object model)
            : base(result)
        {
            StatusCode = 200;
            Response = model;
        }
    }
}