USE [master]
GO
/****** Object:  Database [TEST]    Script Date: 3/20/2018 12:25:05 PM ******/
CREATE DATABASE [TEST]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'TEST', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TEST.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'TEST_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQLEXPRESS\MSSQL\DATA\TEST_log.ldf' , SIZE = 816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [TEST] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [TEST].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [TEST] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [TEST] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [TEST] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [TEST] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [TEST] SET ARITHABORT OFF 
GO
ALTER DATABASE [TEST] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [TEST] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [TEST] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [TEST] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [TEST] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [TEST] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [TEST] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [TEST] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [TEST] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [TEST] SET  DISABLE_BROKER 
GO
ALTER DATABASE [TEST] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [TEST] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [TEST] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [TEST] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [TEST] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [TEST] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [TEST] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [TEST] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [TEST] SET  MULTI_USER 
GO
ALTER DATABASE [TEST] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [TEST] SET DB_CHAINING OFF 
GO
ALTER DATABASE [TEST] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [TEST] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [TEST] SET DELAYED_DURABILITY = DISABLED 
GO
USE [TEST]
GO
/****** Object:  UserDefinedTableType [dbo].[SGLIST]    Script Date: 3/20/2018 12:25:05 PM ******/
CREATE TYPE [dbo].[SGLIST] AS TABLE(
	[SGID] [varchar](max) NULL
)
GO
/****** Object:  UserDefinedTableType [dbo].[SGLISTs]    Script Date: 3/20/2018 12:25:05 PM ******/
CREATE TYPE [dbo].[SGLISTs] AS TABLE(
	[SGID] [int] NULL
)
GO
/****** Object:  Table [dbo].[address]    Script Date: 3/20/2018 12:25:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[address](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[street1] [varchar](max) NULL,
	[street2] [varchar](max) NULL,
	[city] [varchar](50) NULL,
	[state] [varchar](50) NULL,
	[zipcode] [int] NULL,
	[country] [varchar](50) NULL,
	[GUID] [int] NULL,
	[addedby] [int] NULL,
	[addedon] [datetime] NULL,
	[updatedby] [int] NULL,
	[updatedon] [datetime] NULL,
	[isdelete] [bit] NULL,
	[type] [int] NULL,
 CONSTRAINT [PK_address] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[callog]    Script Date: 3/20/2018 12:25:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[callog](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[empname] [varchar](max) NULL,
	[mobno] [varchar](50) NULL,
	[duration] [varchar](50) NULL,
	[calldate] [datetime] NULL,
	[user_id] [int] NULL,
	[zone] [varchar](10) NULL,
	[addedon] [datetime] NULL,
 CONSTRAINT [PK_callog] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactMaster]    Script Date: 3/20/2018 12:25:05 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[GUID] [int] NULL,
	[addedby] [int] NULL,
	[addedon] [datetime] NULL,
	[updatedby] [int] NULL,
	[updatedon] [datetime] NULL,
	[isdelete] [bit] NULL,
	[first_name] [varchar](50) NULL,
	[middle_name] [varchar](50) NULL,
	[last_name] [varchar](50) NULL,
	[prefix] [varchar](50) NULL,
	[sufix] [varchar](50) NULL,
	[organisation] [varchar](250) NULL,
	[title] [varchar](250) NULL,
	[SGID] [varchar](200) NULL,
	[IsDeleteRequest] [bit] NULL,
 CONSTRAINT [PK_ContactMaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[contactno]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[contactno](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[contactno] [varchar](50) NULL,
	[isdelete] [bit] NULL,
	[addedby] [int] NULL,
	[addedon] [datetime] NULL,
	[updatedby] [int] NULL,
	[updatedon] [datetime] NULL,
	[GUID] [int] NULL,
	[type] [int] NULL,
 CONSTRAINT [PK_contactno] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CustomerMaster]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CustomerMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SG_id] [nvarchar](max) NOT NULL,
	[SG_customer_id] [nvarchar](max) NULL,
	[name] [varchar](max) NULL,
	[address] [text] NULL,
	[zone] [varchar](50) NULL,
 CONSTRAINT [PK_CustomerMaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[email]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[email](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[email] [varchar](max) NULL,
	[isdelete] [bit] NULL,
	[addedby] [int] NULL,
	[addedon] [datetime] NULL,
	[updatedby] [int] NULL,
	[updatedon] [datetime] NULL,
	[GUID] [int] NULL,
	[type] [int] NULL,
 CONSTRAINT [PK_email] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[GUIDMaster]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GUIDMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[GU_id] [nvarchar](max) NULL,
	[zone] [varchar](200) NULL,
	[SG_id] [nvarchar](max) NULL,
	[isdelete] [bit] NULL,
	[addedby] [int] NULL,
	[addedon] [datetime] NULL,
	[updatedby] [int] NULL,
	[updatedon] [datetime] NULL,
	[IsUpdated] [bit] NULL,
	[DRequestedBy] [varchar](50) NULL,
 CONSTRAINT [PK_VCFMaster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SMS]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SMS](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[sgid] [varchar](max) NULL,
	[mobileno] [varchar](max) NULL,
	[message] [varchar](max) NULL,
	[IsSent] [bit] NULL,
	[reqdatetime] [datetime] NULL,
	[processdatetime] [datetime] NULL,
	[returnMsg] [varchar](max) NULL,
 CONSTRAINT [PK_SMS] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[syncdetails]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[syncdetails](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[syncdate] [datetime] NULL,
	[zone] [varchar](50) NULL,
 CONSTRAINT [PK_syncdetails] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbldevice]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbldevice](
	[SL_NO] [int] NOT NULL,
	[sgid] [int] NULL,
	[gsm_id] [nvarchar](max) NULL,
	[flag] [bit] NULL,
 CONSTRAINT [PK_tbldevice] PRIMARY KEY CLUSTERED 
(
	[SL_NO] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblpermission]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblpermission](
	[permission_id] [int] IDENTITY(1,1) NOT NULL,
	[permission] [varchar](20) NOT NULL,
 CONSTRAINT [PK_tblpermission] PRIMARY KEY CLUSTERED 
(
	[permission_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblroledetails]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblroledetails](
	[role_detailid] [int] IDENTITY(1,1) NOT NULL,
	[role_id] [int] NULL,
	[permission_id] [int] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_tblroledetails] PRIMARY KEY CLUSTERED 
(
	[role_detailid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblrolemaster]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblrolemaster](
	[role_id] [int] IDENTITY(1,1) NOT NULL,
	[role_name] [varchar](50) NULL,
	[description] [varchar](100) NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_tblrolemaster] PRIMARY KEY CLUSTERED 
(
	[role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tblsuperAdmin]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tblsuperAdmin](
	[User_ID] [int] NOT NULL,
	[UserName] [varchar](20) NULL,
	[Password] [varchar](50) NULL,
 CONSTRAINT [PK_tblsuperAdmin] PRIMARY KEY CLUSTERED 
(
	[User_ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbluserrolemaster]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbluserrolemaster](
	[user_role_id] [int] IDENTITY(1,1) NOT NULL,
	[user_id] [int] NULL,
	[role_id] [int] NULL,
	[assignmentDate] [date] NULL,
	[assignedBy] [int] NULL,
	[from_date] [date] NULL,
	[to_date] [date] NULL,
	[all_time] [bit] NULL,
	[IsDelete] [bit] NULL,
 CONSTRAINT [PK_tbluserrolemaster] PRIMARY KEY CLUSTERED 
(
	[user_role_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[type]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[type](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [varchar](15) NULL,
 CONSTRAINT [PK_type] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UserMaster]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UserMaster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[SG_id] [nvarchar](max) NULL,
	[mobile_no] [varchar](50) NULL,
	[reporting_person] [varchar](200) NULL,
	[reporting_person_id] [nvarchar](200) NULL,
	[firstname] [varchar](200) NULL,
	[lastname] [varchar](200) NULL,
	[middlename] [varchar](200) NULL,
	[zone] [nvarchar](200) NULL,
	[date_of_joining] [date] NULL,
	[date_of_relieving] [date] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_usermater] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[website]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[website](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[website] [varchar](100) NULL,
	[isdelete] [bit] NULL,
	[addedby] [int] NULL,
	[addedon] [datetime] NULL,
	[updatedby] [int] NULL,
	[updatedon] [datetime] NULL,
	[GUID] [int] NULL,
 CONSTRAINT [PK_website] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[website_vcf]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[website_vcf](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[website_id] [int] NULL,
	[GUID] [int] NULL,
	[isdelete] [bit] NULL,
	[addedby] [int] NULL,
	[addedon] [datetime] NULL,
	[updatedby] [int] NULL,
	[updatedon] [datetime] NULL,
 CONSTRAINT [PK_website_vcf] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[address] ON 

INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (1, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 1, 0, CAST(N'2018-01-31 14:04:15.260' AS DateTime), 0, CAST(N'2018-01-31 14:08:24.947' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (2, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 2, 0, CAST(N'2018-01-31 14:05:36.210' AS DateTime), 0, CAST(N'2018-01-31 14:08:25.010' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (3, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 3, 0, CAST(N'2018-01-31 14:08:24.867' AS DateTime), 0, CAST(N'2018-01-31 15:02:28.673' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (4, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 4, 0, CAST(N'2018-01-31 14:21:02.717' AS DateTime), 0, CAST(N'2018-01-31 14:21:04.113' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (5, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 5, 0, CAST(N'2018-01-31 14:21:04.017' AS DateTime), 0, CAST(N'2018-01-31 14:25:56.903' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (6, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 6, 0, CAST(N'2018-01-31 14:22:20.093' AS DateTime), 0, CAST(N'2018-01-31 14:57:02.083' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (7, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 7, 0, CAST(N'2018-01-31 14:25:56.820' AS DateTime), 0, CAST(N'2018-01-31 14:57:02.143' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (8, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 8, 0, CAST(N'2018-01-31 14:57:02.010' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.483' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (9, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 9, 0, CAST(N'2018-01-31 15:00:00.867' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.543' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (10, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 10, 0, CAST(N'2018-01-31 15:01:29.410' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.613' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (11, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 11, 0, CAST(N'2018-01-31 15:02:28.593' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.760' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (12, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 12, 0, CAST(N'2018-01-31 15:02:53.413' AS DateTime), 0, CAST(N'2018-02-12 12:06:21.560' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (13, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 13, 0, CAST(N'2018-01-31 15:24:43.043' AS DateTime), 0, CAST(N'2018-02-12 12:06:21.633' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (14, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 14, 0, CAST(N'2018-01-31 15:25:41.363' AS DateTime), 0, CAST(N'2018-01-31 15:27:48.153' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (15, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 15, 0, CAST(N'2018-01-31 15:27:48.073' AS DateTime), 0, CAST(N'2018-01-31 15:28:18.537' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (16, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 16, 0, CAST(N'2018-01-31 15:28:18.453' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (17, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 17, 0, CAST(N'2018-01-31 17:08:30.740' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (18, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 18, 0, CAST(N'2018-01-31 17:09:57.187' AS DateTime), NULL, NULL, 0, 2)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (19, N'Ullal', N'Mastikatte', N'Mangalore', N'Karnataka', 551520, N'India', 19, 0, CAST(N'2018-01-31 17:12:53.513' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (20, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 20, 0, CAST(N'2018-01-31 17:20:05.317' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (21, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 21, 0, CAST(N'2018-01-31 17:21:15.183' AS DateTime), 0, CAST(N'2018-01-31 17:27:32.203' AS DateTime), 1, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (22, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 22, 0, CAST(N'2018-01-31 17:23:47.363' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (23, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 23, 0, CAST(N'2018-01-31 17:24:50.303' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (24, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 24, 0, CAST(N'2018-01-31 17:26:18.003' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (25, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 25, 0, CAST(N'2018-01-31 17:27:32.057' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (26, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 26, 0, CAST(N'2018-01-31 17:30:37.813' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (27, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 27, 0, CAST(N'2018-01-31 17:50:21.660' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (28, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 28, 0, CAST(N'2018-01-31 17:51:43.100' AS DateTime), NULL, NULL, 0, 1)
INSERT [dbo].[address] ([id], [street1], [street2], [city], [state], [zipcode], [country], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [type]) VALUES (29, N'Ullal', N'Mastilatte', N'Mangalore', N'Karnataka', 551520, N'India', 29, 0, CAST(N'2018-02-12 12:06:21.443' AS DateTime), NULL, NULL, 0, 1)
SET IDENTITY_INSERT [dbo].[address] OFF
SET IDENTITY_INSERT [dbo].[ContactMaster] ON 

INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (1, 1, 0, CAST(N'2018-01-31 14:04:12.690' AS DateTime), 0, CAST(N'2018-01-31 14:08:24.930' AS DateTime), 1, N'Namratha', N'U', N'P', N'MR', N'Ullal', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (2, 2, 0, CAST(N'2018-01-31 14:05:34.237' AS DateTime), 0, CAST(N'2018-01-31 14:08:24.993' AS DateTime), 1, N'Sneha', N'Kanniguli', N'K', N'MS', N'T', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (3, 3, 0, CAST(N'2018-01-31 14:08:18.147' AS DateTime), 0, CAST(N'2018-01-31 15:02:28.660' AS DateTime), 1, N'Sneha', N'Kanniguli', N'K', N'MR', N'Ullal', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (4, 4, 0, CAST(N'2018-01-31 14:21:02.427' AS DateTime), 0, CAST(N'2018-01-31 14:21:04.097' AS DateTime), 1, N'Smallf', N'MD', N'MD', N'MR', N'S', N'MR', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (5, 5, 0, CAST(N'2018-01-31 14:21:03.720' AS DateTime), 0, CAST(N'2018-01-31 14:25:56.887' AS DateTime), 1, N'Smallf', N'MD', N'MD', N'MR', N'S', N'MR', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (6, 6, 0, CAST(N'2018-01-31 14:22:17.940' AS DateTime), 0, CAST(N'2018-01-31 14:57:02.060' AS DateTime), 1, N'Employee', N'MD', N'MD', N'MRS', N'S', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (7, 7, 0, CAST(N'2018-01-31 14:25:54.717' AS DateTime), 0, CAST(N'2018-01-31 14:57:02.130' AS DateTime), 1, N'Rashmi', N'MD', N'MD', N'MR', N'S', N'MR', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (8, 8, 0, CAST(N'2018-01-31 14:53:33.763' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.467' AS DateTime), 1, N'Rashmi', N'MD', N'MD', N'MRS', N'S', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (9, 9, 0, CAST(N'2018-01-31 14:59:58.413' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.530' AS DateTime), 1, N'Namratha', N'MD', N'MD', N'MR', N'S', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (10, 10, 0, CAST(N'2018-01-31 15:00:55.567' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.593' AS DateTime), 1, N'SuperAdmin', N'MD', N'MD', N'MR', N'X', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (11, 11, 0, CAST(N'2018-01-31 15:02:17.663' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.713' AS DateTime), 1, N'SuperAdmin', N'MD', N'MD', N'MR', N'P', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (12, 12, 0, CAST(N'2018-01-31 15:02:46.383' AS DateTime), 0, CAST(N'2018-02-12 12:06:21.530' AS DateTime), 1, N'SuperAdmin', N'MD', N'MD', N'MRS', N'S', N'YUJ', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (13, 13, 0, CAST(N'2018-01-31 15:24:42.550' AS DateTime), 0, CAST(N'2018-02-12 12:06:21.610' AS DateTime), 1, N'Namratha', N'MD', N'MD', N'MR', N'S', N'yuj', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (14, 14, 0, CAST(N'2018-01-31 15:25:40.880' AS DateTime), 0, CAST(N'2018-01-31 15:27:48.140' AS DateTime), 1, N'Junior', N'MD', N'MD', N'MR', NULL, NULL, NULL, N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (15, 15, 0, CAST(N'2018-01-31 15:27:47.437' AS DateTime), 0, CAST(N'2018-01-31 15:28:18.520' AS DateTime), 1, N'Namratha', NULL, N'MD', N'MR', NULL, NULL, NULL, N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (16, 16, 0, CAST(N'2018-01-31 15:28:17.473' AS DateTime), NULL, NULL, 0, N'Namratha', NULL, N'MD', N'MR', NULL, NULL, NULL, N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (17, 17, 0, CAST(N'2018-01-31 17:08:29.687' AS DateTime), NULL, NULL, 0, N'Rashmi', N'U', N'P', N'MR', N'Ullal', N'yuj', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (18, 18, 0, CAST(N'2018-01-31 17:09:56.360' AS DateTime), NULL, NULL, 0, N'Ashish', N'B', N'L', N'MR', N'sr', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (19, 19, 0, CAST(N'2018-01-31 17:12:53.017' AS DateTime), NULL, NULL, 0, N'Priya', N'Darshan', N'Manjeshwar', N'MR', N'M', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (20, 20, 0, CAST(N'2018-01-31 17:20:04.733' AS DateTime), NULL, NULL, 0, N'Laxmikanth', N'K', N'L', N'MR', N'K', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (21, 21, 0, CAST(N'2018-01-31 17:21:14.653' AS DateTime), 0, CAST(N'2018-01-31 17:27:32.130' AS DateTime), 1, N'Junior', N'U', N'MD', N'MR', N'X', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (22, 22, 0, CAST(N'2018-01-31 17:23:46.827' AS DateTime), NULL, NULL, 0, N'Rashmi', N'U', N'MD', N'MS', N'S', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (23, 23, 0, CAST(N'2018-01-31 17:24:49.663' AS DateTime), NULL, NULL, 0, N'Employee', N'U', N'MD', N'MR', N'S', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (24, 24, 0, CAST(N'2018-01-31 17:26:17.503' AS DateTime), NULL, NULL, 0, N'Rasika', N'L', N'Badhe', N'MS', N'M', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (25, 25, 0, CAST(N'2018-01-31 17:27:31.397' AS DateTime), NULL, NULL, 0, N'Shut', N'Down', N'K', N'MS', N'L', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (26, 26, 0, CAST(N'2018-01-31 17:30:37.157' AS DateTime), NULL, NULL, 0, N'Snehal', N'Badhe', N'M', N'MR', N'D', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (27, 27, 0, CAST(N'2018-01-31 17:50:21.053' AS DateTime), NULL, NULL, 0, N'Namratha', N'U', N'MD', N'MR', N'S', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (28, 28, 0, CAST(N'2018-01-31 17:51:42.627' AS DateTime), NULL, NULL, 0, N'Premnath', N'K', N'D', N'MR', N'K', N'Yuj IT Solutions', N'SE', N'0', 0)
INSERT [dbo].[ContactMaster] ([id], [GUID], [addedby], [addedon], [updatedby], [updatedon], [isdelete], [first_name], [middle_name], [last_name], [prefix], [sufix], [organisation], [title], [SGID], [IsDeleteRequest]) VALUES (29, 29, 0, CAST(N'2018-02-12 12:06:16.140' AS DateTime), NULL, NULL, 0, N'Namratha', N'MD', N'MD', N'MRS', N'S', N'YUJ', N'SE', N'0', 0)
SET IDENTITY_INSERT [dbo].[ContactMaster] OFF
SET IDENTITY_INSERT [dbo].[contactno] ON 

INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (1, N'7798099087', 1, 0, CAST(N'2018-01-31 14:04:12.893' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.573' AS DateTime), 1, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (2, N'99014098845', 1, 0, CAST(N'2018-01-31 14:05:34.437' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.543' AS DateTime), 2, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (3, N'7798099087', 1, 0, CAST(N'2018-01-31 14:08:18.500' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.573' AS DateTime), 3, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (4, N'99014098845', 1, 0, CAST(N'2018-01-31 14:08:18.567' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.543' AS DateTime), 3, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (5, N'99014098848', 1, 0, CAST(N'2018-01-31 14:21:02.610' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.513' AS DateTime), 4, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (6, N'99014098848', 1, 0, CAST(N'2018-01-31 14:21:03.943' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.513' AS DateTime), 5, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (7, N'890140988890', 1, 0, CAST(N'2018-01-31 14:22:18.097' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.447' AS DateTime), 6, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (8, N'90680055556', 1, 0, CAST(N'2018-01-31 14:25:54.940' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.480' AS DateTime), 7, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (9, N'99014098848', 1, 0, CAST(N'2018-01-31 14:25:55.013' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.513' AS DateTime), 7, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (10, N'890140988890', 1, 0, CAST(N'2018-01-31 14:53:34.163' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.447' AS DateTime), 8, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (11, N'90680055556', 1, 0, CAST(N'2018-01-31 14:53:34.250' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.480' AS DateTime), 8, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (12, N'99014098848', 1, 0, CAST(N'2018-01-31 14:53:34.317' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.513' AS DateTime), 8, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (13, N'89014098885', 1, 0, CAST(N'2018-01-31 14:59:58.600' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.690' AS DateTime), 9, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (14, N'+9177980990876', 1, 0, CAST(N'2018-01-31 15:00:56.053' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.607' AS DateTime), 10, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (15, N'99014098845', 1, 0, CAST(N'2018-01-31 15:02:18.310' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.543' AS DateTime), 11, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (16, N'7798099087', 1, 0, CAST(N'2018-01-31 15:02:18.417' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.573' AS DateTime), 11, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (17, N'890140988890', 1, 0, CAST(N'2018-01-31 15:02:47.420' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.447' AS DateTime), 12, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (18, N'90680055556', 1, 0, CAST(N'2018-01-31 15:02:47.500' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.480' AS DateTime), 12, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (19, N'99014098848', 1, 0, CAST(N'2018-01-31 15:02:47.577' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.513' AS DateTime), 12, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (20, N'99014098845', 1, 0, CAST(N'2018-01-31 15:02:47.650' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.543' AS DateTime), 12, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (21, N'7798099087', 1, 0, CAST(N'2018-01-31 15:02:47.713' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.573' AS DateTime), 12, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (22, N'+9177980990876', 1, 0, CAST(N'2018-01-31 15:02:47.783' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.607' AS DateTime), 12, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (23, N'89014098885', 1, 0, CAST(N'2018-01-31 15:02:47.850' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.690' AS DateTime), 12, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (24, N'908901409888', 1, 0, CAST(N'2018-01-31 15:24:42.740' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.723' AS DateTime), 13, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (25, N'8901409888', 1, 0, CAST(N'2018-01-31 15:25:41.040' AS DateTime), 0, CAST(N'2018-01-31 15:28:17.957' AS DateTime), 14, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (26, N'+918901409888', 1, 0, CAST(N'2018-01-31 15:27:47.720' AS DateTime), 0, CAST(N'2018-01-31 15:28:17.937' AS DateTime), 15, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (27, N'8901409888', 1, 0, CAST(N'2018-01-31 15:27:47.790' AS DateTime), 0, CAST(N'2018-01-31 15:28:17.957' AS DateTime), 15, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (28, N'08901409888', 0, 0, CAST(N'2018-01-31 15:28:18.020' AS DateTime), NULL, NULL, 16, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (29, N'+918901409888', 0, 0, CAST(N'2018-01-31 15:28:18.090' AS DateTime), NULL, NULL, 16, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (30, N'8901409888', 0, 0, CAST(N'2018-01-31 15:28:18.163' AS DateTime), NULL, NULL, 16, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (31, N'9902509885', 0, 0, CAST(N'2018-01-31 17:08:29.940' AS DateTime), NULL, NULL, 17, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (32, N'9964667080', 0, 0, CAST(N'2018-01-31 17:09:56.550' AS DateTime), NULL, NULL, 18, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (33, N'7798099085', 0, 0, CAST(N'2018-01-31 17:12:53.220' AS DateTime), NULL, NULL, 19, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (34, N'779809908709', 0, 0, CAST(N'2018-01-31 17:20:04.920' AS DateTime), NULL, NULL, 20, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (35, N'77980990878', 1, 0, CAST(N'2018-01-31 17:21:14.817' AS DateTime), 0, CAST(N'2018-01-31 17:27:31.663' AS DateTime), 21, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (36, N'97798099087', 0, 0, CAST(N'2018-01-31 17:23:47.030' AS DateTime), NULL, NULL, 22, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (37, N'+9177980990873', 0, 0, CAST(N'2018-01-31 17:24:50.003' AS DateTime), NULL, NULL, 23, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (38, N'7754980990878', 0, 0, CAST(N'2018-01-31 17:26:17.687' AS DateTime), NULL, NULL, 24, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (39, N'77980990878', 0, 0, CAST(N'2018-01-31 17:27:31.730' AS DateTime), NULL, NULL, 25, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (40, N'787980990878', 0, 0, CAST(N'2018-01-31 17:30:37.373' AS DateTime), NULL, NULL, 26, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (41, N'779809908785', 0, 0, CAST(N'2018-01-31 17:50:21.240' AS DateTime), NULL, NULL, 27, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (42, N'07798099087', 0, 0, CAST(N'2018-01-31 17:51:42.780' AS DateTime), NULL, NULL, 28, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (43, N'890140988890', 0, 0, CAST(N'2018-02-12 12:06:19.867' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (44, N'90680055556', 0, 0, CAST(N'2018-02-12 12:06:19.940' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (45, N'99014098848', 0, 0, CAST(N'2018-02-12 12:06:20.007' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (46, N'99014098845', 0, 0, CAST(N'2018-02-12 12:06:20.087' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (47, N'7798099087', 0, 0, CAST(N'2018-02-12 12:06:20.153' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (48, N'+9177980990876', 0, 0, CAST(N'2018-02-12 12:06:20.220' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (49, N'89014098885', 0, 0, CAST(N'2018-02-12 12:06:20.290' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[contactno] ([id], [contactno], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (50, N'908901409888', 0, 0, CAST(N'2018-02-12 12:06:20.357' AS DateTime), NULL, NULL, 29, 1)
SET IDENTITY_INSERT [dbo].[contactno] OFF
SET IDENTITY_INSERT [dbo].[email] ON 

INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (1, N'nam@gmail.com', 1, 0, CAST(N'2018-01-31 14:04:12.820' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.703' AS DateTime), 1, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (2, N'nam2@gmail.com', 1, 0, CAST(N'2018-01-31 14:05:34.357' AS DateTime), 0, CAST(N'2018-02-12 12:06:18.390' AS DateTime), 2, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (3, N'nam@gmail.com', 1, 0, CAST(N'2018-01-31 14:08:18.240' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.703' AS DateTime), 3, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (4, N'nam2@gmail.com', 1, 0, CAST(N'2018-01-31 14:08:18.350' AS DateTime), 0, CAST(N'2018-02-12 12:06:18.390' AS DateTime), 3, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (5, N'junior@gmail.com', 1, 0, CAST(N'2018-01-31 14:21:02.513' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.013' AS DateTime), 4, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (6, N'junior@gmail.com', 1, 0, CAST(N'2018-01-31 14:21:03.840' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.013' AS DateTime), 5, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (7, N'smal@gmail.com3', 1, 0, CAST(N'2018-01-31 14:22:18.027' AS DateTime), 0, CAST(N'2018-02-12 12:06:16.680' AS DateTime), 6, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (8, N'junior@gmail.com', 1, 0, CAST(N'2018-01-31 14:25:54.847' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.013' AS DateTime), 7, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (9, N'smal@gmail.com3', 1, 0, CAST(N'2018-01-31 14:53:33.900' AS DateTime), 0, CAST(N'2018-02-12 12:06:16.680' AS DateTime), 8, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (10, N'junior@gmail.com', 1, 0, CAST(N'2018-01-31 14:53:33.993' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.013' AS DateTime), 8, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (11, N'jsunior@gmail.com', 1, 0, CAST(N'2018-01-31 14:59:58.513' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.257' AS DateTime), 9, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (12, N'na66m@gmail.com', 1, 0, CAST(N'2018-01-31 15:00:55.820' AS DateTime), 0, CAST(N'2018-02-12 12:06:18.530' AS DateTime), 10, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (13, N'jun554ior@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:17.860' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.350' AS DateTime), 11, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (14, N'nam@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:18.003' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.703' AS DateTime), 11, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (15, N'nam2@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:18.193' AS DateTime), 0, CAST(N'2018-02-12 12:06:18.390' AS DateTime), 11, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (16, N'smal@gmail.com3', 1, 0, CAST(N'2018-01-31 15:02:46.487' AS DateTime), 0, CAST(N'2018-02-12 12:06:16.680' AS DateTime), 12, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (17, N'junior@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:46.580' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.013' AS DateTime), 12, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (18, N'jun554ior@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:46.680' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.350' AS DateTime), 12, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (19, N'nam@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:46.777' AS DateTime), 0, CAST(N'2018-02-12 12:06:17.703' AS DateTime), 12, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (20, N'nam2@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:46.880' AS DateTime), 0, CAST(N'2018-02-12 12:06:18.390' AS DateTime), 12, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (21, N'na66m@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:46.980' AS DateTime), 0, CAST(N'2018-02-12 12:06:18.530' AS DateTime), 12, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (22, N'jsunior@gmail.com', 1, 0, CAST(N'2018-01-31 15:02:47.073' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.257' AS DateTime), 12, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (23, N'gfrfnam@gmail.com', 1, 0, CAST(N'2018-01-31 15:24:42.670' AS DateTime), 0, CAST(N'2018-02-12 12:06:19.390' AS DateTime), 13, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (24, N'jugggggnior@gmail.comk', 1, 0, CAST(N'2018-01-31 15:25:40.967' AS DateTime), 0, CAST(N'2018-01-31 15:28:17.820' AS DateTime), 14, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (25, N'juniotttr@gmail.com', 1, 0, CAST(N'2018-01-31 15:27:47.523' AS DateTime), 0, CAST(N'2018-01-31 15:28:17.690' AS DateTime), 15, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (26, N'jugggggnior@gmail.comk', 1, 0, CAST(N'2018-01-31 15:27:47.623' AS DateTime), 0, CAST(N'2018-01-31 15:28:17.820' AS DateTime), 15, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (27, N'junior@gmail.com000', 0, 0, CAST(N'2018-01-31 15:28:17.577' AS DateTime), NULL, NULL, 16, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (28, N'juniotttr@gmail.com', 0, 0, CAST(N'2018-01-31 15:28:17.727' AS DateTime), NULL, NULL, 16, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (29, N'jugggggnior@gmail.comk', 0, 0, CAST(N'2018-01-31 15:28:17.860' AS DateTime), NULL, NULL, 16, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (30, N'namx@gmail.com', 0, 0, CAST(N'2018-01-31 17:08:29.823' AS DateTime), NULL, NULL, 17, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (31, N'bubne@gmail.com', 0, 0, CAST(N'2018-01-31 17:09:56.463' AS DateTime), NULL, NULL, 18, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (32, N'namnew@gmail.com', 0, 0, CAST(N'2018-01-31 17:12:53.150' AS DateTime), NULL, NULL, 19, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (33, N'nssam@gmail.com', 0, 0, CAST(N'2018-01-31 17:20:04.853' AS DateTime), NULL, NULL, 20, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (34, N'nam7882@gmail.com', 1, 0, CAST(N'2018-01-31 17:21:14.747' AS DateTime), 0, CAST(N'2018-01-31 17:27:31.587' AS DateTime), 21, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (35, N'najjjm@gmail.com', 0, 0, CAST(N'2018-01-31 17:23:46.963' AS DateTime), NULL, NULL, 22, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (36, N'junilllor@gmail.com', 0, 0, CAST(N'2018-01-31 17:24:49.880' AS DateTime), NULL, NULL, 23, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (37, N'juniornew@gmail.com', 0, 0, CAST(N'2018-01-31 17:26:17.617' AS DateTime), NULL, NULL, 24, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (38, N'nsam@gmail.com', 0, 0, CAST(N'2018-01-31 17:27:31.497' AS DateTime), NULL, NULL, 25, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (39, N'nam7882@gmail.com', 0, 0, CAST(N'2018-01-31 17:27:31.633' AS DateTime), NULL, NULL, 25, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (40, N'sma44l@gmail.com', 0, 0, CAST(N'2018-01-31 17:30:37.303' AS DateTime), NULL, NULL, 26, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (41, N'junior222@gmail.com', 0, 0, CAST(N'2018-01-31 17:50:21.167' AS DateTime), NULL, NULL, 27, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (42, N'namkkk@gmail.com', 0, 0, CAST(N'2018-01-31 17:51:42.713' AS DateTime), NULL, NULL, 28, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (43, N'smal@gmail.com3', 0, 0, CAST(N'2018-02-12 12:06:16.830' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (44, N'junior@gmail.com', 0, 0, CAST(N'2018-02-12 12:06:17.070' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (45, N'jun554ior@gmail.com', 0, 0, CAST(N'2018-02-12 12:06:17.623' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (46, N'nam@gmail.com', 0, 0, CAST(N'2018-02-12 12:06:17.783' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (47, N'nam2@gmail.com', 0, 0, CAST(N'2018-02-12 12:06:18.453' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (48, N'na66m@gmail.com', 0, 0, CAST(N'2018-02-12 12:06:19.140' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (49, N'jsunior@gmail.com', 0, 0, CAST(N'2018-02-12 12:06:19.327' AS DateTime), NULL, NULL, 29, 1)
INSERT [dbo].[email] ([id], [email], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID], [type]) VALUES (50, N'gfrfnam@gmail.com', 0, 0, CAST(N'2018-02-12 12:06:19.430' AS DateTime), NULL, NULL, 29, 1)
SET IDENTITY_INSERT [dbo].[email] OFF
SET IDENTITY_INSERT [dbo].[GUIDMaster] ON 

INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (1, N'7a282a6e-2865-4b6c-8a25-7da075c280a6', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 14:04:12.623' AS DateTime), 0, CAST(N'2018-01-31 14:08:24.910' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (2, N'472d9f52-1cf8-4809-9788-2023de3f9fe8', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 14:05:34.220' AS DateTime), 0, CAST(N'2018-01-31 14:08:24.980' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (3, N'01739c4d-c3b1-4a03-b26e-6db121709303', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 14:08:18.127' AS DateTime), 0, CAST(N'2018-01-31 15:02:28.627' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (4, N'b36256be-aa62-4f8e-9d92-a72796b41958', N'EAST', N'0', 1, 0, CAST(N'2018-01-31 14:21:02.357' AS DateTime), 0, CAST(N'2018-01-31 14:21:04.050' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (5, N'251881f8-6a65-4f1c-adce-80511986bd12', N'EAST', N'0', 1, 0, CAST(N'2018-01-31 14:21:03.697' AS DateTime), 0, CAST(N'2018-01-31 14:25:56.857' AS DateTime), 1, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (6, N'75dd37f5-5354-4254-a728-1a2fb6abe6c7', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 14:22:17.923' AS DateTime), 0, CAST(N'2018-01-31 14:57:02.043' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (7, N'c4013672-874c-4088-a958-33001063026b', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 14:25:54.697' AS DateTime), 0, CAST(N'2018-01-31 14:57:02.113' AS DateTime), 1, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (8, N'4b399e40-c709-46a6-b703-df78c5a7d6c4', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 14:53:33.747' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.450' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (9, N'ad17c301-3b1a-41c2-a03f-21400d792e89', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 14:59:58.393' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.517' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (10, N'23773b0d-673a-40f2-b6b3-f63e382117f0', N'EAST', N'0', 1, 0, CAST(N'2018-01-31 15:00:55.540' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.577' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (11, N'5a228fd7-7f21-4a68-bc5d-c4d3b5bcd6a2', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 15:02:17.610' AS DateTime), 0, CAST(N'2018-01-31 15:02:53.677' AS DateTime), 1, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (12, N'978b9fee-5469-4d59-8914-1fe8ffaf86cb', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 15:02:46.367' AS DateTime), 0, CAST(N'2018-02-12 12:06:21.517' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (13, N'b006c6ff-4402-4ae4-9ebe-10a252398c1e', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 15:24:42.537' AS DateTime), 0, CAST(N'2018-02-12 12:06:21.593' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (14, N'4bad5bbf-60b6-4cfa-a5be-5a60ccac206e', N'EAST', N'0', 1, 0, CAST(N'2018-01-31 15:25:40.867' AS DateTime), 0, CAST(N'2018-01-31 15:27:48.107' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (15, N'fc1c3d5d-d0c4-46ff-97e0-e4e6dad39108', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 15:27:47.417' AS DateTime), 0, CAST(N'2018-01-31 15:28:18.490' AS DateTime), 1, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (16, N'4e4ee468-5d55-4fb7-8445-ab947b32cd8c', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 15:28:17.453' AS DateTime), NULL, NULL, 1, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (17, N'77565497-b91d-4260-b45a-2d8f4fe22e5d', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:08:29.533' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (18, N'a82df347-ac21-4e4e-a687-0c08d1548c86', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:09:56.343' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (19, N'e87a72ee-46ec-4dfd-b8cd-4534105a7534', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:12:52.970' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (20, N'134d6c22-f086-4133-82b4-2411a4d7cd4c', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:20:04.717' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (21, N'a369f4d0-c543-4de8-adc7-5f98312cff64', N'WEST', N'0', 1, 0, CAST(N'2018-01-31 17:21:14.617' AS DateTime), 0, CAST(N'2018-01-31 17:27:32.097' AS DateTime), 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (22, N'23909ce5-c78f-4f3c-8462-a993fcac2173', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:23:46.757' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (23, N'c99e61e4-b3c5-4df7-a68d-85c7f8583248', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:24:49.610' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (24, N'459303c2-e25e-48ac-b283-13b22e8f06b6', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:26:17.487' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (25, N'14b662e4-cd28-4d61-b97f-a73ec72ff14f', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:27:31.377' AS DateTime), NULL, NULL, 1, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (26, N'22557a6b-d83e-49d0-a13b-154429128443', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:30:37.140' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (27, N'e3d81a0f-cc1a-4879-bb0d-afcff31ac5d3', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:50:21.037' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (28, N'd960b313-fbdb-47b2-985c-7fe4e3d53ebf', N'WEST', N'0', 0, 0, CAST(N'2018-01-31 17:51:42.613' AS DateTime), NULL, NULL, 0, N'0')
INSERT [dbo].[GUIDMaster] ([id], [GU_id], [zone], [SG_id], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [IsUpdated], [DRequestedBy]) VALUES (29, N'1bf3d3c5-c17b-47c3-bca5-e680cd3539ca', N'WEST', N'0', 0, 0, CAST(N'2018-02-12 12:06:15.677' AS DateTime), NULL, NULL, 0, N'0')
SET IDENTITY_INSERT [dbo].[GUIDMaster] OFF
SET IDENTITY_INSERT [dbo].[tblpermission] ON 

INSERT [dbo].[tblpermission] ([permission_id], [permission]) VALUES (1, N'Add Contact')
INSERT [dbo].[tblpermission] ([permission_id], [permission]) VALUES (2, N'View Contact')
INSERT [dbo].[tblpermission] ([permission_id], [permission]) VALUES (3, N'Delete Contact')
INSERT [dbo].[tblpermission] ([permission_id], [permission]) VALUES (4, N'Edit Contact')
SET IDENTITY_INSERT [dbo].[tblpermission] OFF
SET IDENTITY_INSERT [dbo].[tblroledetails] ON 

INSERT [dbo].[tblroledetails] ([role_detailid], [role_id], [permission_id], [IsDelete]) VALUES (1, 1, 1, 0)
INSERT [dbo].[tblroledetails] ([role_detailid], [role_id], [permission_id], [IsDelete]) VALUES (2, 2, 2, 0)
INSERT [dbo].[tblroledetails] ([role_detailid], [role_id], [permission_id], [IsDelete]) VALUES (3, 3, 3, 0)
INSERT [dbo].[tblroledetails] ([role_detailid], [role_id], [permission_id], [IsDelete]) VALUES (4, 4, 4, 0)
SET IDENTITY_INSERT [dbo].[tblroledetails] OFF
SET IDENTITY_INSERT [dbo].[tblrolemaster] ON 

INSERT [dbo].[tblrolemaster] ([role_id], [role_name], [description], [IsDelete]) VALUES (1, N'ADD', N'Add Contacts', 0)
INSERT [dbo].[tblrolemaster] ([role_id], [role_name], [description], [IsDelete]) VALUES (2, N'VIEW', N'View Contact', 0)
INSERT [dbo].[tblrolemaster] ([role_id], [role_name], [description], [IsDelete]) VALUES (3, N'DELETE', N'Delete Contact', 0)
INSERT [dbo].[tblrolemaster] ([role_id], [role_name], [description], [IsDelete]) VALUES (4, N'Edit', N'Edit contact', 0)
SET IDENTITY_INSERT [dbo].[tblrolemaster] OFF
INSERT [dbo].[tblsuperAdmin] ([User_ID], [UserName], [Password]) VALUES (1, N'SuperAdmin', N'Password@123')
SET IDENTITY_INSERT [dbo].[tbluserrolemaster] ON 

INSERT [dbo].[tbluserrolemaster] ([user_role_id], [user_id], [role_id], [assignmentDate], [assignedBy], [from_date], [to_date], [all_time], [IsDelete]) VALUES (1, 1, 1, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbluserrolemaster] ([user_role_id], [user_id], [role_id], [assignmentDate], [assignedBy], [from_date], [to_date], [all_time], [IsDelete]) VALUES (2, 2, 2, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbluserrolemaster] ([user_role_id], [user_id], [role_id], [assignmentDate], [assignedBy], [from_date], [to_date], [all_time], [IsDelete]) VALUES (3, 3, 3, NULL, NULL, NULL, NULL, NULL, 0)
INSERT [dbo].[tbluserrolemaster] ([user_role_id], [user_id], [role_id], [assignmentDate], [assignedBy], [from_date], [to_date], [all_time], [IsDelete]) VALUES (4, 4, 4, NULL, NULL, NULL, NULL, NULL, 0)
SET IDENTITY_INSERT [dbo].[tbluserrolemaster] OFF
SET IDENTITY_INSERT [dbo].[type] ON 

INSERT [dbo].[type] ([id], [type]) VALUES (1, N'WORK')
INSERT [dbo].[type] ([id], [type]) VALUES (2, N'HOME')
SET IDENTITY_INSERT [dbo].[type] OFF
SET IDENTITY_INSERT [dbo].[UserMaster] ON 

INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (1, N'1001', N'9902509884', N'Namratha', N'1000', N'Rashmi', N'U', N'P', N'WEST', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (2, N'1000', N'9481976767', N'null', N'null', N'Vinod', N'K', N'S', N'WEST', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (3, N'1002', N'9481976767', N'Namratha', N'1001', N'Suraj', N'S', N'K', N'WEST', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (4, N'1003', N'9964667060', N'Namratha', N'1001', N'Ashish', N'Bubne', N'L', N'NORTH', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (5, N'1004', N'9902509884', N'Namratha', N'1000', N'Amir', N'U', N'P', N'SOUTH', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (6, N'1005', N'9481976767', N'Rashmi', N'1001', N'Vikas', N'H', N'K', N'EAST', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (7, N'1006', N'9481976767', N'Namratha', N'1001', N'Akshay', N'S', N'K', N'WEST', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (8, N'1007', N'9964667060', N'Namratha', N'1001', N'Mithili', N'K', N'L', N'NORTH', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (9, N'1008', N'9067167133', N'Omkar', N'1001', N'Namratha', N'U', N'P', N'NORTH', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
INSERT [dbo].[UserMaster] ([id], [SG_id], [mobile_no], [reporting_person], [reporting_person_id], [firstname], [lastname], [middlename], [zone], [date_of_joining], [date_of_relieving], [IsActive]) VALUES (10, N'N0737155', N'9619488280', N'Omkar', N'1001', N'Neeraj', N'Vasantkumar', N'', N'WEST', CAST(N'2017-10-10' AS Date), CAST(N'2017-10-10' AS Date), 1)
SET IDENTITY_INSERT [dbo].[UserMaster] OFF
SET IDENTITY_INSERT [dbo].[website] ON 

INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (1, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 14:04:15.193' AS DateTime), NULL, NULL, 1)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (2, N'http://www.googles.com', 0, 0, CAST(N'2018-01-31 14:05:36.143' AS DateTime), NULL, NULL, 2)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (3, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 14:08:20.800' AS DateTime), NULL, NULL, 3)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (4, N'http://www.googles.com', 0, 0, CAST(N'2018-01-31 14:08:24.800' AS DateTime), NULL, NULL, 3)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (5, N'http://www.googles.co.in', 0, 0, CAST(N'2018-01-31 14:22:20.000' AS DateTime), NULL, NULL, 6)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (6, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 14:25:56.747' AS DateTime), NULL, NULL, 7)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (7, N'http://www.googles.co.in', 0, 0, CAST(N'2018-01-31 14:57:00.867' AS DateTime), NULL, NULL, 8)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (8, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 14:57:01.940' AS DateTime), NULL, NULL, 8)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (9, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 15:00:00.797' AS DateTime), NULL, NULL, 9)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (10, N'http://www.googles.com', 0, 0, CAST(N'2018-01-31 15:01:29.340' AS DateTime), NULL, NULL, 10)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (11, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 15:02:27.633' AS DateTime), NULL, NULL, 11)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (12, N'http://www.googles.com', 0, 0, CAST(N'2018-01-31 15:02:28.527' AS DateTime), NULL, NULL, 11)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (13, N'http://www.googles.co.in', 0, 0, CAST(N'2018-01-31 15:02:49.983' AS DateTime), NULL, NULL, 12)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (14, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 15:02:53.117' AS DateTime), NULL, NULL, 12)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (15, N'http://www.googles.com', 0, 0, CAST(N'2018-01-31 15:02:53.330' AS DateTime), NULL, NULL, 12)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (16, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 15:24:42.960' AS DateTime), NULL, NULL, 13)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (17, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 15:25:41.290' AS DateTime), NULL, NULL, 14)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (18, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 15:27:48.007' AS DateTime), NULL, NULL, 15)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (19, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 15:28:18.373' AS DateTime), NULL, NULL, 16)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (20, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:08:30.640' AS DateTime), NULL, NULL, 17)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (21, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:09:56.840' AS DateTime), NULL, NULL, 18)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (22, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:12:53.440' AS DateTime), NULL, NULL, 19)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (23, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:20:05.230' AS DateTime), NULL, NULL, 20)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (24, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:21:15.100' AS DateTime), NULL, NULL, 21)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (25, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:23:47.297' AS DateTime), NULL, NULL, 22)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (26, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:24:50.233' AS DateTime), NULL, NULL, 23)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (27, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:26:17.923' AS DateTime), NULL, NULL, 24)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (28, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:27:31.980' AS DateTime), NULL, NULL, 25)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (29, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:30:37.627' AS DateTime), NULL, NULL, 26)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (30, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:50:21.470' AS DateTime), NULL, NULL, 27)
INSERT [dbo].[website] ([id], [website], [isdelete], [addedby], [addedon], [updatedby], [updatedon], [GUID]) VALUES (31, N'http://www.google.com', 0, 0, CAST(N'2018-01-31 17:51:43.030' AS DateTime), NULL, NULL, 28)
SET IDENTITY_INSERT [dbo].[website] OFF
/****** Object:  StoredProcedure [dbo].[DeleteRecords]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[DeleteRecords] 
@table_names  as varchar(MAX),
@field_Name1 as varchar(MAX), 
@field_Name2 as varchar(MAX), 
@value2 as varchar(MAX) ,
@value1 as varchar(MAX),
@field_Name as varchar(MAX) ,
@value as varchar(MAX)
as

If @table_names  = 'ContactMaster' 
BEGIN
   update  ContactMaster set isdelete=1  , updatedby   =  @value1 , updatedon  = getdate() WHERE GUID = @value
END
Else If @table_names  = 'GUIDMaster'
BEGIN
     update  GUIDMaster set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GU_id  = @value
END
Else If @table_names  = 'address'
BEGIN
    update  address set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GUID  = @value
END
Else If @table_names  = 'website'
BEGIN
    update  website set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GUID = @value
END
Else If @table_names  = 'contactno'
BEGIN
    update  contactno set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GUID = @value
END
Else If @table_names  = 'email'
BEGIN
    update  email set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GUID = @value
END
Else If @table_names  = 'contactnonew'
BEGIN
    update  contactno set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE contactno = @value
END

Else If @table_names  = 'emailnew'
BEGIN
  update  email set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE email = @value
END
Else If @table_names  = 'websitenew'
BEGIN
 update  website set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GUID = @value
END


GO
/****** Object:  StoredProcedure [dbo].[DeleteRecordsNEW]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create procedure [dbo].[DeleteRecordsNEW] 
@table_names  as varchar(MAX),
@field_Name1 as varchar(MAX), 
@field_Name2 as varchar(MAX), 
@value2 as varchar(MAX) ,
@value1 as varchar(MAX),
@field_Name as varchar(MAX) ,
@value as varchar(MAX)
as

If @table_names  = 'ContactMaster' 
BEGIN
   update  ContactMaster set isdelete=1  , updatedby   =  @value1 , updatedon  = getdate() WHERE GUID = @value
END
Else If @table_names  = 'GUIDMaster'
BEGIN
     update  GUIDMaster set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE id  = @value
END
Else If @table_names  = 'address'
BEGIN
    update  address set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GUID  = @value
END
Else If @table_names  = 'website'
BEGIN
    update  website set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GUID = @value
END
Else If @table_names  = 'contactno'
BEGIN
    update  contactno set isdelete=1  , updatedby  =  @value1 , updatedon = getdate() WHERE GUID = @value
END
Else If @table_names  = 'email'
BEGIN
    update  email set isdelete=1  , @field_Name1  =  @value1 , updatedon = getdate() WHERE GUID = @value
END


GO
/****** Object:  StoredProcedure [dbo].[GetAllRecords]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetAllRecords] 

@user_id as int,
@zone as varchar(50),
  @List AS dbo.SGLIST READONLY  
as
 select contacts.first_name,contacts.middle_name,contacts.last_name,contacts.prefix, contacts.addedby ,contacts.addedon ,GuidList.GU_id as GUID ,GuidList.IsUpdated ,  
contacts.sufix, contacts.organisation   
,contacts.title ,contacts.SGID ,contacts.GUID as vcf_id   
,contactlist.id as  contactno_id,contactlist.contactno ,contactlist.type as number_type  ,  
emaillist.id as email_id,emaillist.email,emaillist.type as email_type   ,  
addresslist.id as address_id,addresslist.city,addresslist.zipcode,addresslist.country,  
addresslist.street1,addresslist.street2,addresslist.state,contacts .IsDeleteRequest,  
addresslist.type as address_type  ,  
websitelist.id as website_id,websitelist.website ,
GuidList.zone   from (
(select  guid.id,guid.zone,guid.GU_id,guid.IsUpdated  from GUIDMaster guid
inner join ContactMaster contact on contact.GUID=guid.id where guid.isdelete= 0 )  GuidList

left join (select *  from  address addres where isdelete=0 ) addresslist on GuidList.id=addresslist.GUID 
left join (select *  from  contactno nos where isdelete=0 ) contactlist on GuidList.id=contactlist.GUID 
left join (select *  from  email emails where isdelete=0 ) emaillist on GuidList.id=emaillist.GUID 
left join (select *  from  website emails where isdelete=0 ) websitelist on GuidList.id=websitelist.GUID 
inner join ContactMaster contacts on contacts.GUID=GuidList.id 
inner join @List lists on lists.SGID=contacts.addedby 
) where contacts.isdelete=0  
and GuidList.zone=@zone



GO
/****** Object:  StoredProcedure [dbo].[getcalllog]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getcalllog]  
@user_id as int as
select CallLogs.id,CallLogs.empname,CallLogs.mobno,CallLogs.duration,CallLogs.calldate,CallLogs.user_id,CallLogs.zone,CallLogs.addedon  ,um.SG_id from callog CallLogs
inner join usermaster um on um.id=CallLogs.user_id 
where CallLogs.user_id= @user_id

GO
/****** Object:  StoredProcedure [dbo].[getcalllogOnSupervisor]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[getcalllogOnSupervisor] 
  @List AS dbo.SGLIST READONLY  
as 
select CallLogs.id,CallLogs.empname,CallLogs.mobno,CallLogs.duration,CallLogs.calldate,CallLogs.user_id,CallLogs.zone,CallLogs.addedon  ,um.SG_id from callog CallLogs
inner join usermaster um on um.id=CallLogs.user_id 
inner join @List lists on lists.SGID=CallLogs.user_id    

GO
/****** Object:  StoredProcedure [dbo].[GetContactInformation]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetContactInformation]   
as  


 select contacts.first_name,contacts.middle_name,contacts.last_name,contacts.prefix, contacts.addedby ,contacts.addedon ,contacts.GUID  as GUID,  
contacts.sufix, contacts.organisation   
,contacts.title ,contacts.SGID ,contacts.GUID as vcf_id   
,contactlist.id as  contactno_id,contactlist.contactno ,contactlist.type as number_type  ,  
emaillist.id as email_id,emaillist.email,emaillist.type as email_type   ,  
addresslist.id as address_id,addresslist.city,addresslist.zipcode,addresslist.country,  
addresslist.street1,addresslist.street2,addresslist.state,contacts .IsDeleteRequest,  
addresslist.type as address_type  ,  
websitelist.id as website_id,websitelist.website ,
GuidList.zone   from (
(select  guid.id,guid.zone,guid.GU_id  from GUIDMaster guid
inner join ContactMaster contact on contact.GUID=guid.id where guid.isdelete= 0 )  GuidList

left join (select *  from  address addres where isdelete=0 ) addresslist on GuidList.id=addresslist.GUID 
left join (select *  from  contactno nos where isdelete=0 ) contactlist on GuidList.id=contactlist.GUID 
left join (select *  from  email emails where isdelete=0 ) emaillist on GuidList.id=emaillist.GUID 
left join (select *  from  website emails where isdelete=0 ) websitelist on GuidList.id=websitelist.GUID 
inner join ContactMaster contacts on contacts.GUID=GuidList.id 
) where contacts.isdelete=0  



GO
/****** Object:  StoredProcedure [dbo].[GetContactInformationBasedOnGUID]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetContactInformationBasedOnGUID] 
@GUID as varchar(Max) as
 select contacts.first_name,contacts.middle_name,contacts.last_name,contacts.prefix, contacts.addedby ,contacts.addedon ,contacts.GUID as GUID ,  
contacts.sufix, contacts.organisation   
,contacts.title ,contacts.SGID ,contacts.GUID as vcf_id   
,contactlist.id as  contactno_id,contactlist.contactno ,contactlist.type as number_type  ,  
emaillist.id as email_id,emaillist.email,emaillist.type as email_type   ,  
addresslist.id as address_id,addresslist.city,addresslist.zipcode,addresslist.country,  
addresslist.street1,addresslist.street2,addresslist.state,contacts .IsDeleteRequest,  
addresslist.type as address_type  ,  
websitelist.id as website_id,websitelist.website ,
GuidList.zone   from (
(select  guid.id,guid.zone,guid.GU_id   from GUIDMaster guid
inner join ContactMaster contact on contact.GUID=guid.id where guid.isdelete= 0 )  GuidList

left join (select *  from  address addres where isdelete=0 ) addresslist on GuidList.id=addresslist.GUID 
left join (select *  from  contactno nos where isdelete=0 ) contactlist on GuidList.id=contactlist.GUID 
left join (select *  from  email emails where isdelete=0 ) emaillist on GuidList.id=emaillist.GUID 
left join (select *  from  website emails where isdelete=0 ) websitelist on GuidList.id=websitelist.GUID 
inner join ContactMaster contacts on contacts.GUID=GuidList.id 
) where contacts.isdelete=0   and GuidList.GU_id=@GUID


GO
/****** Object:  StoredProcedure [dbo].[GetContactInformationBasedOnID]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetContactInformationBasedOnID] 
@ID as varchar(Max) as
 select contacts.first_name,contacts.middle_name,contacts.last_name,contacts.prefix, contacts.addedby ,contacts.addedon ,contacts.GUID as GUID ,  
contacts.sufix, contacts.organisation   
,contacts.title ,contacts.SGID ,contacts.GUID as vcf_id   
,contactlist.id as  contactno_id,contactlist.contactno ,contactlist.type as number_type  ,  
emaillist.id as email_id,emaillist.email,emaillist.type as email_type   ,  
addresslist.id as address_id,addresslist.city,addresslist.zipcode,addresslist.country,  
addresslist.street1,addresslist.street2,addresslist.state,contacts .IsDeleteRequest,  
addresslist.type as address_type  ,  
websitelist.id as website_id,websitelist.website ,
GuidList.zone,contacts.addedby as user_id   from (
(select  guid.id,  guid.GU_id,guid.zone  from GUIDMaster guid
inner join ContactMaster contact on contact.GUID=guid.id where guid.isdelete= 0 )  GuidList

left join (select *  from  address addres where isdelete=0 ) addresslist on GuidList.id=addresslist.GUID 
left join (select *  from  contactno nos where isdelete=0 ) contactlist on GuidList.id=contactlist.GUID 
left join (select *  from  email emails where isdelete=0 ) emaillist on GuidList.id=emaillist.GUID 
left join (select *  from  website emails where isdelete=0 ) websitelist on GuidList.id=websitelist.GUID 
inner join ContactMaster contacts on contacts.GUID=GuidList.id 
) where contacts.isdelete=0   and GuidList.id=@ID

select *from contactmaster

GO
/****** Object:  StoredProcedure [dbo].[GetContactInformationBasedOnSupervisor]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetContactInformationBasedOnSupervisor] 
  @List AS dbo.SGLIST READONLY  
AS  
SET NOCOUNT ON;  
 select contacts.first_name,contacts.middle_name,contacts.last_name,contacts.prefix, contacts.addedby ,contacts.addedon ,contacts.GUID as GUID ,  
contacts.sufix, contacts.organisation   
,contacts.title ,contacts.SGID ,contacts.GUID as vcf_id   
,contactlist.id as  contactno_id,contactlist.contactno ,contactlist.type as number_type  ,  
emaillist.id as email_id,emaillist.email,emaillist.type as email_type   ,  
addresslist.id as address_id,addresslist.city,addresslist.zipcode,addresslist.country,  
addresslist.street1,addresslist.street2,addresslist.state,contacts .IsDeleteRequest,  
addresslist.type as address_type  ,  
websitelist.id as website_id,websitelist.website ,
GuidList.zone   from (
(select  guid.id,guid.zone,   guid.GU_id from GUIDMaster guid
inner join ContactMaster contact on contact.GUID=guid.id where guid.isdelete= 0 )  GuidList

left join (select *  from  address addres where isdelete=0 ) addresslist on GuidList.id=addresslist.GUID 
left join (select *  from  contactno nos where isdelete=0 ) contactlist on GuidList.id=contactlist.GUID 
left join (select *  from  email emails where isdelete=0 ) emaillist on GuidList.id=emaillist.GUID 
left join (select *  from  website emails where isdelete=0 ) websitelist on GuidList.id=websitelist.GUID 
inner join ContactMaster contacts on contacts.GUID=GuidList.id 
inner join @List lists on lists.SGID=contacts.addedby   
) where contacts.isdelete=0  

--where vcf.isdelete =0




GO
/****** Object:  StoredProcedure [dbo].[GetContactInformationOnUser]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetContactInformationOnUser]
@userid as int 
as
 select contacts.first_name,contacts.middle_name,contacts.last_name,contacts.prefix, contacts.addedby ,contacts.addedon ,contacts.GUID as GUID ,  
contacts.sufix, contacts.organisation   
,contacts.title ,contacts.SGID ,contacts.GUID as vcf_id   
,contactlist.id as  contactno_id,contactlist.contactno ,contactlist.type as number_type  ,  
emaillist.id as email_id,emaillist.email,emaillist.type as email_type   ,  
addresslist.id as address_id,addresslist.city,addresslist.zipcode,addresslist.country,  
addresslist.street1,addresslist.street2,addresslist.state,contacts .IsDeleteRequest,  
addresslist.type as address_type  ,  
websitelist.id as website_id,websitelist.website ,
GuidList.zone   from (
(select  guid.id,guid.zone , guid.GU_id  from GUIDMaster guid
inner join ContactMaster contact on contact.GUID=guid.id where guid.isdelete= 0 )  GuidList

left join (select *  from  address addres where isdelete=0 ) addresslist on GuidList.id=addresslist.GUID 
left join (select *  from  contactno nos where isdelete=0 ) contactlist on GuidList.id=contactlist.GUID 
left join (select *  from  email emails where isdelete=0 ) emaillist on GuidList.id=emaillist.GUID 
left join (select *  from  website emails where isdelete=0 ) websitelist on GuidList.id=websitelist.GUID 
inner join ContactMaster contacts on contacts.GUID=GuidList.id 
) where contacts.isdelete=0  
and contacts.addedby=@userid  
--and emails.isdelete=0 
--and nos.isdelete=0 and addres.isdelete=0  and contact.isdelete=0   and websites.isdelete =0



GO
/****** Object:  StoredProcedure [dbo].[GetDeletedRecords]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetDeletedRecords] 
@syncdate as datetime,
@user_id as int,
@zone as varchar(50),
@List AS dbo.SGLIST READONLY  
as
select    distinct(vcf.GU_id) as GUID,
CASE WHEN DRequestedBy =@user_id
            THEN 'True' 
            ELSE 'False'
       END AS IsPersonal 
from  GUIDMaster vcf
inner join @List lists on lists.SGID=vcf.addedby 
where vcf.isdelete =1
 and vcf.zone=@zone
 and CAST(vcf.updatedon AS datetime ) >= isnull(CAST(@syncdate AS datetime),  vcf.updatedon) 


 select *from GUIDMaster 


GO
/****** Object:  StoredProcedure [dbo].[GetDeleteformation]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetDeleteformation] 
as
 select contacts.first_name,contacts.middle_name,contacts.last_name,contacts.prefix, contacts.addedby ,contacts.addedon ,contacts.GUID as GUID ,  
contacts.sufix, contacts.organisation   
,contacts.title ,contacts.SGID ,contacts.GUID as vcf_id   
,contactlist.id as  contactno_id,contactlist.contactno ,contactlist.type as number_type  ,  
emaillist.id as email_id,emaillist.email,emaillist.type as email_type   ,  
addresslist.id as address_id,addresslist.city,addresslist.zipcode,addresslist.country,  
addresslist.street1,addresslist.street2,addresslist.state,contacts .IsDeleteRequest,  
addresslist.type as address_type  ,  
websitelist.id as website_id,websitelist.website ,
GuidList.zone   from (
(select  guid.id,guid.zone , guid.GU_id  from GUIDMaster guid
inner join ContactMaster contact on contact.GUID=guid.id where guid.isdelete= 0 )  GuidList

left join (select *  from  address addres where isdelete=0 ) addresslist on GuidList.id=addresslist.GUID 
left join (select *  from  contactno nos where isdelete=0 ) contactlist on GuidList.id=contactlist.GUID 
left join (select *  from  email emails where isdelete=0 ) emaillist on GuidList.id=emaillist.GUID 
left join (select *  from  website emails where isdelete=0 ) websitelist on GuidList.id=websitelist.GUID 
inner join ContactMaster contacts on contacts.GUID=GuidList.id 
) where contacts.isdelete=0   and contacts .IsDeleteRequest='1'

select *from ContactMaster 

GO
/****** Object:  StoredProcedure [dbo].[GetPendingSync Records]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetPendingSync Records] 
@syncdate as datetime,
@user_id as int,
@zone as varchar(50),
  @List AS dbo.SGLIST READONLY  
as
 select contacts.first_name,contacts.middle_name,contacts.last_name,contacts.prefix, contacts.addedby ,contacts.addedon ,GuidList.GU_id as GUID ,GuidList.IsUpdated ,  
contacts.sufix, contacts.organisation   
,contacts.title ,contacts.SGID ,contacts.GUID as vcf_id   
,contactlist.id as  contactno_id,contactlist.contactno ,contactlist.type as number_type  ,  
emaillist.id as email_id,emaillist.email,emaillist.type as email_type   ,  
addresslist.id as address_id,addresslist.city,addresslist.zipcode,addresslist.country,  
addresslist.street1,addresslist.street2,addresslist.state,contacts .IsDeleteRequest,  
addresslist.type as address_type  ,  
websitelist.id as website_id,websitelist.website ,
GuidList.zone   from (
(select  guid.id,guid.zone,guid.GU_id,guid.IsUpdated  from GUIDMaster guid
inner join ContactMaster contact on contact.GUID=guid.id where guid.isdelete= 0 )  GuidList

left join (select *  from  address addres where isdelete=0 ) addresslist on GuidList.id=addresslist.GUID 
left join (select *  from  contactno nos where isdelete=0 ) contactlist on GuidList.id=contactlist.GUID 
left join (select *  from  email emails where isdelete=0 ) emaillist on GuidList.id=emaillist.GUID 
left join (select *  from  website emails where isdelete=0 ) websitelist on GuidList.id=websitelist.GUID 
inner join ContactMaster contacts on contacts.GUID=GuidList.id 
inner join @List lists on lists.SGID=contacts.addedby 
) where contacts.isdelete=0  
and CAST( contacts.addedon AS datetime ) >= isnull(CAST(@syncdate AS datetime), contacts.addedon) and GuidList.zone=@zone
--and emails.isdelete=0 
--and nos.isdelete=0 and addres.isdelete=0  and contact.isdelete=0   and websites.isdelete =0  
--and vcf.addedby !=@user_id 



GO
/****** Object:  StoredProcedure [dbo].[GetPhoneNumber]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetPhoneNumber]
@contact as varchar(MAX) as

SELECT contactno  FROM contactno WHERe  REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
       REPLACE(REPLACE(REPLACE(REPLACE(Substring(contactno, 3,len(contactno)),
        '-',''),'@',''),')',''),'$',''),'%',''),
        '^',''),'(',''),'*',''),' ','') like '%'+ REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(
       REPLACE(REPLACE(REPLACE(REPLACE(Substring(@contact, 3,len(@contact)),
        '-',''),'@',''),')',''),'$',''),'%',''),
        '^',''),'(',''),'*',''),' ','')+'%'
		union 
SELECT contactno  FROM contactno WHERe  contactno like '%'+@contact+'%'
union 
	
SELECT Substring(@contact, 4,len(@contact))  FROM contactno 





GO
/****** Object:  StoredProcedure [dbo].[GetProfile]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetProfile]
@userName AS VARCHAR(max)
as
select *from UserMaster where IsActive='True' and SG_id=isnull(@userName,SG_id)


GO
/****** Object:  StoredProcedure [dbo].[GetSubordinates]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetSubordinates] 
@SGID as varchar(max) as
WITH UserCTE
     AS (SELECT SG_id,id,
              
                reporting_person_id,
                0 AS EmpLevel
         FROM   UserMaster 
        WHERE  reporting_person_id = @SGID
         UNION ALL
         SELECT usr.SG_id,usr.id,
               usr.reporting_person_id,
                mgr.[EmpLevel] + 1
         FROM   UserMaster AS usr
                INNER JOIN UserCTE AS mgr
                        ON usr.reporting_person_id = mgr.SG_id
         WHERE  usr.reporting_person_id IS NOT NULL)
SELECT *
FROM   UserCTE AS u
ORDER  BY EmpLevel; 

GO
/****** Object:  StoredProcedure [dbo].[getUserRoles]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[getUserRoles]
	@user_id int
AS
	SELECT rolemaster.role_id, rolemaster.role_name,permissions.permission_id, permissions.permission, userrole.user_id, userrole.user_role_id 
	from tblrolemaster rolemaster
	LEFT OUTER JOIN tbluserrolemaster userrole on userrole.role_id = rolemaster.role_id LEFT OUTER JOIN tblroledetails roledetails on 
	roledetails.role_id=rolemaster.role_id LEFT OUTER JOIN tblpermission permissions on permissions.permission_id=roledetails.permission_id
	where userrole.user_id = @user_id


GO
/****** Object:  StoredProcedure [dbo].[GetWithoutSubordinate]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetWithoutSubordinate]
@SGID as varchar(max) ,
@zone as varchar(50)
as
SELECT SG_id,id
  FROM UserMaster e
 WHERE NOT EXISTS (SELECT SG_id
                     FROM UserMaster e2
                    WHERE e2.reporting_person_id = e.SG_id )
					and  e.zone=@zone
GO
/****** Object:  StoredProcedure [dbo].[GetWithoutSubordinates]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[GetWithoutSubordinates]
@SGID as varchar(max) as
SELECT SG_id,id
  FROM UserMaster e
 WHERE NOT EXISTS (SELECT 1
                     FROM UserMaster e2
                    WHERE e2.reporting_person_id = @SGID)
GO
/****** Object:  StoredProcedure [dbo].[spAddressSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spAddressSave]    
	@street1	varchar(MAX),
	@street2	varchar(MAX),
  
    @city	varchar(50),
    @state	varchar(50),
    @zipcode	varchar(50),
    @country	varchar(50) ,  
	@AddressID int output ,
	@ID as int ,
	@Vcfid as int,
	@added_by as varchar(Max),
	@type as int
AS    
  
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int     
 if(@ID=0)
 BEGIN
 INSERT INTO [dbo].[address]    
   --        ([street]    
   --        ,[Pobox]  
   --        ,[Neiborhood]    
			--,[city]    
   --        ,[state]    
			--,[zipcode],
			--[country],
			--[IsDelete] )    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@street1    
           ,@street2  
         
			,@city    
           ,@state    
			,@zipcode,
			@country ,
			@Vcfid ,
			@added_by ,
			getdate(),
			null,
			null,
			'0'  ,
			@type 
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @AddressID = @insertedKey  
 END
 ELSE
BEGIN 
 UPDATE  [dbo].[address] 
          set 
          street1=isnull(@street1,street1)
           , street2=isnull(@street2,street2) 
           --, Neiborhood=isnull(@Neiborhood,street)
			, city=isnull(@city,city)  
           , state=isnull(@state,state) 
			, zipcode=isnull(@zipcode,zipcode)
			, country=isnull(@country,country)
			--,updatedby=@updatedby
			
			where id=@ID
			 Set @AddressID = @ID  
  END
  END



GO
/****** Object:  StoredProcedure [dbo].[spcatalogSaveSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spcatalogSaveSave]
@empname	as varchar(MAX)	,
@mobno	as varchar(MAX)	,
@duration	as varchar(MAX)	,
@calldate	as datetime	,
@user_id	as int	,
@zone	as varchar(MAX)	
		
 as
INSERT into callog VALUES (@empname, @mobno, @duration,@calldate ,@user_id,@zone,getdate())
			


GO
/****** Object:  StoredProcedure [dbo].[spContactNOSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[spContactNOSave]    
	@contactno	varchar(15),
	@ContactID int output ,
	@ID as int   ,
   @Vcfid as int,
	@added_by as varchar(Max),
	@type as int
AS    
   
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int  
   if(@ID=0)
   BEGIN   
 INSERT INTO [dbo].[contactno]    
            
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@contactno ,'0'  ,@added_by,getdate(),null,null,@Vcfid ,@type 
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @ContactID = @insertedKey  
  END
  ELSE
  BEGIN
  Update [contactno] set [contactno]=isnull(@contactno,contactno) where id=@ID 
   Set @ContactID = @ID  
  END
  END



GO
/****** Object:  StoredProcedure [dbo].[spEmailSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spEmailSave]    
	@email	varchar(MAX),
	@emailID int output ,
	@ID as int    ,
	   @Vcfid as int,
	@added_by as varchar(Max),
	@type as int
AS    
   
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int     
  if(@ID=0)
  BEGIN
 INSERT INTO [dbo].email    
          --(email,[IsDelete])    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@email ,'0' ,@added_by,getdate(),null,null,@Vcfid ,@type 
             
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @emailID = @insertedKey  
 END
   ELSE
  BEGIN
  Update email set email=isnull(@email,email) where id=@ID 
   Set @emailID = @ID  
  END
  END



GO
/****** Object:  StoredProcedure [dbo].[spSaveContactMaster]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[spSaveContactMaster]
@firstname	as varchar(max) ,
@middlename	as varchar(max), 
@lastname	as varchar(max) ,
@prefix	as varchar(20) ,
@sufix	as varchar(20) ,
@organisation  as varchar(50),
@jobtitle	as varchar(20),
@sg_id as varchar(max),
@Vcfid as int,
--@type as int,
@addedby as varchar(max), 
--@updatedby as int, 
@ContactID int output ,
@ID as int as
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int     
 IF(@ID=0)
 BEGIN
 INSERT INTO ContactMaster    
        --   (
			--firstname,	
			--middlename,	
			--lastname,	
			--email1	,
			--email2	,
			--email3	,
			--email1_type,
			--email2_type,
			--email3_type,
			--add1,
			--add2,
			--add3,
			--add1_type,
			--add2_type,
			--add3_type,
			--website1,	
			--website2,
			--website3,
			--contactno1,	
			--contactno2,	
			--contactno3,	
			--contactno4,	
			--contactno5,	
			--contact1_type,
			--contact2_type,
			--contact3_type,
			--contact4_type,
			--contact5_type,
			--organisation,
			--jobtitle	,
			--Notes	,
			--status,
			--addedby,
			--addedon	,	
			--updatedby,
			--updatedon	
		--	)    
			output inserted.id into @NewIdentityValues     
		 VALUES    
          (
		
		  @Vcfid ,
		  @addedby,
		  getdate(),
		  null  ,
		   null  ,
		   '0',
		@firstname,	
		@middlename,	
		@lastname,	
		@prefix,
		@sufix ,
		--@type , 
		@organisation,
		@jobtitle	,
		@sg_id,
		'0'
	
    )
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @ContactID = @insertedKey  
 END
 ELSE
BEGIN 
 --UPDATE  [dbo].contactmaster 
 --         set 
	--	    firstname=isnull(@firstname,firstname)	,
	--		middlename=isnull(@middlename,middlename	),
	--		lastname=isnull(@lastname,lastname	),
	--		email1=isnull(@email1	,email1),
	--		email2=isnull(@email2	,email2),
	--		email3=isnull(@email3	,email3),
	--		email1_type=isnull(@email1_type,email1_type),
	--		email2_type=isnull(@email2_type,email2_type),
	--		email3_type=isnull(@email3_type,email3_type),
	--		add1=isnull(@add1,add1),
	--		add2=isnull(@add2,add2),
	--		add3=isnull(@add3,add3),
	--		add1_type=isnull(@add1_type,add1_type),
	--		add2_type=isnull(@add2_type,add2_type),
	--		add3_type=isnull(@add3_type,add3_type),
	--		website1=isnull(@website1,website1)	,
	--		website2=isnull(@website2,website2),
	--		website3=isnull(@website3,website3),
	--		contactno1=isnull(@contactno1,	contactno1),
	--		contactno2=isnull(@contactno2,	contactno2),
	--		contactno3=isnull(@contactno3,	contactno3),
	--		contactno4=isnull(@contactno4,	contactno4),
	--		contactno5=isnull(@contactno5,contactno5),	
	--		contact1_type=isnull(@contact1_type,contact1_type),
	--		contact2_type=isnull(@contact2_type,contact2_type),
	--		contact3_type=isnull(@contact3_type,contact3_type),
	--		contact4_type=isnull(@contact4_type,contact4_type),
	--		contact5_type=isnull(@contact5_type,organisation),
	--		organisation=isnull(@organisation,organisation),
	--		jobtitle=isnull(@jobtitle	,jobtitle),
	--		Notes=isnull(@Notes	,Notes),
	--		updatedby=isnull(@updatedby,updatedby),
	--		updatedon=getdate()		
	--		where id=@ID
			 Set @ContactID = @ID  
  END
  END



GO
/****** Object:  StoredProcedure [dbo].[spSMSDetailSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROCEDURE [dbo].[spSMSDetailSave]
@sgid	as varchar(MAX)	,
@mobileno	as varchar(MAX)	,
@message	as varchar(MAX)	,
@IsSent as	bit	,
@reqdatetime	as datetime	,
@processdatetime	as datetime	,
@returnMsg	as varchar(MAX)	
		
 as
INSERT into SMS(sgid, mobileno, message,IsSent ,reqdatetime,processdatetime,returnMsg)VALUES(@sgid, @mobileno, @message,@IsSent ,@reqdatetime,@processdatetime,@returnMsg)
			



GO
/****** Object:  StoredProcedure [dbo].[spSyncSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[spSyncSave]    
	@user_id	varchar(MAX),
	@zone varchar(MAX)
AS    
 INSERT INTO syncdetails        
     VALUES    
          (@user_id  ,getdate(),@zone
             
			) 
    




GO
/****** Object:  StoredProcedure [dbo].[spSyncUpdate]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROC [dbo].[spSyncUpdate]    
	@user_id	varchar(MAX),
	@zone varchar(MAX)
AS    
update syncdetails   set syncdate= getdate() ,zone=@zone where user_id=@user_id     
    
    




GO
/****** Object:  StoredProcedure [dbo].[sptDeviceDetailSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sptDeviceDetailSave]
 @SL_NO as INT,
 @sgid as varchar(50),
 @gsm_id as nvarchar(max)
 as
INSERT into tbldevice(SL_NO, sgid, gsm_id )VALUES(@SL_NO, @sgid,  @gsm_id)
			



GO
/****** Object:  StoredProcedure [dbo].[spTypeSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create PROC [dbo].[spTypeSave]    
	@type	varchar(15),
	@typeID int output,
	@ID as int   
	
AS    
   
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int 
   if(@ID=0)
  BEGIN    
 INSERT INTO [dbo].type    
          --(website,[IsDelete])    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@type  
             
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @typeID = @insertedKey  
   END
   ELSE
  BEGIN
  --Update type set type=isnull(@type) where id=@ID 
   Set @typeID = @ID  
  END
  END



GO
/****** Object:  StoredProcedure [dbo].[spVcfSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROC [dbo].[spVcfSave]    
	--@name	varchar(MAX),
	@ContactID int output,
	@ID as int   ,
		@GU_id as varchar(MAX)  ,
		@zone as varchar(50),
		@SG_id as varchar(200)  ,
	@added_by  as varchar(200)  ,
	@IsUpdated as bit
AS    
   
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int 
   if(@ID=0)
  BEGIN    
 INSERT INTO [dbo].GUIDMaster    
          --(website,[IsDelete])    
 output inserted.id into @NewIdentityValues     
     VALUES    
          ( @GU_id,@zone,@SG_id,'0',@added_by,getdate(),null,null,@IsUpdated,'0'
             
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @ContactID = @insertedKey  
   END
   ELSE
  BEGIN
     Set @ContactID = @ID  
  END
  END

GO
/****** Object:  StoredProcedure [dbo].[spWebsiteSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROC [dbo].[spWebsiteSave]    
	@website	varchar(Max),
	@websiteID int output    ,
	@ID as int   ,
		--   @Vcfid as int,
	@added_by as varchar(Max),
	@guid as int
AS    
   
BEGIN    
 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int 
   if(@ID=0)
  BEGIN    
 INSERT INTO [dbo].website    
          --(website,[IsDelete])    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@website ,'0'   ,@added_by,getdate(),null,null  ,@guid
             
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @websiteID = @insertedKey  
   END
   ELSE
  BEGIN
  Update website set website=isnull(@website,website) where id=@ID 
   Set @websiteID = @ID  
  END
  END





GO
/****** Object:  StoredProcedure [dbo].[spWebsiteVCFSave]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE  PROC [dbo].[spWebsiteVCFSave]    
	@website_id	int,
	@vcf_id	int,
	@ID int output,
	@added_by as int

		
AS    
   

 Declare @NewIdentityValues table(UniqueID int) 
  Declare @insertedKey as int 
  

 INSERT INTO [dbo].website_vcf    
          --(website,[IsDelete])    
 output inserted.id into @NewIdentityValues     
     VALUES    
          (@website_id,  @vcf_id,'0',@added_by,getdate(),null,null 
             
			) 
    
 select @insertedKey=UniqueID FROM @NewIdentityValues    
 Set @ID = @insertedKey  

 


GO
/****** Object:  StoredProcedure [dbo].[truncatetables]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[truncatetables] as
truncate table address
truncate table ContactMaster
truncate table contactno
truncate table CustomerMaster
truncate table email
truncate table GUIDMaster
truncate table SMS
truncate table syncdetails
truncate table tbldevice
truncate table type
truncate table syncdetails
truncate table website
truncate table website_vcf
truncate table callog
GO
/****** Object:  StoredProcedure [dbo].[ValidateUser]    Script Date: 3/20/2018 12:25:06 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create procedure [dbo].[ValidateUser]
@SG_id as varchar(50),
@mobile_no as varchar(20) as
select *from usermaster where SG_id=@SG_id and mobile_no=@mobile_no 

GO
USE [master]
GO
ALTER DATABASE [TEST] SET  READ_WRITE 
GO
